package com.dhp.baylordiabetes.util;


public class LConstants {

    public static final int CODE_WRITE_PERMISSION = 101;

    public static final boolean AUTO_SYNC_ENABLED = true;

    public static final String ADMIN_ID  = "admin";
    public static final String ADMIN_PWD = "baylor123";

    public static final String LANGUAGE_FILE     = "input_data.json";
    public static final String ENGLISH_DATA_FILE = "data_english.json";

    public static final String FILES_PATH       = "/BaylorDiabetes/";
    public static final String VIDEO_PATH       = FILES_PATH + "Videos/";
    public static final String GIF_PATH         = FILES_PATH + "Gifs/";
    public static final String OUTPUT_JSON_PATH = FILES_PATH + "Output_Data/";

//    public static final String FILES_PATH_HIDDEN = "/.BaylorDiabetesFiles/";
//    public static final String INPUT_JSON_PATH   = FILES_PATH_HIDDEN + "Input_Json/";
//    public static final String ICONS_PATH        = FILES_PATH_HIDDEN + "Icons/";
//    public static final String AUDIO_PATH        = FILES_PATH_HIDDEN + "Audio/";

    public static final String FILES_PATH_HIDDEN = "/.cbzmpsejbcfuftgjmft/";
    public static final String INPUT_JSON_PATH   = FILES_PATH_HIDDEN + "joqvuktpo/";
    public static final String ICONS_PATH        = FILES_PATH_HIDDEN + "jdpot/";
    public static final String AUDIO_PATH        = FILES_PATH_HIDDEN + "bvejp/";

    public static final String DATABASE_ENCRYPTION_KEY = "baylor@db123key";
    public static final String INPUT_JSON_DECRYPT_KEY  = "baylor123enc@key";

}