package com.dhp.baylordiabetes.data.item;

import com.google.gson.annotations.SerializedName;


public class ItemQuestion {

    @SerializedName("correct_answer")
    private String correctAnswer;

    @SerializedName("title")
    private String title;

    @SerializedName("audio_file")
    private String audioFile;

    @SerializedName("question")
    private String question;

    @SerializedName("answers")
    private ItemAnswer[] answers;

    @SerializedName("answers_in_popup")
    private String answersInPopup;

    public String getCorrectAnswer() {
        return correctAnswer;
    }

    public void setCorrectAnswer(String correctAnswer) {
        this.correctAnswer = correctAnswer;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAudioFile() {
        return audioFile;
    }

    public void setAudioFile(String audioFile) {
        this.audioFile = audioFile;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public ItemAnswer[] getAnswers() {
        return answers;
    }

    public void setAnswers(ItemAnswer[] answers) {
        this.answers = answers;
    }

    public String getAnswersInPopup() {
        return answersInPopup;
    }

    public void setAnswersInPopup(String answersInPopup) {
        this.answersInPopup = answersInPopup;
    }
}
