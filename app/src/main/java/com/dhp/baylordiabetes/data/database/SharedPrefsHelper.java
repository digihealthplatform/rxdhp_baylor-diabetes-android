package com.dhp.baylordiabetes.data.database;

import android.content.SharedPreferences;

import javax.inject.Inject;
import javax.inject.Singleton;


@Singleton
public class SharedPrefsHelper {

    public static final String SHARED_PREFS_NAME = "screening_app_shared_prefs";

    public static final String KEY_LOGGED_IN             = "logged_in";
    public static final String KEY_REMEMBER_LOG_IN       = "remember_login";
    public static final String KEY_LOGGED_IN_USERNAME    = "logged_in_user_name";
    public static final String KEY_AUTO_SYNC             = "auto_sync";
    public static final String KEY_ASSETS_COPIED_VERSION = "assets_copied_version";

    public static final String KEY_VIDEO_COUNT_BATCH       = "key_video_count_batch";
    public static final String KEY_COUNSELLING_COUNT_BATCH = "key_counselling_count_batch";

    public static final String KEY_VIDEO_COUNT_BATCH_TRAINER       = "key_video_count_batch_trainer";
    public static final String KEY_COUNSELLING_COUNT_BATCH_TRAINER = "key_counselling_count_batch_trainer";

    public static final int NOT_LOGGED_IN   = 1;
    public static final int USER_LOGGED_IN  = 2;
    public static final int ADMIN_LOGGED_IN = 3;

    public static final boolean AUTO_SYNC_ENABLED = true;

    private SharedPreferences sharedPreferences;

    @Inject
    public SharedPrefsHelper(SharedPreferences sharedPreferences) {
        this.sharedPreferences = sharedPreferences;
    }

    public void setPref(String key, int value) {
        sharedPreferences.edit().putInt(key, value).apply();
    }

    public void setPref(String key, boolean value) {
        sharedPreferences.edit().putBoolean(key, value).apply();
    }

    public void setPref(String key, String value) {
        sharedPreferences.edit().putString(key, value).apply();
    }

    public int getIntPref(String key, int defaultValue) {
        return sharedPreferences.getInt(key, defaultValue);
    }

    public boolean getBooleanPref(String key, boolean defaultValue) {
        return sharedPreferences.getBoolean(key, defaultValue);
    }

    public String getStringPref(String key, String defaultValue) {
        return sharedPreferences.getString(key, defaultValue);
    }
}