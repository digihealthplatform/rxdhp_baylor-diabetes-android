package com.dhp.baylordiabetes.data.item;

import com.google.gson.annotations.SerializedName;


public class SociodemographicData {

    @SerializedName("title")
    private String title;

    @SerializedName("questions")
    private ItemQuestion[] questions;

    public ItemQuestion[] getQuestions() {
        return questions;
    }

    public void setQuestions(ItemQuestion[] questions) {
        this.questions = questions;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}