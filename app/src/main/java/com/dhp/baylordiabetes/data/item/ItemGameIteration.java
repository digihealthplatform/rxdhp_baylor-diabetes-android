package com.dhp.baylordiabetes.data.item;

import com.google.gson.annotations.SerializedName;


public class ItemGameIteration {

    @SerializedName("items")
    private ItemFood[] foodItems;

    @SerializedName("good_choice_plate_image")
    private String goodChoicePlateImage;

    @SerializedName("good_choice_partition_1_2")
    private String[] goodChoicePartition12;

    @SerializedName("good_choice_partition_3")
    private String[] goodChoicePartition3;

    public ItemFood[] getFoodItems() {
        return foodItems;
    }

    public void setFoodItems(ItemFood[] foodItems) {
        this.foodItems = foodItems;
    }

    public String getGoodChoicePlateImage() {
        return goodChoicePlateImage;
    }

    public void setGoodChoicePlateImage(String goodChoicePlateImage) {
        this.goodChoicePlateImage = goodChoicePlateImage;
    }

    public String[] getGoodChoicePartition12() {
        return goodChoicePartition12;
    }

    public void setGoodChoicePartition12(String[] goodChoicePartition12) {
        this.goodChoicePartition12 = goodChoicePartition12;
    }

    public String[] getGoodChoicePartition3() {
        return goodChoicePartition3;
    }

    public void setGoodChoicePartition3(String[] goodChoicePartition3) {
        this.goodChoicePartition3 = goodChoicePartition3;
    }
}