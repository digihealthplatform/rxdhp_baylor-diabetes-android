package com.dhp.baylordiabetes.data.item;


public class ItemQuestionAnswer {

    private ItemQuestion question;

    private String answer;
    private String parentQuestion;

    public ItemQuestion getQuestion() {
        return question;
    }

    public void setQuestion(ItemQuestion question) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getParentQuestion() {
        return parentQuestion;
    }

    public void setParentQuestion(String parentQuestion) {
        this.parentQuestion = parentQuestion;
    }
}
