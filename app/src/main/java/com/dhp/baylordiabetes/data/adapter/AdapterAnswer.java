package com.dhp.baylordiabetes.data.adapter;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.dhp.baylordiabetes.R;
import com.dhp.baylordiabetes.data.item.ItemAnswer;
import com.dhp.baylordiabetes.ui.AnswerCallback;

import java.io.File;

import static com.dhp.baylordiabetes.util.LUtils.getImagePath;


public class AdapterAnswer extends RecyclerView.Adapter<AdapterAnswer.ViewHolder> {

    private int   currentSelection = -1;
    private int[] originalPadding  = new int[4];

    private       ItemAnswer[]   itemAnswers;
    private final Context        context;
    private final LayoutInflater layoutInflater;

    public AdapterAnswer(Context context, ItemAnswer[] itemAnswers) {
        this.context = context;
        this.itemAnswers = itemAnswers;
        this.layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public AdapterAnswer.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View                      view      = layoutInflater.inflate(R.layout.item_answer, parent, false);
        ImageView                 imageView = (ImageView) view.findViewById(R.id.iv_answer);
        LinearLayout.LayoutParams params    = (LinearLayout.LayoutParams) imageView.getLayoutParams();

        if (itemAnswers.length == 2) {
            params.setMargins(params.leftMargin * 4, params.topMargin, params.rightMargin * 4, params.bottomMargin);

        } else if (itemAnswers.length == 3) {
            params.setMargins(params.leftMargin * 3, params.topMargin, params.rightMargin * 3, params.bottomMargin);

        } else if (itemAnswers.length == 4) {
            params.setMargins(params.leftMargin * 2, params.topMargin, params.rightMargin * 2, params.bottomMargin);
        }

        imageView.setLayoutParams(params);
        return new AdapterAnswer.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AdapterAnswer.ViewHolder holder, int position) {
        ItemAnswer itemAnswer = itemAnswers[position];

        if (itemAnswer.getIcon().endsWith(".gif")) {
            File imageFile = new File(getImagePath(itemAnswer.getIcon()));

            if (imageFile.exists()) {
                Uri imageUri = Uri.fromFile(imageFile);
                Glide.with(context)
                        .load(imageUri)
                        .asGif()
                        .into(holder.ivAnswer);
            }

        } else {
            File imageFile = new File(getImagePath(itemAnswer.getIcon()));
            if (imageFile.exists()) {
                holder.ivAnswer.setImageBitmap(BitmapFactory.decodeFile(imageFile.getAbsolutePath()));
            }
        }

//        int id = context.getResources().getIdentifier(itemAnswer.getIcon(), "drawable", context.getPackageName());
//        holder.ivAnswer.setImageDrawable(context.getResources().getDrawable(id));

        if (position == currentSelection) {
            holder.ivAnswer.setPadding(0, 0, 0, 0);
        } else {
            holder.ivAnswer.setPadding(originalPadding[0], originalPadding[1], originalPadding[2], originalPadding[3]);
        }
    }

    @Override
    public int getItemCount() {
        if (null != itemAnswers) {
            return itemAnswers.length;
        }
        return 0;
    }

    public void setSelection(int position) {
        currentSelection = position;
        ((AnswerCallback) context).onAnswerSelected(position);
        notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ImageView ivAnswer;

        ViewHolder(View itemView) {
            super(itemView);
            ivAnswer = (ImageView) itemView.findViewById(R.id.iv_answer);

            originalPadding[0] = ivAnswer.getPaddingLeft();
            originalPadding[1] = ivAnswer.getPaddingTop();
            originalPadding[2] = ivAnswer.getPaddingRight();
            originalPadding[3] = ivAnswer.getPaddingBottom();

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            currentSelection = getAdapterPosition();

            ((AnswerCallback) context).onAnswerSelected(currentSelection);

            notifyDataSetChanged();
        }
    }
}