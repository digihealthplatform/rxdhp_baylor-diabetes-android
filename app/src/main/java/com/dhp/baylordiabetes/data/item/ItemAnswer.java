package com.dhp.baylordiabetes.data.item;

import com.google.gson.annotations.SerializedName;


public class ItemAnswer {

    @SerializedName("answer")
    private String answer;

    @SerializedName("icon")
    private String icon;

    @SerializedName("questions")
    private ItemQuestion[] questions;

    public ItemAnswer(String answer, String icon) {
        this.answer = answer;
        this.icon = icon;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public ItemQuestion[] getQuestions() {
        return questions;
    }

    public void setQuestions(ItemQuestion[] questions) {
        this.questions = questions;
    }
}