package com.dhp.baylordiabetes.data.item;

import com.google.gson.annotations.SerializedName;


public class ItemUserSelectionGame {

    @SerializedName("game3")
    private String[] game3;

    public String[] getGame3() {
        return game3;
    }

    public void setGame3(String[] game3) {
        this.game3 = game3;
    }
}