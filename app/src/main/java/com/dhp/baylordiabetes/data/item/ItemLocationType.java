package com.dhp.baylordiabetes.data.item;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;


public class ItemLocationType {

    @SerializedName("id")
    private String id;

    @SerializedName("title")
    private String title;

    @SerializedName("locations")
    private ArrayList<ItemLocationName> locations;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ArrayList<ItemLocationName> getLocations() {
        return locations;
    }

    public void setLocations(ArrayList<ItemLocationName> locations) {
        this.locations = locations;
    }
}