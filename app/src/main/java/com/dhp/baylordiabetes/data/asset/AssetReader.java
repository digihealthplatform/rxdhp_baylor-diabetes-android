package com.dhp.baylordiabetes.data.asset;

import android.os.Environment;

import com.dhp.baylordiabetes.LApplication;
import com.dhp.baylordiabetes.R;
import com.dhp.baylordiabetes.data.DataManager;
import com.dhp.baylordiabetes.data.item.ColumnQuestionAnswer;
import com.dhp.baylordiabetes.data.item.ConsentData;
import com.dhp.baylordiabetes.data.item.ItemGameIteration;
import com.dhp.baylordiabetes.data.item.ItemLocationType;
import com.dhp.baylordiabetes.data.item.ItemQuestion;
import com.dhp.baylordiabetes.data.item.ItemResultDialog;
import com.dhp.baylordiabetes.data.item.ItemUserSelectionGame;
import com.dhp.baylordiabetes.data.item.SociodemographicData;
import com.dhp.baylordiabetes.data.item.VideoFile;
import com.dhp.baylordiabetes.data.item.VideoRating;
import com.dhp.baylordiabetes.data.table.TableSurveyData;
import com.dhp.baylordiabetes.data.table.TableSurveyData_Table;
import com.dhp.baylordiabetes.util.CryptoUtil;
import com.dhp.baylordiabetes.util.LLog;
import com.dhp.baylordiabetes.util.LToast;
import com.dhp.baylordiabetes.util.LUtils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.raizlabs.android.dbflow.sql.language.SQLite;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;

import static com.dhp.baylordiabetes.data.database.SharedPrefsHelper.KEY_COUNSELLING_COUNT_BATCH;
import static com.dhp.baylordiabetes.data.database.SharedPrefsHelper.KEY_COUNSELLING_COUNT_BATCH_TRAINER;
import static com.dhp.baylordiabetes.data.database.SharedPrefsHelper.KEY_VIDEO_COUNT_BATCH;
import static com.dhp.baylordiabetes.data.database.SharedPrefsHelper.KEY_VIDEO_COUNT_BATCH_TRAINER;
import static com.dhp.baylordiabetes.util.LConstants.ENGLISH_DATA_FILE;
import static com.dhp.baylordiabetes.util.LConstants.INPUT_JSON_PATH;
import static com.dhp.baylordiabetes.util.LConstants.LANGUAGE_FILE;


public class AssetReader {

    public static final String KEY_ICON_BBH    = "bbh_logo";
    public static final String KEY_ICON_BAYLOR = "baylor_logo";

    public static final String KEY_PRIMARY_INVESTIGATOR = "primary_investigator";
    public static final String KEY_CO_INVESTIGATORS     = "co_investigators";
    public static final String KEY_CREATIVE_DIRECTORS   = "creative_directors";
    public static final String KEY_CONSULTANTS          = "consultants";
    public static final String KEY_FUNDED               = "funded";
    public static final String KEY_NURSE_CONSULTANT     = "nurse_consultant";
    public static final String KEY_COPYRIGHT            = "copyright";

    public static final String KEY_DEVELOPED_BY = "developed_by";
    public static final String KEY_WEBSITE_URL  = "website_url";
    public static final String KEY_ANIMATION_BY = "animation_by";

    public static final String KEY_CORRECT_ANSWER_GIF   = "correct_answer_gif";
    public static final String KEY_WRONG_ANSWER_GIF     = "wrong_answer_gif";
    public static final String KEY_CORRECT_ANSWER_AUDIO = "correct_answer_sound";
    public static final String KEY_WRONG_ANSWER_AUDIO   = "wrong_answer_sound";

    private static int[] postQuestionQuestionAnswersIndex;

    private static long surveyStartTime;
    private static long surveyEndTime;

    private static String selectedLanguageString;

    private static String locationType;
    private static String locationName;

    public static final String KEY_YES    = "yes";
    public static final String KEY_NO     = "no";
    public static final String KEY_CANCEL = "cancel";
    public static final String KEY_WATCH  = "watch";
    public static final String KEY_SUBMIT = "submit";
    public static final String KEY_EXPORT = "export_data";

    public static final String KEY_CONFIRM_PROCEED   = "confirm_proceed";
    public static final String KEY_CONFIRM_START_NEW = "confirm_start_new";
    public static final String KEY_CONFIRM_ON_BACK   = "confirm_on_back";

    public static final String KEY_TITLE_CONSENT                   = "title_consent";
    public static final String KEY_TITLE_LOCATION                  = "title_location";
    public static final String KEY_TITLE_SOCIODEMOGRAPHIC_DATA     = "title_sociodemographic_data";
    public static final String KEY_START_PRE_QUESTIONS             = "start_pre_questions";
    public static final String KEY_START_PRE_QUESTIONS_AUDIO_FILE  = "start_pre_questions_audio_file";
    public static final String KEY_TITLE_PRE_QUESTIONS             = "title_pre_questions";
    public static final String KEY_TITLE_WATCH_VIDEO               = "title_watch_video";
    public static final String KEY_START_POST_QUESTIONS            = "start_post_questions";
    public static final String KEY_START_POST_QUESTIONS_AUDIO_FILE = "start_post_questions_audio_file";
    public static final String KEY_TITLE_POST_QUESTIONS            = "title_post_questions";
    public static final String KEY_TITLE_RESULTS                   = "title_results";
    public static final String KEY_TITLE_THANK_YOU                 = "title_thank_you";
    public static final String KEY_TITLE_VIDEO_RATING              = "video_rating_title";
    public static final String KEY_SELECT_VIDEO_RATING             = "please_select_rating";

    public static final String KEY_LOCATION_NAME               = "location_name";
    public static final String KEY_LOCATION_TYPE               = "location_type";
    public static final String KEY_SELECT_LOCATION_TYPE        = "message_select_location_type";
    public static final String KEY_ENTER_LOCATION_NAME         = "message_enter_location_name";
    public static final String KEY_ENTER_LOCATION_NAME_ENGLISH = "message_enter_location_name_in_english";
    public static final String KEY_CONTINUE                    = "continue";

    public static final String KEY_WRONG_ANSWER   = "wrong_answer";
    public static final String KEY_CORRECT_ANSWER = "correct_answer";

    public static final String KEY_VIEW_FULL_VIDEO   = "view_full_video";
    public static final String KEY_VIEW_MODULE_VIDEO = "view_module_video";
    public static final String KEY_START_SURVEY      = "start_survey";

    public static final String KEY_SELECT        = "select";
    public static final String KEY_ANSWER        = "answer";
    public static final String KEY_UPDATE_ANSWER = "update_answer";
    public static final String KEY_SELECT_ANSWER = "select_answer";

    public static final String KEY_DONE                 = "done";
    public static final String KEY_WATCH_FULL_VIDEO     = "watch_full_video";
    public static final String KEY_START_COUNSELLING    = "start_counselling";
    public static final String KEY_COUNSELLING_COMPLETE = "counselling_done";
    public static final String KEY_START_NEW            = "start_new";

    public static final String KEY_YOUR_SCORE = "your_score";

    private static JSONObject jsonAppData;
    private static JSONObject jsonData;
    private static JSONObject jsonDataEnglish;
    private static JSONObject jsonAppTexts;

    private static ArrayList<ColumnQuestionAnswer> socioDemographicQuestionAnswers;
    private static ArrayList<ColumnQuestionAnswer> preQuestionUserAnswers;
    private static ArrayList<ColumnQuestionAnswer> postQuestionUserAnswers;

    private static ItemUserSelectionGame itemUserSelectionGame;

    public static String surveyType;

    public static long consentStartDateTime;
    public static long sociodemographicStartDateTime;
    public static long preQuestionsStartDateTime;
    public static long videoOrCounsellingStartDateTime;
    public static long postQuestionsStartDateTime;
    public static long resultStartDateTime;
    public static long resultEndDateTime;

    public static long game1StartDateTime;
    public static long game1EndDateTime;
    public static long game2StartDateTime;
    public static long game2EndDateTime;
    public static long game3StartDateTime;
    public static long game3EndDateTime;
    public static long game4StartDateTime;
    public static long game4EndDateTime;

    public static String videoRating = "";

    public static int lastSurveyNumber = -1;

    private static String videoWatched;

    public static String getAppText(String key) {
        try {
            return jsonAppTexts.getString(key);
        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return "";
    }

    public static boolean init() {
        try {
            String content = CryptoUtil.decryptString(Environment.getExternalStorageDirectory() + INPUT_JSON_PATH + LANGUAGE_FILE);
            jsonAppData = new JSONObject(content);

            if (isJsonFilesValid()) {
                return true;
            }

        } catch (Exception e) {
            LLog.printStackTrace(e);
            LToast.error(LApplication.getContext().getString(R.string.invalid_apk));
        }

        return false;
    }

    public static String[] getLanguages() {
        try {
            JSONArray jsonArray = jsonAppData.getJSONArray("languages");

            String[] languages = new String[jsonArray.length()];

            for (int i = 0; i < jsonArray.length(); i++) {
                languages[i] = jsonArray.getJSONObject(i).getString("language");
            }

            return languages;

        } catch (JSONException e) {
            LLog.printStackTrace(e);
        }

        return null;
    }

    public static void setSelectedLanguage(int selectedLanguage) {

        String dataFileName = "";

        try {
            JSONArray jsonArray = jsonAppData.getJSONArray("languages");

            selectedLanguageString = jsonArray.getJSONObject(selectedLanguage).getString("language_text");
            dataFileName = jsonArray.getJSONObject(selectedLanguage).getString("data_file");

            String content = CryptoUtil.decryptString(Environment.getExternalStorageDirectory() + INPUT_JSON_PATH + dataFileName);
            jsonData = new JSONObject(content);

            content = CryptoUtil.decryptString(Environment.getExternalStorageDirectory() + INPUT_JSON_PATH + ENGLISH_DATA_FILE);
            jsonDataEnglish = new JSONObject(content);
            jsonAppTexts = jsonData.getJSONObject("app_texts");

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }
    }

    private static boolean isJsonFilesValid() {

        String dataFileName = "";

        try {
            JSONArray jsonArray = jsonAppData.getJSONArray("languages");

            for (int i = 0; i < jsonArray.length(); i++) {
                dataFileName = jsonArray.getJSONObject(i).getString("data_file");

                String content = CryptoUtil.decryptString(Environment.getExternalStorageDirectory() + INPUT_JSON_PATH + dataFileName);

                JSONObject jsonObject = new JSONObject(content);

                return true;
            }

        } catch (Exception e) {
            LToast.error(LApplication.getContext().getString(R.string.invalid_apk));
            LLog.printStackTrace(e);
        }

        return false;
    }

    public static JSONObject getJsonData() {
        return jsonData;
    }

    public static JSONObject getJsonDataEnglish() {
        return jsonDataEnglish;
    }

    public static ConsentData getConsent() {
        ConsentData consentData = new ConsentData();

        try {
            JSONObject jsonObject = jsonData.getJSONObject("consent");

            consentData.setTitle(jsonObject.getString("title"));
            JSONArray jsonArray = jsonObject.getJSONArray("points");

            String[] points = new String[jsonArray.length()];

            for (int i = 0; i < jsonArray.length(); i++) {
                points[i] = jsonArray.getString(i);
            }

            consentData.setPoints(points);
            consentData.setAudioFile(jsonObject.getString("audio_file"));
            consentData.setAgreeIcon(jsonObject.getString("agree_icon"));
            consentData.setCancelIcon(jsonObject.getString("cancel_icon"));

        } catch (JSONException e) {
            LLog.printStackTrace(e);
        }

        return consentData;
    }

    public static SociodemographicData getSocioDemographicQuestions(JSONObject jsonData) {
        SociodemographicData sociodemographicData = new SociodemographicData();

        try {
            JSONObject jsonObject = jsonData.getJSONObject("sociodemographic_data");

            GsonBuilder gsonBuilder = new GsonBuilder();
            Gson        gson        = gsonBuilder.create();
            sociodemographicData = gson.fromJson(jsonObject.toString(), SociodemographicData.class);

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return sociodemographicData;
    }

    public static ItemQuestion[] getPreQuestions(JSONObject jsonData) {
        ItemQuestion questions[] = null;

        try {
            JSONArray jsonArray = jsonData.getJSONArray("questionnaire");

            GsonBuilder gsonBuilder = new GsonBuilder();
            Gson        gson        = gsonBuilder.create();
            questions = gson.fromJson(jsonArray.toString(), ItemQuestion[].class);

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return questions;
    }

    public static ItemQuestion[] getPostQuestions(JSONObject jsonData) {
        return getPreQuestions(jsonData);
    }

    public static void setSocioDemographicAnswers(ArrayList<ColumnQuestionAnswer> questionAnswers) {
        socioDemographicQuestionAnswers = questionAnswers;
    }

    public static void setPreQuestionUserAnswers(ArrayList<ColumnQuestionAnswer> questionAnswers) {
        preQuestionUserAnswers = questionAnswers;
    }

    public static void setPostQuestionQuestionAnswers(ArrayList<ColumnQuestionAnswer> questionAnswers, int[] questionAnswersIndex) {
        postQuestionUserAnswers = questionAnswers;
        postQuestionQuestionAnswersIndex = questionAnswersIndex;
    }

    public static int[] getPostQuestionUserAnswers() {
        return postQuestionQuestionAnswersIndex;
    }

    public static void surveyStarted() {
        surveyStartTime = Calendar.getInstance().getTimeInMillis();
    }

    public static void surveyEnded() {
        surveyEndTime = Calendar.getInstance().getTimeInMillis();
    }

    public static void saveLocationDetails(String locationType, String locationName) {
        AssetReader.locationType = locationType;
        AssetReader.locationName = locationName;
    }

    public static void saveSurveyData(DataManager dataManager, String location) {
        TableSurveyData tableSurveyData = new TableSurveyData();

        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson        gson        = gsonBuilder.create();

        tableSurveyData.setSurveyDateTime(LUtils.getDateTime(surveyStartTime));

        tableSurveyData.setLocationType(locationType);
        tableSurveyData.setLocationName(locationName);
        tableSurveyData.setSurveyorName(dataManager.getLoggedInUsername());
        tableSurveyData.setTrainer(dataManager.isTrainer());
        tableSurveyData.setSelectedLanguage(selectedLanguageString);
        tableSurveyData.setSociodemographicData(gson.toJson(socioDemographicQuestionAnswers));
        tableSurveyData.setPreQuestions(gson.toJson(preQuestionUserAnswers));
        tableSurveyData.setPostQuestions(gson.toJson(postQuestionUserAnswers));
        tableSurveyData.setVideoWatched(videoWatched);

        if (surveyType.equals("Video")) {
            tableSurveyData.setSurveyType("1");
        } else {
            tableSurveyData.setSurveyType("2");
        }

        tableSurveyData.setConsentStartDateTime(consentStartDateTime);
        tableSurveyData.setSociodemographicStartDateTime(sociodemographicStartDateTime);
        tableSurveyData.setPreQuestionsStartDateTime(preQuestionsStartDateTime);
        tableSurveyData.setVideoOrCounsellingStartDateTime(videoOrCounsellingStartDateTime);
        tableSurveyData.setPostQuestionsStartDateTime(postQuestionsStartDateTime);

        tableSurveyData.setGame1StartDateTime(game1StartDateTime);
        tableSurveyData.setGame1EndDateTime(game1EndDateTime);

        tableSurveyData.setGame2StartDateTime(game2StartDateTime);
        tableSurveyData.setGame2EndDateTime(game2EndDateTime);

        tableSurveyData.setGame3StartDateTime(game3StartDateTime);
        tableSurveyData.setGame3EndDateTime(game3EndDateTime);

        tableSurveyData.setGame4StartDateTime(game4StartDateTime);
        tableSurveyData.setGame4EndDateTime(game4EndDateTime);

        if (null != itemUserSelectionGame) {
            tableSurveyData.setUserSelectionGame(gson.toJson(itemUserSelectionGame));
        }

        tableSurveyData.setGpsLocation(location);

        dataManager.saveSurveyData(tableSurveyData);

        if (dataManager.isTrainer()) {
            int surveyBatchVideoCount       = dataManager.getInt(KEY_VIDEO_COUNT_BATCH_TRAINER);
            int surveyBatchCounsellingCount = dataManager.getInt(KEY_COUNSELLING_COUNT_BATCH_TRAINER);

            if (surveyType.equals("Video")) {
                ++surveyBatchVideoCount;
            } else {
                ++surveyBatchCounsellingCount;
            }

            if ((surveyBatchVideoCount + surveyBatchCounsellingCount) >= AssetReader.getRandomBatchSizeTrainer()) {
                surveyBatchVideoCount = 0;
                surveyBatchCounsellingCount = 0;
            }

            dataManager.setInt(KEY_VIDEO_COUNT_BATCH_TRAINER, surveyBatchVideoCount);
            dataManager.setInt(KEY_COUNSELLING_COUNT_BATCH_TRAINER, surveyBatchCounsellingCount);

        } else {
            int surveyBatchVideoCount       = dataManager.getInt(KEY_VIDEO_COUNT_BATCH);
            int surveyBatchCounsellingCount = dataManager.getInt(KEY_COUNSELLING_COUNT_BATCH);

            if (surveyType.equals("Video")) {
                ++surveyBatchVideoCount;
            } else {
                ++surveyBatchCounsellingCount;
            }

            if ((surveyBatchVideoCount + surveyBatchCounsellingCount) >= AssetReader.getRandomBatchSize()) {
                surveyBatchVideoCount = 0;
                surveyBatchCounsellingCount = 0;
            }

            dataManager.setInt(KEY_VIDEO_COUNT_BATCH, surveyBatchVideoCount);
            dataManager.setInt(KEY_COUNSELLING_COUNT_BATCH, surveyBatchCounsellingCount);
        }

        clearValues();
    }

    private static void clearValues() {
        game1StartDateTime = 0;
        game1EndDateTime = 0;
        game2StartDateTime = 0;
        game2EndDateTime = 0;
        game3StartDateTime = 0;
        game3EndDateTime = 0;
        game4StartDateTime = 0;
        game4EndDateTime = 0;

        videoRating = "";
        videoWatched = "";
        itemUserSelectionGame = null;
    }

    public static void setItemUserSelectionGame(ItemUserSelectionGame item) {
        itemUserSelectionGame = item;
    }

    public static void updateResultDateTime() {
        if (-1 == lastSurveyNumber) {
            return;
        }

        try {
            TableSurveyData tableSurveyData = SQLite.select()
                    .from(TableSurveyData.class)
                    .where(TableSurveyData_Table.survey_number.eq(lastSurveyNumber))
                    .querySingle();

            if (null != tableSurveyData) {
                tableSurveyData.setVideoRating(videoRating);
                tableSurveyData.setResultStartDateTime(resultStartDateTime);
                tableSurveyData.setResultEndDateTime(resultEndDateTime);
                tableSurveyData.update();
            }

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        lastSurveyNumber = -1;
    }

    public static String getVideoFileName(JSONObject jsonData) {
        String videoFileName = "";

        try {
            videoFileName = jsonData.getString("video_file");
        } catch (JSONException e) {
            LLog.printStackTrace(e);
        }

        return videoFileName;
    }

    public static VideoFile[] getVideoFiles(JSONObject jsonData) {
        VideoFile videoFiles[] = null;

        try {
            JSONArray jsonArray = jsonData.getJSONArray("video_files");

            GsonBuilder gsonBuilder = new GsonBuilder();
            Gson        gson        = gsonBuilder.create();
            videoFiles = gson.fromJson(jsonArray.toString(), VideoFile[].class);

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return videoFiles;
    }

    public static void setVideoWatched(String title) {
        videoWatched = title;
    }

    public static ItemLocationType[] getLocationTypes() {
        ItemLocationType[] locationType = null;

        try {
            JSONArray jsonArray = jsonData.getJSONArray("location_types");

            GsonBuilder gsonBuilder = new GsonBuilder();
            Gson        gson        = gsonBuilder.create();
            locationType = gson.fromJson(jsonArray.toString(), ItemLocationType[].class);

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return locationType;
    }

    public static VideoRating[] getVideoRatings() {
        VideoRating[] videoRating = null;

        try {
            JSONArray jsonArray = jsonData.getJSONArray("video_rating");

            GsonBuilder gsonBuilder = new GsonBuilder();
            Gson        gson        = gsonBuilder.create();
            videoRating = gson.fromJson(jsonArray.toString(), VideoRating[].class);

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return videoRating;
    }

    public static ItemGameIteration[] getGameIterations() {
        ItemGameIteration[] itemGameIterations = null;

        try {
            JSONArray jsonArray = jsonData.getJSONObject("game_details").getJSONObject("game_3").getJSONArray("iterations");

            GsonBuilder gsonBuilder = new GsonBuilder();
            Gson        gson        = gsonBuilder.create();
            itemGameIterations = gson.fromJson(jsonArray.toString(), ItemGameIteration[].class);

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return itemGameIterations;
    }

    public static ItemResultDialog getResultDialog(JSONObject jsonData, int correctAnswersCount, int totalQuestions) {
        ItemResultDialog itemResultDialog = new ItemResultDialog();

        try {
            JSONArray jsonArray = jsonData.getJSONArray("result_message");

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);

                int rangeStart = jsonObject.getInt("range_start");
                int rangeEnd   = jsonObject.getInt("range_end");

                if (rangeStart <= correctAnswersCount && correctAnswersCount <= rangeEnd) {
                    itemResultDialog.setMessage(jsonObject.getString("message"));
                    itemResultDialog.setImage(jsonObject.getString("image"));

                    break;
                }
            }
        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return itemResultDialog;
    }

    public static String getStringValue(String keyName) {
        try {
            return jsonAppData.getString(keyName);
        } catch (Exception e) {
            LLog.printStackTrace(e);
        }
        return "";
    }

    public static String getStringValueGame(int gameNumber, String keyName) {
        try {
            return jsonData.getJSONObject("game_details").getJSONObject("game_" + gameNumber).getString(keyName);
        } catch (Exception e) {
            LLog.printStackTrace(e);
        }
        return "";
    }

    public static String getGameTitle(int gameNumber) {
        try {
            return jsonData.getJSONObject("game_details").getJSONObject("game_" + gameNumber).getString("title");
        } catch (Exception e) {
            LLog.printStackTrace(e);
        }
        return "";
    }

    public static String getGameGif(int gameNumber, int step) {
        try {
            return jsonData.getJSONObject("game_details").getJSONObject("game_" + gameNumber).getJSONArray("steps").getJSONObject(step - 1).getString("gif");
        } catch (Exception e) {
            LLog.printStackTrace(e);
        }
        return "";
    }

    public static String getGameVideo(int gameNumber, int step) {
        try {
            return jsonData.getJSONObject("game_details").getJSONObject("game_" + gameNumber).getJSONArray("steps").getJSONObject(step - 1).getString("video");
        } catch (Exception e) {
            LLog.printStackTrace(e);
        }
        return "";
    }

    public static String getGameAudio(int gameNumber, int step) {
        try {
            return jsonData.getJSONObject("game_details").getJSONObject("game_" + gameNumber).getJSONArray("steps").getJSONObject(step - 1).getString("audio");
        } catch (Exception e) {
            LLog.printStackTrace(e);
        }
        return "";
    }

    public static String getGameAudioFile(String audioFile) {
        try {
            return jsonData.getJSONObject("game_details").getString(audioFile);
        } catch (Exception e) {
            LLog.printStackTrace(e);
        }
        return "";
    }

    public static ArrayList<String> getGifOptions(int gameNumber) {

        try {
            ArrayList<String> options = new ArrayList<>();

            JSONArray jsonArray = jsonData.getJSONObject("game_details").getJSONObject("game_" + gameNumber).getJSONArray("game_answers");

            for (int i = 0; i < jsonArray.length(); i++) {
                options.add(jsonArray.getString(i));
            }

            return options;

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return null;
    }

    public static int getInt(String key) {
        try {
            return jsonAppData.getInt(key);
        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return 0;
    }

    public static String[] getCounsellingImages() {
        String[] images = null;

        try {
            JSONArray jsonArray = jsonData.getJSONArray("counselling_images");

            images = new String[jsonArray.length()];

            for (int i = 0; i < jsonArray.length(); i++) {
                images[i] = jsonArray.getString(i);
            }

        } catch (JSONException e) {
            LLog.printStackTrace(e);
        }

        return images;
    }

    public static boolean isToRandomise() {
        try {
            return jsonAppData.getBoolean("randomise");
        } catch (Exception e) {
            LLog.printStackTrace(e);
        }
        return false;
    }

    public static boolean isGamesEnabled() {
        try {
            return jsonAppData.getBoolean("enable_games");
        } catch (Exception e) {
            LLog.printStackTrace(e);
        }
        return false;
    }

    public static int getRandomBatchSize() {
        try {
            return jsonAppData.getInt("random_batch_size");
        } catch (Exception e) {
            LLog.printStackTrace(e);
        }
        return 10;
    }

    public static int getRandomBatchSizeTrainer() {
        try {
            return jsonAppData.getInt("random_batch_size_trainer");
        } catch (Exception e) {
            LLog.printStackTrace(e);
        }
        return 10;
    }

    public static boolean isToShowVideoLinkInResult() {
        try {
            return jsonAppData.getBoolean("show_full_video_in_result");
        } catch (Exception e) {
            LLog.printStackTrace(e);
        }
        return false;
    }

    public static boolean toShowStartPrePostQuestions() {
        try {
            return jsonAppData.getBoolean("show_start_pre_post_questions");
        } catch (Exception e) {
            LLog.printStackTrace(e);
        }
        return false;
    }
}