package com.dhp.baylordiabetes.data.item;


public class ConsentData {

    private String title;
    private String audioFile;
    private String agreeIcon;
    private String cancelIcon;

    private String[] points;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAudioFile() {
        return audioFile;
    }

    public void setAudioFile(String audioFile) {
        this.audioFile = audioFile;
    }

    public String getAgreeIcon() {
        return agreeIcon;
    }

    public void setAgreeIcon(String agreeIcon) {
        this.agreeIcon = agreeIcon;
    }

    public String getCancelIcon() {
        return cancelIcon;
    }

    public void setCancelIcon(String cancelIcon) {
        this.cancelIcon = cancelIcon;
    }

    public String[] getPoints() {
        return points;
    }

    public void setPoints(String[] points) {
        this.points = points;
    }
}
