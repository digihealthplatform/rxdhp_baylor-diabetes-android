package com.dhp.baylordiabetes.data.di.component;

import dagger.Component;
import com.dhp.baylordiabetes.data.di.PerActivity;
import com.dhp.baylordiabetes.data.di.module.ActivityModule;
import com.dhp.baylordiabetes.ui.admin.ActivityBaseAdminHome;
import com.dhp.baylordiabetes.ui.login.ActivityBaseCopyAssets;
import com.dhp.baylordiabetes.ui.login.ActivityBaseLogin;
import com.dhp.baylordiabetes.ui.login.ActivityBaseLoginCheck;
import com.dhp.baylordiabetes.ui.user.ActivityResult;
import com.dhp.baylordiabetes.ui.user.ActivityPostQuestions;
import com.dhp.baylordiabetes.ui.user.ActivityPreQuestions;
import com.dhp.baylordiabetes.ui.user.ActivityLanguageSelection;

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = ActivityModule.class)
public interface ActivityComponent {

    void inject(ActivityBaseLoginCheck activityLoginCheck);

    void inject(ActivityBaseLogin activityLogin);

    void inject(ActivityBaseAdminHome activityAdminHome);

    void inject(ActivityLanguageSelection activityLanguageSelection);

    void inject(ActivityPostQuestions activityPostQuestions);

    void inject(ActivityResult activityResult);

    void inject(ActivityBaseCopyAssets activityCopyAssets);

    void inject(ActivityPreQuestions activityPreQuestions);
}