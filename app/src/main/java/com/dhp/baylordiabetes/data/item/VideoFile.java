package com.dhp.baylordiabetes.data.item;

import com.google.gson.annotations.SerializedName;


public class VideoFile {

    @SerializedName("title")
    private String title;

    @SerializedName("file_name")
    private String fileName;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
}
