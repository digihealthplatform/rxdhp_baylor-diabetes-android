package com.dhp.baylordiabetes.data.table;

import com.dhp.baylordiabetes.data.database.BaylorDatabase;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;

import java.io.Serializable;


@Table(database = BaylorDatabase.class)
public class TableSurveyData extends BaseModel implements Serializable {

    @Column(name = "survey_number")
    @PrimaryKey(autoincrement = true)
    private int surveyNo;

    @Column(name = "selected_language")
    private String selectedLanguage = "";

    @Column(name = "sociodemographic_data")
    private String sociodemographicData = "";

    @Column(name = "pre_questions")
    private String preQuestions = "";

    @Column(name = "post_questions")
    private String postQuestions = "";

    @Column(name = "video_watched")
    private String videoWatched = "";

    @Column(name = "survey_type")
    private String surveyType = "";// Video or Counselling

    @Column(name = "video_rating")
    private String videoRating = "";

    @Column(name = "consent_start_date_time")
    private long consentStartDateTime;

    @Column(name = "sociodemographics_start_date_time")
    private long sociodemographicStartDateTime;

    @Column(name = "pre_questions_start_date_time")
    private long preQuestionsStartDateTime;

    @Column(name = "video_or_counselling_start_date_time")
    private long videoOrCounsellingStartDateTime;

    @Column(name = "game_1_start_date_time")
    private long game1StartDateTime;

    @Column(name = "game_1_end_date_time")
    private long game1EndDateTime;

    @Column(name = "game_2_start_date_time")
    private long game2StartDateTime;

    @Column(name = "game_2_end_date_time")
    private long game2EndDateTime;

    @Column(name = "game_3_start_date_time")
    private long game3StartDateTime;

    @Column(name = "game_3_end_date_time")
    private long game3EndDateTime;

    @Column(name = "game_4_start_date_time")
    private long game4StartDateTime;

    @Column(name = "game_4_end_date_time")
    private long game4EndDateTime;

    @Column(name = "user_selection_game")
    private String userSelectionGame = "";

    @Column(name = "post_questions_start_date_time")
    private long postQuestionsStartDateTime;

    @Column(name = "result_start_date_time")
    private long resultStartDateTime;

    @Column(name = "result_end_date_time")
    private long resultEndDateTime;

    @Column(name = "location_type")
    private String locationType = "";

    @Column(name = "location_name")
    private String locationName = "";

    @Column(name = "surveyor_name")
    private String surveyorName = "";

    @Column(name = "survey_date_time")
    private String surveyDateTime = "";

    @Column(name = "gps_location")
    private String gpsLocation = "";

    @Column(name = "trainer")
    private boolean trainer;

    @Column(name = "synced")
    private boolean synced;

    public int getSurveyNo() {
        return surveyNo;
    }

    public void setSurveyNo(int surveyNo) {
        this.surveyNo = surveyNo;
    }

    public String getSelectedLanguage() {
        return selectedLanguage;
    }

    public void setSelectedLanguage(String selectedLanguage) {
        this.selectedLanguage = selectedLanguage;
    }

    public String getSociodemographicData() {
        return sociodemographicData;
    }

    public void setSociodemographicData(String sociodemographicData) {
        this.sociodemographicData = sociodemographicData;
    }

    public String getPreQuestions() {
        return preQuestions;
    }

    public void setPreQuestions(String preQuestions) {
        this.preQuestions = preQuestions;
    }

    public String getPostQuestions() {
        return postQuestions;
    }

    public void setPostQuestions(String postQuestions) {
        this.postQuestions = postQuestions;
    }

    public String getVideoWatched() {
        return videoWatched;
    }

    public void setVideoWatched(String videoWatched) {
        this.videoWatched = videoWatched;
    }

    public String getSurveyType() {
        return surveyType;
    }

    public void setSurveyType(String surveyType) {
        this.surveyType = surveyType;
    }

    public String getVideoRating() {
        return videoRating;
    }

    public void setVideoRating(String videoRating) {
        this.videoRating = videoRating;
    }

    public long getConsentStartDateTime() {
        return consentStartDateTime;
    }

    public void setConsentStartDateTime(long consentStartDateTime) {
        this.consentStartDateTime = consentStartDateTime;
    }

    public long getSociodemographicStartDateTime() {
        return sociodemographicStartDateTime;
    }

    public void setSociodemographicStartDateTime(long sociodemographicStartDateTime) {
        this.sociodemographicStartDateTime = sociodemographicStartDateTime;
    }

    public long getPreQuestionsStartDateTime() {
        return preQuestionsStartDateTime;
    }

    public void setPreQuestionsStartDateTime(long preQuestionsStartDateTime) {
        this.preQuestionsStartDateTime = preQuestionsStartDateTime;
    }

    public long getVideoOrCounsellingStartDateTime() {
        return videoOrCounsellingStartDateTime;
    }

    public void setVideoOrCounsellingStartDateTime(long videoOrCounsellingStartDateTime) {
        this.videoOrCounsellingStartDateTime = videoOrCounsellingStartDateTime;
    }

    public long getGame1StartDateTime() {
        return game1StartDateTime;
    }

    public void setGame1StartDateTime(long game1StartDateTime) {
        this.game1StartDateTime = game1StartDateTime;
    }

    public long getGame1EndDateTime() {
        return game1EndDateTime;
    }

    public void setGame1EndDateTime(long game1EndDateTime) {
        this.game1EndDateTime = game1EndDateTime;
    }

    public long getGame2StartDateTime() {
        return game2StartDateTime;
    }

    public void setGame2StartDateTime(long game2StartDateTime) {
        this.game2StartDateTime = game2StartDateTime;
    }

    public long getGame2EndDateTime() {
        return game2EndDateTime;
    }

    public void setGame2EndDateTime(long game2EndDateTime) {
        this.game2EndDateTime = game2EndDateTime;
    }

    public long getGame3StartDateTime() {
        return game3StartDateTime;
    }

    public void setGame3StartDateTime(long game3StartDateTime) {
        this.game3StartDateTime = game3StartDateTime;
    }

    public long getGame3EndDateTime() {
        return game3EndDateTime;
    }

    public void setGame3EndDateTime(long game3EndDateTime) {
        this.game3EndDateTime = game3EndDateTime;
    }

    public long getGame4StartDateTime() {
        return game4StartDateTime;
    }

    public void setGame4StartDateTime(long game4StartDateTime) {
        this.game4StartDateTime = game4StartDateTime;
    }

    public long getGame4EndDateTime() {
        return game4EndDateTime;
    }

    public void setGame4EndDateTime(long game4EndDateTime) {
        this.game4EndDateTime = game4EndDateTime;
    }

    public String getUserSelectionGame() {
        return userSelectionGame;
    }

    public void setUserSelectionGame(String userSelectionGame) {
        this.userSelectionGame = userSelectionGame;
    }

    public long getPostQuestionsStartDateTime() {
        return postQuestionsStartDateTime;
    }

    public void setPostQuestionsStartDateTime(long postQuestionsStartDateTime) {
        this.postQuestionsStartDateTime = postQuestionsStartDateTime;
    }

    public long getResultStartDateTime() {
        return resultStartDateTime;
    }

    public void setResultStartDateTime(long resultStartDateTime) {
        this.resultStartDateTime = resultStartDateTime;
    }

    public long getResultEndDateTime() {
        return resultEndDateTime;
    }

    public void setResultEndDateTime(long resultEndDateTime) {
        this.resultEndDateTime = resultEndDateTime;
    }

    public String getLocationType() {
        return locationType;
    }

    public void setLocationType(String locationType) {
        this.locationType = locationType;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public String getSurveyorName() {
        return surveyorName;
    }

    public void setSurveyorName(String surveyorName) {
        this.surveyorName = surveyorName;
    }

    public String getSurveyDateTime() {
        return surveyDateTime;
    }

    public void setSurveyDateTime(String surveyDateTime) {
        this.surveyDateTime = surveyDateTime;
    }

    public String getGpsLocation() {
        return gpsLocation;
    }

    public void setGpsLocation(String gpsLocation) {
        this.gpsLocation = gpsLocation;
    }

    public boolean isTrainer() {
        return trainer;
    }

    public void setTrainer(boolean trainer) {
        this.trainer = trainer;
    }

    public boolean isSynced() {
        return synced;
    }

    public void setSynced(boolean synced) {
        this.synced = synced;
    }
}