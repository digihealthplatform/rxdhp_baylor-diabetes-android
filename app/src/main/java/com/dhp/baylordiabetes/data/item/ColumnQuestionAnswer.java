package com.dhp.baylordiabetes.data.item;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class ColumnQuestionAnswer implements Serializable {

    @SerializedName("question")
    String question;

    @SerializedName("answer")
    String answer;

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }
}
