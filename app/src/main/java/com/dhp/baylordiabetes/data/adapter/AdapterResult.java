package com.dhp.baylordiabetes.data.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.dhp.baylordiabetes.R;
import com.dhp.baylordiabetes.data.asset.AssetReader;
import com.dhp.baylordiabetes.data.item.ItemQuestion;
import com.dhp.baylordiabetes.data.item.VideoFile;
import com.dhp.baylordiabetes.ui.user.ActivityVideoPlay;

import java.io.File;

import static com.dhp.baylordiabetes.util.LUtils.getCorrectAnswerIndex;
import static com.dhp.baylordiabetes.util.LUtils.getImagePath;


public class AdapterResult extends RecyclerView.Adapter<AdapterResult.ViewHolder> {

    private int[] postQuestionAnswers;

    private ItemQuestion[] questions;

    private final Context        context;
    private final LayoutInflater layoutInflater;

    public AdapterResult(Context context, ItemQuestion[] questions, int[] postQuestionAnswers) {
        this.context = context;
        this.questions = questions;
        this.postQuestionAnswers = postQuestionAnswers;
        this.layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public AdapterResult.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.item_result, parent, false);
        return new AdapterResult.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AdapterResult.ViewHolder holder, int position) {
        holder.tvQuestionNo.setText("" + (position + 1) + ".");
        holder.tvQuestion.setText(questions[position].getQuestion());

        if (null != questions[position].getAnswersInPopup()
                && questions[position].getAnswersInPopup().toLowerCase().equals("yes")) {

            File correctAnsFile = new File(getImagePath(questions[position].getAnswers()[getCorrectAnswerIndex(questions[position])].getIcon()));

            File userAnsFile = new File(getImagePath(questions[position].getAnswers()[postQuestionAnswers[position]].getIcon()));


            if (questions[position].getCorrectAnswer().equals(questions[position].getAnswers()[postQuestionAnswers[position]].getAnswer())) {
                holder.btnPlay.setVisibility(View.GONE);
                holder.llWrongAnswer.setVisibility(View.GONE);

            } else {
                Uri imageUri = Uri.fromFile(userAnsFile);
                loadImage(imageUri, holder.ivUserAnswer);

                holder.llWrongAnswer.setVisibility(View.VISIBLE);
                holder.btnPlay.setVisibility(View.VISIBLE);
            }

            Uri imageUri = Uri.fromFile(correctAnsFile);
            loadImage(imageUri, holder.ivCorrectAnswer);

        } else {
            File correctAnsFile = new File(getImagePath(questions[position].getAnswers()[getCorrectAnswerIndex(questions[position])].getIcon()));

            File userAnsFile = new File(getImagePath(questions[position].getAnswers()[postQuestionAnswers[position]].getIcon()));

            if (questions[position].getCorrectAnswer().equals(questions[position].getAnswers()[postQuestionAnswers[position]].getAnswer())) {
                holder.btnPlay.setVisibility(View.GONE);
                holder.llWrongAnswer.setVisibility(View.GONE);

            } else {
                holder.ivUserAnswer.setImageBitmap(BitmapFactory.decodeFile(userAnsFile.getAbsolutePath()));
                holder.llWrongAnswer.setVisibility(View.VISIBLE);
                holder.btnPlay.setVisibility(View.VISIBLE);
            }

            holder.ivCorrectAnswer.setImageBitmap(BitmapFactory.decodeFile(correctAnsFile.getAbsolutePath()));
        }
    }

    private void loadImage(Uri imageUri, ImageView imageView) {

        if (imageUri.getPath().endsWith(".gif")) {
            Glide.with(context)
                    .load(imageUri)
                    .asGif()
                    .placeholder(R.drawable.loader)
                    .into(imageView);
        } else {
            Glide.with(context)
                    .load(imageUri)
                    .placeholder(R.drawable.ic_panorama_black_24dp)
                    .into(imageView);
        }
    }

    @Override
    public int getItemCount() {
        return questions.length;
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView     tvQuestion;
        private TextView     tvQuestionNo;
        private Button       btnPlay;
        private ImageView    ivCorrectAnswer;
        private ImageView    ivUserAnswer;
        private LinearLayout llWrongAnswer;
        private TextView     tvWrongAnswer;
        private TextView     tvCorrectAnswer;

        ViewHolder(View itemView) {
            super(itemView);
            tvQuestionNo = (TextView) itemView.findViewById(R.id.tv_question_no);
            tvQuestion = (TextView) itemView.findViewById(R.id.tv_question);
            btnPlay = (Button) itemView.findViewById(R.id.btn_play);
            ivUserAnswer = (ImageView) itemView.findViewById(R.id.iv_user_answer);
            ivCorrectAnswer = (ImageView) itemView.findViewById(R.id.iv_correct_answer);
            llWrongAnswer = (LinearLayout) itemView.findViewById(R.id.ll_wrong_answer);
            tvWrongAnswer = (TextView) itemView.findViewById(R.id.tv_wrong_answer);
            tvCorrectAnswer = (TextView) itemView.findViewById(R.id.tv_correct_answer);

            btnPlay.setOnClickListener(this);

            tvWrongAnswer.setText(AssetReader.getAppText(AssetReader.KEY_WRONG_ANSWER));
            tvCorrectAnswer.setText(AssetReader.getAppText(AssetReader.KEY_CORRECT_ANSWER));
            btnPlay.setText(AssetReader.getAppText(AssetReader.KEY_WATCH));
        }

        @Override
        public void onClick(View view) {
            Intent intent = new Intent(context, ActivityVideoPlay.class);
            intent.putExtra("watch_chunk", true);

            String title = questions[getAdapterPosition()].getTitle();

            final VideoFile[] videoFiles = AssetReader.getVideoFiles(AssetReader.getJsonData());

            for (int i = 0; i < videoFiles.length; i++) {
                if (videoFiles[i].getTitle().equals(title)) {
                    intent.putExtra("video_file", videoFiles[i].getFileName());
                    break;
                }
            }

            context.startActivity(intent);
        }
    }
}