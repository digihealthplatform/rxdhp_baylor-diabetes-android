package com.dhp.baylordiabetes.data.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.dhp.baylordiabetes.R;
import com.dhp.baylordiabetes.data.DataManager;
import com.dhp.baylordiabetes.data.table.TableUser;
import com.dhp.baylordiabetes.util.LToast;

import java.util.List;

import es.dmoral.toasty.Toasty;


public class AdapterUser extends RecyclerView.Adapter<AdapterUser.ViewHolder> {

    private List<TableUser> tableUsers;

    private final Context        context;
    private final LayoutInflater layoutInflater;

    private TextView     tvEmpty;
    private RecyclerView rvUserList;

    private DataManager dataManager;

    public AdapterUser(Context context, DataManager dataManager, RecyclerView rvUserList, TextView tvEmpty) {
        this.context = context;
        this.dataManager = dataManager;

        layoutInflater = LayoutInflater.from(context);

        this.rvUserList = rvUserList;
        this.tvEmpty = tvEmpty;

        showHideEmpty();

        tableUsers = dataManager.getAllUsers();
    }

    @Override
    public AdapterUser.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.item_user, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.tvUserName.setText(tableUsers.get(position).getFullName());
    }

    @Override
    public int getItemCount() {
        tableUsers = dataManager.getAllUsers();

        if (null != tableUsers) {
            return tableUsers.size();
        }

        return 0;
    }

    private void showHideEmpty() {
        if (0 == dataManager.getAllUsers().size()) {
            tvEmpty.setVisibility(View.VISIBLE);
            rvUserList.setVisibility(View.GONE);
        } else {
            tvEmpty.setVisibility(View.GONE);
            rvUserList.setVisibility(View.VISIBLE);
        }
    }

    private void removeItem(int position) {
        dataManager.removeUser(tableUsers.get(position));
        tableUsers.remove(position);
        notifyItemRemoved(position);
        Toasty.success(context, context.getString(R.string.msg_user_removed), Toast.LENGTH_SHORT).show();

        showHideEmpty();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnLongClickListener {

        private TextView tvUserName;

        ViewHolder(View itemView) {
            super(itemView);

            tvUserName = (TextView) itemView.findViewById(R.id.tv_user_name);

            itemView.setOnLongClickListener(this);
        }

        @Override
        public boolean onLongClick(View view) {

            String options_array[] = {context.getString(R.string.option_edit), context.getString(R.string.option_remove)};

            AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.AlertDialogLight);

            builder.setItems(options_array, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {

                    switch (which) {
                        case 0: {
                            updateUser(tableUsers.get(getAdapterPosition()));
                        }
                        break;

                        case 1: {
                            AlertDialog.Builder builder;

                            builder = new AlertDialog.Builder(context, R.style.AlertDialogLight);

                            builder.setMessage(context.getString(R.string.confirm_remove_user));
                            builder.setPositiveButton(context.getString(R.string.btn_remove), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    removeItem(getAdapterPosition());
                                }
                            });
                            builder.setNegativeButton(context.getString(R.string.btn_cancel), null);
                            builder.show();
                        }
                        break;
                    }
                }
            });

            builder.show();

            return true;
        }
    }

    private void updateUser(final TableUser tableUser) {
        View dialogView = LayoutInflater.from(context).inflate(R.layout.dialog_add_user, null);

        AlertDialog.Builder builder;

        builder = new AlertDialog.Builder(context, R.style.AlertDialogLight);

        builder.setView(dialogView);

        builder.setPositiveButton(context.getString(R.string.btn_save), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        builder.setNegativeButton(context.getString(R.string.btn_cancel), null);

        final AlertDialog dialog = builder.create();
        dialog.show();
        dialog.setCanceledOnTouchOutside(false);

        final EditText etFullName = (EditText) dialogView.findViewById(R.id.et_full_name);
        etFullName.setText(tableUser.getFullName());
        etFullName.setEnabled(false);

        final EditText etUsername = (EditText) dialogView.findViewById(R.id.et_username);
        etUsername.setText(tableUser.getUsername());
        etUsername.setEnabled(false);

        final EditText etPassword = (EditText) dialogView.findViewById(R.id.et_password);
        etPassword.setText(tableUser.getPassword());

        final CheckBox cbTrainer = (CheckBox) dialogView.findViewById(R.id.cb_trainer);
        cbTrainer.setChecked(tableUser.isTrainer());

        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String password = etPassword.getText().toString();

                if (!password.equals("")) {

                    if (tableUser.getPassword().equals(password)
                            && tableUser.isTrainer() == cbTrainer.isChecked()) {
                        dialog.dismiss();
                        return;
                    }

                    tableUser.setPassword(password);
                    tableUser.setTrainer(cbTrainer.isChecked());

                    dataManager.updateUser(tableUser);

                    tableUsers = dataManager.getAllUsers();

                    notifyDataSetChanged();

                    dialog.dismiss();
                    LToast.success(context.getString(R.string.msg_user_updated));

                } else {
                    LToast.warning(context.getString(R.string.msg_enter_password));
                }
            }
        });
    }
}