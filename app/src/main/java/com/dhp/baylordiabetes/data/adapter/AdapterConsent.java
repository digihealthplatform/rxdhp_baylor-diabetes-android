package com.dhp.baylordiabetes.data.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bluejamesbond.text.DocumentView;
import com.dhp.baylordiabetes.R;


public class AdapterConsent extends RecyclerView.Adapter<AdapterConsent.ViewHolder> {

    private final String[] consent;

    private final LayoutInflater layoutInflater;

    public AdapterConsent(Context context, String[] consent) {
        this.consent = consent;
        this.layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public AdapterConsent.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.item_consent, parent, false);
        return new AdapterConsent.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AdapterConsent.ViewHolder holder, int position) {
        holder.tvConsent.setText(consent[position]);
    }

    @Override
    public int getItemCount() {
        if (null != consent) {
            return consent.length;
        }
        return 0;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        //        private TextView tvConsent;
        private DocumentView tvConsent;

        ViewHolder(View itemView) {
            super(itemView);
            tvConsent = (DocumentView) itemView.findViewById(R.id.tv_consent);
        }
    }
}