package com.dhp.baylordiabetes.data.item;

import com.google.gson.annotations.SerializedName;


public class ItemLocationName {

    @SerializedName("id")
    private String id;

    @SerializedName("title")
    private String title;

    public ItemLocationName(String id, String title) {
        this.id = id;
        this.title = title;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}