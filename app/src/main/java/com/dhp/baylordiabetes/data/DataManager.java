package com.dhp.baylordiabetes.data;

import android.content.Context;

import com.dhp.baylordiabetes.BuildConfig;
import com.dhp.baylordiabetes.data.database.DatabaseHelper;
import com.dhp.baylordiabetes.data.database.SharedPrefsHelper;
import com.dhp.baylordiabetes.data.di.ApplicationContext;
import com.dhp.baylordiabetes.data.table.TableSurveyData;
import com.dhp.baylordiabetes.data.table.TableUser;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import static com.dhp.baylordiabetes.data.database.SharedPrefsHelper.ADMIN_LOGGED_IN;
import static com.dhp.baylordiabetes.data.database.SharedPrefsHelper.KEY_ASSETS_COPIED_VERSION;
import static com.dhp.baylordiabetes.data.database.SharedPrefsHelper.KEY_LOGGED_IN;
import static com.dhp.baylordiabetes.data.database.SharedPrefsHelper.KEY_LOGGED_IN_USERNAME;
import static com.dhp.baylordiabetes.data.database.SharedPrefsHelper.KEY_REMEMBER_LOG_IN;
import static com.dhp.baylordiabetes.data.database.SharedPrefsHelper.NOT_LOGGED_IN;
import static com.dhp.baylordiabetes.data.database.SharedPrefsHelper.USER_LOGGED_IN;

@Singleton
public class DataManager {

    private DatabaseHelper    databaseHelper;
    private SharedPrefsHelper sharedPrefsHelper;

    @Inject
    public DataManager(@ApplicationContext Context context,
                       DatabaseHelper databaseHelper,
                       SharedPrefsHelper sharedPrefsHelper) {
        this.databaseHelper = databaseHelper;
        this.sharedPrefsHelper = sharedPrefsHelper;
    }

    public boolean createUser(TableUser tableUser) {
        return databaseHelper.createUser(tableUser);
    }

    public boolean updateUser(TableUser tableUser) {
        return databaseHelper.updateUser(tableUser);
    }

    public TableUser getUser(String userName) {
        return databaseHelper.getUser(userName);
    }

    public List<TableUser> getAllUsers() {
        return databaseHelper.getAllUsers();
    }

    public boolean removeUser(TableUser tableUser) {
        return databaseHelper.removeUser(tableUser);
    }

    public boolean saveSurveyData(TableSurveyData tableSurveyData) {
        return databaseHelper.createSurveyData(tableSurveyData);
    }

    public List<TableSurveyData> getSurveyData() {
        return databaseHelper.getSurveyData();
    }

    public boolean isSurveyDataPresent() {
        return databaseHelper.isSurveyDataPresent();
    }

    public void setPref(String key, int value) {
        sharedPrefsHelper.setPref(key, value);
    }

    public void setPref(String key, boolean value) {
        sharedPrefsHelper.setPref(key, value);
    }

    public void setPref(String key, String value) {
        sharedPrefsHelper.setPref(key, value);
    }

    public int getIntPref(String key, int defaultValue) {
        return sharedPrefsHelper.getIntPref(key, defaultValue);
    }

    public boolean getBooleanPref(String key, boolean defaultValue) {
        return sharedPrefsHelper.getBooleanPref(key, defaultValue);
    }

    public String getStringPref(String key, String defaultValue) {
        return sharedPrefsHelper.getStringPref(key, defaultValue);
    }

    public boolean validateUserLogin(String username, String password) {
        TableUser tableUser = getUser(username);
        return null != tableUser && password.equals(tableUser.getPassword());
    }

    public void adminLoggedIn(boolean rememberLogin) {
        sharedPrefsHelper.setPref(KEY_LOGGED_IN, ADMIN_LOGGED_IN);
        sharedPrefsHelper.setPref(KEY_REMEMBER_LOG_IN, rememberLogin);
    }

    public void userLoggedIn(boolean rememberLogin, String username) {
        sharedPrefsHelper.setPref(KEY_LOGGED_IN, USER_LOGGED_IN);
        sharedPrefsHelper.setPref(KEY_REMEMBER_LOG_IN, rememberLogin);

        TableUser tableUser = getUser(username);
        sharedPrefsHelper.setPref(KEY_LOGGED_IN_USERNAME, tableUser.getUsername());
    }

    public String getLoggedInUsername() {
        return sharedPrefsHelper.getStringPref(KEY_LOGGED_IN_USERNAME, "");
    }

    public boolean isTrainer() {
        TableUser tableUser = getUser(getLoggedInUsername());
        return null != tableUser && tableUser.isTrainer();
    }

    public void logout() {
        sharedPrefsHelper.setPref(KEY_LOGGED_IN, NOT_LOGGED_IN);
        sharedPrefsHelper.setPref(KEY_REMEMBER_LOG_IN, false);
        sharedPrefsHelper.setPref(KEY_LOGGED_IN_USERNAME, null);
    }

    public boolean isCopyAssetsNeeded() {
        return BuildConfig.VERSION_CODE > sharedPrefsHelper.getIntPref(KEY_ASSETS_COPIED_VERSION, 0);

    }

    public void onAssetsCopied() {
        sharedPrefsHelper.setPref(KEY_ASSETS_COPIED_VERSION, BuildConfig.VERSION_CODE);
    }

    public void setInt(String key, int value) {
        sharedPrefsHelper.setPref(key, value);
    }

    public int getInt(String key) {
        return sharedPrefsHelper.getIntPref(key, 0);
    }
}