package com.dhp.baylordiabetes.data.database;

import android.content.Context;
import android.util.Log;

import com.dhp.baylordiabetes.data.asset.AssetReader;
import com.dhp.baylordiabetes.data.di.ApplicationContext;
import com.dhp.baylordiabetes.data.di.DatabaseInfo;
import com.dhp.baylordiabetes.data.table.TableSurveyData;
import com.dhp.baylordiabetes.data.table.TableSurveyData_Table;
import com.dhp.baylordiabetes.data.table.TableUser;
import com.dhp.baylordiabetes.data.table.TableUser_Table;
import com.dhp.baylordiabetes.util.LLog;
import com.raizlabs.android.dbflow.sql.language.SQLite;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;


@Singleton
public class DatabaseHelper {

    private Context context;

    @Inject
    DatabaseHelper(@ApplicationContext Context context,
                   @DatabaseInfo String dbName,
                   @DatabaseInfo Integer version) {
        this.context = context;
    }

    public boolean createUser(TableUser tableUser) {
        try {
            tableUser.save();
            return true;
        } catch (Exception e) {
            LLog.printStackTrace(e);
        }
        return false;
    }

    public boolean updateUser(TableUser tableUser) {
        try {
            tableUser.update();
            return true;
        } catch (Exception e) {
            LLog.printStackTrace(e);
        }
        return false;
    }

    public TableUser getUser(String userName) {
        try {
            return SQLite.select().from(TableUser.class).where(TableUser_Table.username.eq(userName)).querySingle();
        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return new TableUser();
    }

    public boolean removeUser(TableUser tableUser) {
        try {
            tableUser.delete();
            return true;
        } catch (Exception e) {
            LLog.printStackTrace(e);
        }
        return false;
    }

    public List<TableUser> getAllUsers() {
        try {
            return SQLite.select().from(TableUser.class).queryList();
        } catch (Exception e) {
            LLog.printStackTrace(e);
        }
        return new ArrayList<>();
    }

    public boolean createSurveyData(TableSurveyData tableSurveyData) {
        try {
            tableSurveyData.save();

            AssetReader.lastSurveyNumber = tableSurveyData.getSurveyNo();

            return true;

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }
        return false;
    }

    public List<TableSurveyData> getSurveyData() {
        try {
            List<TableSurveyData> tableSurveyData = SQLite.select()
                    .from(TableSurveyData.class)
                    .where(TableSurveyData_Table.trainer.eq(false))
                    .queryList();

            Log.d("", "getSurveyData: ");

            return tableSurveyData;

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return null;
    }

    public boolean isSurveyDataPresent() {
        try {
            TableSurveyData tableSurveyData = SQLite.select()
                    .from(TableSurveyData.class)
                    .querySingle();

            if (null != tableSurveyData) {
                return true;
            }

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return false;
    }
}