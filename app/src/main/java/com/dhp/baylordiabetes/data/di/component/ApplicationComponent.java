package com.dhp.baylordiabetes.data.di.component;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Component;
import com.dhp.baylordiabetes.LApplication;
import com.dhp.baylordiabetes.data.DataManager;
import com.dhp.baylordiabetes.data.di.ApplicationContext;
import com.dhp.baylordiabetes.data.di.module.ApplicationModule;

@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {

    void inject(LApplication lApplication);

    @ApplicationContext
    Context getContext();

    DataManager getDataManager();
}