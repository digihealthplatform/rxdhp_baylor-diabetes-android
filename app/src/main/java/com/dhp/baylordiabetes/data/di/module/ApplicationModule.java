package com.dhp.baylordiabetes.data.di.module;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import dagger.Module;
import dagger.Provides;
import com.dhp.baylordiabetes.data.database.BaylorDatabase;
import com.dhp.baylordiabetes.data.di.ApplicationContext;
import com.dhp.baylordiabetes.data.di.DatabaseInfo;
import com.dhp.baylordiabetes.data.database.SharedPrefsHelper;

@Module
public class ApplicationModule {
    private final Application application;

    public ApplicationModule(Application app) {
        application = app;
    }

    @Provides
    @ApplicationContext
    Context provideContext() {
        return application;
    }

    @Provides
    Application provideApplication() {
        return application;
    }

    @Provides
    @DatabaseInfo
    String provideDatabaseName() {
        return BaylorDatabase.DATABASE_NAME;
    }

    @Provides
    @DatabaseInfo
    Integer provideDatabaseVersion() {
        return 1;
    }

    @Provides
    SharedPreferences provideSharedPrefs() {
        return application.getSharedPreferences(SharedPrefsHelper.SHARED_PREFS_NAME, Context.MODE_PRIVATE);
    }
}