package com.dhp.baylordiabetes.util;

import android.content.Context;
import android.net.Uri;
import android.widget.ImageView;
import android.widget.Spinner;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.Arrays;

import com.dhp.baylordiabetes.R;
import com.dhp.baylordiabetes.data.adapter.AdapterSpinner;


public class LView {

    public static void setSpinnerAdapter(Context context, Spinner spinner, String[] list) {
        final AdapterSpinner<String> adapterSpinner = new AdapterSpinner<>(
                context,
                R.layout.spinner_layout,
                new ArrayList<>(Arrays.asList(list))
        );
        adapterSpinner.setDropDownViewResource(R.layout.spinner_layout);
        spinner.setAdapter(adapterSpinner);
    }

    public static void loadImage(Context context, Uri imageUri, ImageView imageView) {

        if (imageUri.getPath().endsWith(".gif")) {
            Glide.with(context)
                    .load(imageUri)
                    .asGif()
                    .placeholder(R.drawable.loader)
                    .into(imageView);
        } else {
            Glide.with(context)
                    .load(imageUri)
                    .placeholder(R.drawable.ic_panorama_black_24dp)
                    .into(imageView);
        }
    }
}