package com.dhp.baylordiabetes.util;

import android.widget.Toast;

import es.dmoral.toasty.Toasty;
import com.dhp.baylordiabetes.LApplication;

public class LToast {

    public static void success(String message) {
        Toasty.success(LApplication.getContext(), message, Toast.LENGTH_LONG).show();
    }

    public static void warning(String message) {
        Toasty.warning(LApplication.getContext(), message, Toast.LENGTH_LONG).show();
    }

    public static void error(String message) {
        Toasty.error(LApplication.getContext(), message, Toast.LENGTH_LONG).show();
    }
}