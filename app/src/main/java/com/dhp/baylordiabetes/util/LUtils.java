package com.dhp.baylordiabetes.util;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.widget.Toast;

import com.dhp.baylordiabetes.R;
import com.dhp.baylordiabetes.data.asset.AssetReader;
import com.dhp.baylordiabetes.data.item.ItemAnswer;
import com.dhp.baylordiabetes.data.item.ItemQuestion;
import com.dhp.baylordiabetes.data.item.ItemUserSelectionGame;
import com.dhp.baylordiabetes.data.table.TableSurveyData;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.opencsv.CSVWriter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import es.dmoral.toasty.Toasty;

import static com.dhp.baylordiabetes.util.LConstants.GIF_PATH;
import static com.dhp.baylordiabetes.util.LConstants.ICONS_PATH;

public class LUtils {

    private static ArrayList<String> prePostQuestions;
    private static ArrayList<String> prePostQuestionAnswers;
    private static ArrayList<String> socioQuestions;

    public static void saveFile(Context context, List<TableSurveyData> itemTableSurveyData) {

        if (null == itemTableSurveyData || 0 == itemTableSurveyData.size()) {
            Toasty.warning(context, context.getString(R.string.message_no_data),
                    Toast.LENGTH_SHORT).show();
            return;
        }

        String fileName = exportCsv(itemTableSurveyData);
        Toasty.success(context, "Exported successfully").show();

        Uri    path        = Uri.fromFile(new File(fileName));
        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.setType("vnd.android.cursor.dir/email");

        emailIntent.putExtra(Intent.EXTRA_STREAM, path);

        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Baylor - Survey Data:");
        context.startActivity(Intent.createChooser(emailIntent, "Send email..."));
    }

    private static String getCsvFilePath() {
        File outputPath = new File(Environment.getExternalStorageDirectory(), LConstants.OUTPUT_JSON_PATH);

        String fileName   = "Survey_data_" + getFileName();
        int    i          = 1;
        File   outputFile = new File(outputPath.getPath(), fileName + ".csv");

        while (outputFile.exists()) {
            outputFile = new File(outputPath.getPath(), fileName + "(" + i++ + ").csv");
        }

        return outputFile.getPath();
    }

    private static String exportCsv(List<TableSurveyData> itemTableSurveyData) {
        try {
            String filePath = getCsvFilePath();

            CSVWriter writer = new CSVWriter(new FileWriter(filePath));
            writeHeaders(writer, itemTableSurveyData);
            writeData(writer, itemTableSurveyData);
            writer.close();

            return filePath;
        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return "";
    }

    private static void writeHeaders(CSVWriter writer, List<TableSurveyData> itemTableSurveyData) {
        ArrayList<String> headers = new ArrayList<>(32);
        headers.add("Survey No.");
        headers.add("Survey Date Time");
        headers.add("Location Type");
        headers.add("Location Name");
        headers.add("Surveyor Name");
        headers.add("Selected Language");

        headers.add("Survey Type");

        headers.add("Video Watched");
        headers.add("Video Rating");

        headers.add("Consent Start Date Time");
        headers.add("Sociodemographics Start Date Time");
        headers.add("Pre Awareness Start Date Time");
        headers.add("Video/Counselling Start Date Time");
        headers.add("Post Awareness Start Date Time");
        headers.add("Results Start Date Time");
        headers.add("Results End Date Time");

        socioQuestions = new ArrayList<>(16);
        prePostQuestions = new ArrayList<>(16);
        prePostQuestionAnswers = new ArrayList<>(16);

        ItemQuestion[] questions = AssetReader.getSocioDemographicQuestions(AssetReader.getJsonDataEnglish()).getQuestions();

        for (int i = 0; i < questions.length; i++) {
            headers.add(questions[i].getQuestion());
            socioQuestions.add(questions[i].getQuestion());
            // Nested question

            ItemAnswer[] itemAnswers = questions[i].getAnswers();
            if (null == itemAnswers) {
                continue;
            }

            for (int j = 0; j < itemAnswers.length; j++) {
                ItemQuestion[] itemQuestions = itemAnswers[j].getQuestions();

                if (null != itemQuestions) {
                    headers.add(itemQuestions[0].getQuestion());
                    socioQuestions.add(itemQuestions[0].getQuestion());
                }
            }
        }

        questions = AssetReader.getPreQuestions(AssetReader.getJsonDataEnglish());
        for (ItemQuestion question1 : questions) {
            headers.add("Correct Answer: " + question1.getQuestion());
            prePostQuestions.add(question1.getQuestion());
            prePostQuestionAnswers.add(question1.getAnswers()[getCorrectAnswerIndex(question1)].getAnswer());
        }

        for (ItemQuestion question : questions) {
            headers.add("Pre Awareness Answer: " + question.getQuestion());
        }

        for (ItemQuestion question : questions) {
            headers.add("Post Awareness Answer: " + question.getQuestion());
        }

        headers.add("Pre Awareness Result");
        headers.add("Post Awareness Result");

        headers.add("Game - 1 Start Date Time");
        headers.add("Game - 1 End Date Time");
        headers.add("Game - 2 Start Date Time");
        headers.add("Game - 2 End Date Time");
        headers.add("Game - 3 Start Date Time");
        headers.add("Game - 3 End Date Time");
        headers.add("Game - 4 Start Date Time");
        headers.add("Game - 4 End Date Time");

        headers.add("User selection Game - 3, Iteration - 1");
        headers.add("User selection Game - 3, Iteration - 2");
        headers.add("User selection Game - 3, Iteration - 3");
        headers.add("GPS Location");

        String[] data = new String[headers.size()];
        headers.toArray(data);
        writer.writeNext(data);
    }

    private static void writeData(CSVWriter writer, List<TableSurveyData> itemTableSurveyData) {
        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson        gson        = gsonBuilder.create();

        for (TableSurveyData tableSurveyData : itemTableSurveyData) {

            ArrayList<String> headers = new ArrayList<>(32);

            headers.add("" + tableSurveyData.getSurveyNo());
            headers.add(tableSurveyData.getSurveyDateTime());
            headers.add(tableSurveyData.getLocationType());
            headers.add(tableSurveyData.getLocationName());
            headers.add(tableSurveyData.getSurveyorName());
            headers.add(tableSurveyData.getSelectedLanguage());
            headers.add(tableSurveyData.getSurveyType());
            headers.add(tableSurveyData.getVideoWatched());
            headers.add(tableSurveyData.getVideoRating());

            headers.add(LUtils.getDateTime(tableSurveyData.getConsentStartDateTime()));
            headers.add(LUtils.getDateTime(tableSurveyData.getSociodemographicStartDateTime()));
            headers.add(LUtils.getDateTime(tableSurveyData.getPreQuestionsStartDateTime()));
            headers.add(LUtils.getDateTime(tableSurveyData.getVideoOrCounsellingStartDateTime()));
            headers.add(LUtils.getDateTime(tableSurveyData.getPostQuestionsStartDateTime()));

            if (0 != tableSurveyData.getResultStartDateTime()) {
                headers.add(LUtils.getDateTime(tableSurveyData.getResultStartDateTime()));
            }

            if (0 != tableSurveyData.getResultEndDateTime()) {
                headers.add(LUtils.getDateTime(tableSurveyData.getResultEndDateTime()));
            }

            try {
                JSONArray jsonArray = new JSONArray(tableSurveyData.getSociodemographicData());

                for (String socioQuestion : socioQuestions) {
                    String answer = "";

                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String     question   = jsonObject.getString("question");

                        if (socioQuestion.equals(question)) {
                            answer = jsonObject.getString("answer");
                            break;
                        }
                    }

                    headers.add(answer);
                }

            } catch (JSONException e) {
                LLog.printStackTrace(e);
            }

            // Write PrePostQuestion
            try {
                JSONArray jsonPreQuestions  = new JSONArray(tableSurveyData.getPreQuestions());
                JSONArray jsonPostQuestions = new JSONArray(tableSurveyData.getPostQuestions());

                int correctPreTestAnswers  = 0;
                int correctPostTestAnswers = 0;

                // Correct answers
                for (int i = 0; i < prePostQuestions.size(); i++) {
                    String preQuestion = prePostQuestions.get(i);

                    for (int j = 0; j < jsonPreQuestions.length(); j++) {
                        JSONObject jsonObject = jsonPreQuestions.getJSONObject(j);
                        String     question   = jsonObject.getString("question");

                        if (preQuestion.equals(question)) {
                            headers.add(prePostQuestionAnswers.get(i));
                            break;
                        }
                    }
                }

                // Pre-awareness answers
                for (int i = 0; i < prePostQuestions.size(); i++) {
                    String preQuestion = prePostQuestions.get(i);

                    for (int j = 0; j < jsonPreQuestions.length(); j++) {
                        JSONObject jsonObject = jsonPreQuestions.getJSONObject(j);
                        String     question   = jsonObject.getString("question");

                        if (preQuestion.equals(question)) {
                            headers.add(jsonObject.getString("answer"));
                            break;
                        }
                    }
                }

                // Post-awareness answers
                for (int i = 0; i < prePostQuestions.size(); i++) {
                    String preQuestion = prePostQuestions.get(i);

                    for (int j = 0; j < jsonPreQuestions.length(); j++) {
                        JSONObject jsonObject = jsonPreQuestions.getJSONObject(j);
                        String     question   = jsonObject.getString("question");

                        if (preQuestion.equals(question)) {
                            headers.add(jsonPostQuestions.getJSONObject(j).getString("answer"));

                            if (prePostQuestionAnswers.get(i).equals(jsonObject.getString("answer"))) {
                                ++correctPreTestAnswers;
                            }
                            if (prePostQuestionAnswers.get(i).equals(jsonPostQuestions.getJSONObject(j).getString("answer"))) {
                                ++correctPostTestAnswers;
                            }

                            break;
                        }
                    }
                }

                headers.add("" + correctPreTestAnswers);
                headers.add("" + correctPostTestAnswers);

            } catch (JSONException e) {
                LLog.printStackTrace(e);
            }

            if (tableSurveyData.getGame1StartDateTime() > 0) {
                headers.add(LUtils.getDateTime(tableSurveyData.getGame1StartDateTime()));
                headers.add(LUtils.getDateTime(tableSurveyData.getGame1EndDateTime()));
                headers.add(LUtils.getDateTime(tableSurveyData.getGame2StartDateTime()));
                headers.add(LUtils.getDateTime(tableSurveyData.getGame2EndDateTime()));
                headers.add(LUtils.getDateTime(tableSurveyData.getGame3StartDateTime()));
                headers.add(LUtils.getDateTime(tableSurveyData.getGame3EndDateTime()));
                headers.add(LUtils.getDateTime(tableSurveyData.getGame4StartDateTime()));
                headers.add(LUtils.getDateTime(tableSurveyData.getGame4EndDateTime()));
            } else {
                headers.add("");
                headers.add("");
                headers.add("");
                headers.add("");
                headers.add("");
                headers.add("");
                headers.add("");
                headers.add("");
            }

            int headersRequired = 3;
            try {
                ItemUserSelectionGame item = gson.fromJson(tableSurveyData.getUserSelectionGame(), ItemUserSelectionGame.class);

                headers.add(item.getGame3()[0]);
                --headersRequired;
                headers.add(item.getGame3()[1]);
                --headersRequired;
                headers.add(item.getGame3()[2]);
                --headersRequired;

            } catch (Exception e) {
            }

            while (headersRequired > 0) {
                headers.add("");
                --headersRequired;
            }

            headers.add(tableSurveyData.getGpsLocation());

            String[] data = new String[headers.size()];
            headers.toArray(data);
            writer.writeNext(data);
        }
    }

    private static String getFileName() {
        Calendar calendar = Calendar.getInstance();

        String fileName = "";

        if (10 > calendar.get(Calendar.DATE)) {
            fileName += "0" + calendar.get(Calendar.DATE);
        } else {
            fileName += calendar.get(Calendar.DATE);
        }

        fileName += "_";

        if (9 > calendar.get(Calendar.MONTH)) {
            fileName += "0" + (calendar.get(Calendar.MONTH) + 1);
        } else {
            fileName += (calendar.get(Calendar.MONTH) + 1);
        }

        fileName += "_" + calendar.get(Calendar.YEAR) + "_";

        if (10 > calendar.get(Calendar.HOUR_OF_DAY)) {
            fileName += "0" + calendar.get(Calendar.HOUR_OF_DAY);
        } else {
            fileName += calendar.get(Calendar.HOUR_OF_DAY);
        }

        fileName += "-";

        if (10 > calendar.get(Calendar.MINUTE)) {
            fileName += "0" + calendar.get(Calendar.MINUTE);
        } else {
            fileName += calendar.get(Calendar.MINUTE);
        }

        return fileName;
    }

    public static String getDateTime(long dateTime) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm", Locale.ENGLISH);
        return formatter.format(new Date(dateTime));
    }

    public static String getImagePath(String imageName) {
        if (imageName.endsWith(".gif")) {
            return Environment.getExternalStorageDirectory() + GIF_PATH + imageName;
        } else {
            return Environment.getExternalStorageDirectory() + ICONS_PATH + imageName;
        }
    }

    public static int getCorrectAnswerIndex(ItemQuestion itemQuestion) {
        for (int i = 0; i < itemQuestion.getAnswers().length; i++) {
            if (itemQuestion.getAnswers()[i].getAnswer().equals(itemQuestion.getCorrectAnswer())) {
                return i;
            }
        }
        return 0;
    }
}