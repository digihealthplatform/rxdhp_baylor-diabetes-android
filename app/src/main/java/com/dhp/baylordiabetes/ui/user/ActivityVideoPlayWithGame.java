package com.dhp.baylordiabetes.ui.user;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.widget.MediaController;
import android.widget.VideoView;

import com.dhp.baylordiabetes.R;
import com.dhp.baylordiabetes.data.asset.AssetReader;
import com.dhp.baylordiabetes.data.item.VideoFile;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.dhp.baylordiabetes.util.LConstants.VIDEO_PATH;


public class ActivityVideoPlayWithGame extends AppCompatActivity
        implements MediaPlayer.OnCompletionListener {

    @BindView(R.id.video_view)
    VideoView videoView;

    private int currentVideo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_play);
        ButterKnife.bind(this);

        currentVideo = getIntent().getIntExtra("current_video", 0);

        VideoFile[] videoFiles = AssetReader.getVideoFiles(AssetReader.getJsonData());

        playVideo(videoFiles[currentVideo].getFileName());
    }

    @Override
    public void onBackPressed() {
    }

    void playVideo(String videoUrl) {
        videoView.setVideoPath(Environment.getExternalStorageDirectory().getAbsolutePath() + VIDEO_PATH + videoUrl);
        videoView.setMediaController(new MediaController(this));
        videoView.setOnCompletionListener(this);
        videoView.requestFocus();

        videoView.start();
    }

    @Override
    public void onCompletion(MediaPlayer mediaPlayer) {
        ++currentVideo;

        Intent intent = new Intent(this, ActivityGameStart.class);
        intent.putExtra("current_video", currentVideo);
        startActivity(intent);

        finish();
    }
}