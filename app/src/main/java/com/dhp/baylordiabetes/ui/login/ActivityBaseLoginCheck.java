package com.dhp.baylordiabetes.ui.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.dhp.baylordiabetes.data.DataManager;
import com.dhp.baylordiabetes.data.asset.AssetReader;
import com.dhp.baylordiabetes.data.table.TableUser;
import com.dhp.baylordiabetes.ui.ActivityBase;
import com.dhp.baylordiabetes.ui.admin.ActivityBaseAdminHome;
import com.dhp.baylordiabetes.ui.user.ActivityLanguageSelection;

import javax.inject.Inject;

import static com.dhp.baylordiabetes.data.database.SharedPrefsHelper.ADMIN_LOGGED_IN;
import static com.dhp.baylordiabetes.data.database.SharedPrefsHelper.KEY_LOGGED_IN;
import static com.dhp.baylordiabetes.data.database.SharedPrefsHelper.KEY_REMEMBER_LOG_IN;
import static com.dhp.baylordiabetes.data.database.SharedPrefsHelper.NOT_LOGGED_IN;
import static com.dhp.baylordiabetes.data.database.SharedPrefsHelper.USER_LOGGED_IN;


public class ActivityBaseLoginCheck extends ActivityBase {

    @Inject
    DataManager dataManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivityComponent(this).inject(this);

        // Create test trainer on first open
        if (!dataManager.getBooleanPref("test_trainer_created", false)) {
            TableUser tableUser = new TableUser();
            tableUser.setFullName("Trainer");
            tableUser.setUsername("t1");
            tableUser.setPassword("t1");
            tableUser.setTrainer(true);
            tableUser.save();
            dataManager.setPref("test_trainer_created", true);
        }

        if (dataManager.isCopyAssetsNeeded()) {
            startActivity(new Intent(this, ActivityBaseCopyAssets.class));

        } else {

            boolean initSuccess = AssetReader.init();
            if (!initSuccess) {
                finish();
                return;
            }

            decideLogin();
        }

        finish();
    }

    private void decideLogin() {

        Intent intent = new Intent(this, ActivityBaseLogin.class);

        int loginState = dataManager.getIntPref(KEY_LOGGED_IN, NOT_LOGGED_IN);

        switch (loginState) {
            case NOT_LOGGED_IN: {
                intent = new Intent(this, ActivityBaseLogin.class);
            }
            break;

            case USER_LOGGED_IN: {
                if (dataManager.getBooleanPref(KEY_REMEMBER_LOG_IN, false)) {
                    intent = new Intent(this, ActivityLanguageSelection.class);
                }
            }
            break;

            case ADMIN_LOGGED_IN: {
                if (dataManager.getBooleanPref(KEY_REMEMBER_LOG_IN, false)) {
                    intent = new Intent(this, ActivityBaseAdminHome.class);
                }
            }
            break;
        }

        startActivity(intent);
    }
}