package com.dhp.baylordiabetes.ui.user;

import android.content.Intent;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.ImageView;
import android.widget.VideoView;

import com.bumptech.glide.Glide;
import com.dhp.baylordiabetes.R;
import com.dhp.baylordiabetes.data.asset.AssetReader;
import com.dhp.baylordiabetes.ui.ActivityBase;
import com.dhp.baylordiabetes.util.LToast;

import java.io.File;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.dhp.baylordiabetes.data.asset.AssetReader.KEY_CORRECT_ANSWER_GIF;
import static com.dhp.baylordiabetes.util.LConstants.AUDIO_PATH;
import static com.dhp.baylordiabetes.util.LConstants.VIDEO_PATH;
import static com.dhp.baylordiabetes.util.LUtils.getImagePath;


public class ActivityGame4 extends ActivityBase {

    @BindView(R.id.video_view)
    VideoView videoView;

    @BindView(R.id.iv_1)
    ImageView iv1;

    @BindView(R.id.iv_2)
    ImageView iv2;

    @BindView(R.id.iv_3)
    ImageView iv3;

    @BindView(R.id.iv_4)
    ImageView iv4;

    private ArrayList<String> gifOptions;

    private boolean[] clicked = new boolean[4];

    private boolean isClicked;

    private Handler  handler  = new Handler();
    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            if (!isClicked) {
                LToast.warning(AssetReader.getAppText("select_an_option"));
                handler.postDelayed(this, AssetReader.getInt("select_option_duration") * 1000);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_4);
        ButterKnife.bind(this);
        initToolbar(AssetReader.getGameTitle(4));

        AssetReader.game4StartDateTime = System.currentTimeMillis();

        initViews();

        gifOptions = AssetReader.getGifOptions(4);

        Glide.with(this)
                .load(getImagePath(gifOptions.get(0)))
                .asGif()
                .into(iv1);

        Glide.with(this)
                .load(getImagePath(gifOptions.get(1)))
                .asGif()
                .into(iv2);

        Glide.with(this)
                .load(getImagePath(gifOptions.get(2)))
                .asGif()
                .into(iv3);

        Glide.with(this)
                .load(getImagePath(gifOptions.get(3)))
                .asGif()
                .into(iv4);

        playVideo(AssetReader.getStringValueGame(4, "video"));
        playAudioFile(AssetReader.getStringValueGame(4, "audio"));
    }

    void playVideo(String videoUrl) {
        videoView.setVideoPath(Environment.getExternalStorageDirectory().getAbsolutePath() + VIDEO_PATH + videoUrl);
        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.setVolume(0f, 0f);
                mp.setLooping(true);
            }
        });
        videoView.requestFocus();
        videoView.start();
    }

    @OnClick({R.id.iv_1, R.id.iv_2, R.id.iv_3, R.id.iv_4})
    void onImageClicked(View view) {
        isClicked = true;

        switch (view.getId()) {

            case R.id.iv_1: {
                if (!clicked[0]) {
                    Drawable drawable = iv1.getDrawable();
                    if (drawable instanceof Animatable) {
                        ((Animatable) drawable).stop();
                    }
                    clicked[0] = true;
                    iv1.setAlpha(0.7f);
                    iv1.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_rectangle));
                    onAnswerClicked();
                    onCorrectAnswer();
                }
                break;
            }

            case R.id.iv_2: {
                if (!clicked[1]) {
                    Drawable drawable = iv2.getDrawable();
                    if (drawable instanceof Animatable) {
                        ((Animatable) drawable).stop();
                    }
                    clicked[1] = true;
                    iv2.setAlpha(0.7f);
                    iv2.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_rectangle));
                    onAnswerClicked();
                    onCorrectAnswer();
                }
                break;
            }

            case R.id.iv_3: {
                if (!clicked[2]) {
                    Drawable drawable = iv3.getDrawable();
                    if (drawable instanceof Animatable) {
                        ((Animatable) drawable).stop();
                    }
                    clicked[2] = true;
                    iv3.setAlpha(0.7f);
                    iv3.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_rectangle));
                    onAnswerClicked();
                    onCorrectAnswer();
                }
                break;
            }

            case R.id.iv_4: {
                if (!clicked[3]) {
                    Drawable drawable = iv4.getDrawable();
                    if (drawable instanceof Animatable) {
                        ((Animatable) drawable).stop();
                    }
                    clicked[3] = true;
                    iv4.setAlpha(0.7f);
                    iv4.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_rectangle));
                    onAnswerClicked();
                    onCorrectAnswer();
                }
                break;
            }
        }
    }

    private void onCorrectAnswer() {
        showFullScreenGif(AssetReader.getStringValueGame(2, KEY_CORRECT_ANSWER_GIF));
        playAudio(AssetReader.getStringValueGame(2, "correct_answer_sound"), true);
    }

    private void onAnswerClicked() {
        boolean allClicked = true;

        for (int i = 0; i < clicked.length; i++) {
            if (!clicked[i]) {
                allClicked = false;
                break;
            }
        }

        if (allClicked) {
            Intent intent;

            if (AssetReader.toShowStartPrePostQuestions()) {
                intent = new Intent(this, ActivityStartPostQuestions.class);
            } else {
                intent = (new Intent(this, ActivityPostQuestions.class));
            }
            startActivityDelayed(intent);
        }
    }

    private void startActivityDelayed(final Intent intent) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                AssetReader.game4EndDateTime = System.currentTimeMillis();
                startActivity(intent);
                finish();
            }
        }, 2500);
    }

    private void playAudioFile(final String fileName) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                final File audioFile = new File(Environment.getExternalStorageDirectory(), AUDIO_PATH + fileName);

                if (!audioFile.exists()) {
                    return;
                }

                audioPlayerGame = MediaPlayer.create(ActivityGame4.this, Uri.fromFile(audioFile));
                audioPlayerGame.start();
                audioPlayerGame.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mediaPlayer) {
                        videoView.setVisibility(View.GONE);

                        handler.postDelayed(runnable, AssetReader.getInt("select_option_duration") * 1000);
                    }
                });
            }
        }).start();
    }
}