package com.dhp.baylordiabetes.ui.user;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.dhp.baylordiabetes.R;
import com.dhp.baylordiabetes.data.asset.AssetReader;
import com.dhp.baylordiabetes.ui.ActivityBase;

import static com.dhp.baylordiabetes.data.asset.AssetReader.KEY_START_PRE_QUESTIONS;
import static com.dhp.baylordiabetes.data.asset.AssetReader.KEY_START_PRE_QUESTIONS_AUDIO_FILE;
import static com.dhp.baylordiabetes.data.asset.AssetReader.KEY_TITLE_PRE_QUESTIONS;


public class ActivityStartPreQuestions extends ActivityBase {

    @BindView(R.id.tv_message)
    TextView tvMessage;

    @BindView(R.id.iv_next)
    ImageView ivNext;

    @BindView(R.id.iv_play_audio)
    ImageView ivPlayAudio;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_pre_questions);
        ButterKnife.bind(this);

        initToolbar(AssetReader.getAppText(KEY_TITLE_PRE_QUESTIONS));

        tvMessage.setText(AssetReader.getAppText(KEY_START_PRE_QUESTIONS));
        ivNext.setVisibility(View.VISIBLE);

        initAudio(ivPlayAudio, AssetReader.getAppText(KEY_START_PRE_QUESTIONS_AUDIO_FILE));
    }

    @Override
    protected void onStop() {
        stopAudio();
        super.onStop();
    }

    @OnClick(R.id.iv_next)
    void onNext() {
        startActivity(new Intent(this, ActivityPreQuestions.class));
    }
}