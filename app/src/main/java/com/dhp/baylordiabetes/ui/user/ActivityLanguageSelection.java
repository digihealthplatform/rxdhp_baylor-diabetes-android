package com.dhp.baylordiabetes.ui.user;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.PopupMenu;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import com.dhp.baylordiabetes.R;
import com.dhp.baylordiabetes.data.DataManager;
import com.dhp.baylordiabetes.data.asset.AssetReader;
import com.dhp.baylordiabetes.data.item.VideoFile;
import com.dhp.baylordiabetes.ui.ActivityBase;
import com.dhp.baylordiabetes.ui.login.ActivityBaseLogin;
import com.dhp.baylordiabetes.util.LUtils;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.dhp.baylordiabetes.data.asset.AssetReader.KEY_EXPORT;
import static com.dhp.baylordiabetes.util.LView.setSpinnerAdapter;


public class ActivityLanguageSelection extends ActivityBase {

    @BindView(R.id.rg_language)
    RadioGroup rgLanguage;

    @BindView(R.id.ll_video)
    LinearLayout llVideo;

    @BindView(R.id.btn_start)
    Button btnStart;

    @BindView(R.id.btn_export)
    Button btnExport;

    @BindView(R.id.iv_menu)
    ImageView ivMenu;

    @BindView(R.id.rg_video)
    RadioGroup rgVideo;

    @BindView(R.id.rb_view_full)
    RadioButton rbFullVideo;

    @BindView(R.id.rb_view_module)
    RadioButton rbModuleVideo;

    @BindView(R.id.sp_select_section)
    Spinner spSelectVideo;

    @BindView(R.id.iv_play)
    ImageView ivPlay;

    @BindView(R.id.tv_is_trainer)
    TextView tvIsTrainer;

    String videoUrl   = "";
    String videoTitle = "";

    @Inject
    DataManager dataManager;

    private boolean started;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_language_selection);
        ButterKnife.bind(this);
        getActivityComponent(this).inject(this);

        boolean initSuccess = AssetReader.init();
        if (!initSuccess) {
            logout();
            return;
        }

        initToolbar(getString(R.string.title_language_selection));

        if (AssetReader.isToRandomise()) {
            llVideo.setVisibility(View.GONE);
        } else {
            llVideo.setVisibility(View.VISIBLE);
        }

        rgLanguage.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int index) {

                AssetReader.setSelectedLanguage(index);

                btnExport.setText(AssetReader.getAppText(KEY_EXPORT));

                final VideoFile[] videoFiles = AssetReader.getVideoFiles(AssetReader.getJsonData());
                final String[]    videoList  = new String[videoFiles.length + 1];
                videoList[0] = AssetReader.getAppText(AssetReader.KEY_SELECT);

                for (int i = 0; i < videoFiles.length; i++) {
                    videoList[i + 1] = " " + videoFiles[i].getTitle() + " ";
                }

                rbFullVideo.setText(AssetReader.getAppText(AssetReader.KEY_VIEW_FULL_VIDEO));
                rbModuleVideo.setText(AssetReader.getAppText(AssetReader.KEY_VIEW_MODULE_VIDEO));
                btnStart.setText(AssetReader.getAppText(AssetReader.KEY_START_SURVEY));

                setSpinnerAdapter(ActivityLanguageSelection.this, spSelectVideo, videoList);
            }
        });

        String[] languages = AssetReader.getLanguages();

        if (null == languages) {
            return;
        }

        for (int i = 0; i < languages.length; i++) {
            RadioButton radioButton = new RadioButton(this);
            radioButton.setText(languages[i]);
            radioButton.setId(i);
            radioButton.setPadding(0, 20, 60, 20);
            radioButton.setTextSize(TypedValue.COMPLEX_UNIT_SP, 25);
            rgLanguage.addView(radioButton);
        }

        ((RadioButton) rgLanguage.getChildAt(0)).setChecked(true);

        rgVideo.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int indexId) {
                if (-1 == indexId) {
                    return;
                }

                if (R.id.rb_view_full == indexId) {
                    setSpinnerAdapter(ActivityLanguageSelection.this, spSelectVideo, getVideoList());
                    spSelectVideo.setVisibility(View.INVISIBLE);
                    videoUrl = "";
                    videoTitle = "All";
                    ivPlay.setVisibility(View.VISIBLE);

                } else {
                    spSelectVideo.setVisibility(View.VISIBLE);
                    ivPlay.setVisibility(View.INVISIBLE);
                }
            }
        });

        spSelectVideo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (position < 1) {
                    return;
                }

                VideoFile[] videoFiles = AssetReader.getVideoFiles(AssetReader.getJsonData());

                videoUrl = videoFiles[position - 1].getFileName();
                videoTitle = videoFiles[position - 1].getTitle();
                ivPlay.setVisibility(View.VISIBLE);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        if (dataManager.isTrainer()) {
            tvIsTrainer.setVisibility(View.VISIBLE);
        } else {
            tvIsTrainer.setVisibility(View.GONE);
        }
    }

    private String[] getVideoList() {
        VideoFile[] videoFiles = AssetReader.getVideoFiles(AssetReader.getJsonData());
        String[]    videoList  = new String[videoFiles.length + 1];

        videoList[0] = AssetReader.getAppText(AssetReader.KEY_SELECT);

        for (int i = 0; i < videoFiles.length; i++) {
            videoList[i + 1] = " " + videoFiles[i].getTitle() + " ";
        }

        return videoList;
    }

    @OnClick(R.id.iv_play)
    void onPlay() {
        Intent intent = new Intent(this, ActivityVideoPlay.class);
        intent.putExtra("video_file", videoUrl);
        intent.putExtra("first_page_play", true);
        startActivity(intent);
    }

    @OnClick(R.id.btn_start)
    void onNext() {
        startActivity(new Intent(this, ActivityConsent.class));
        started = true;
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (dataManager.isSurveyDataPresent()) {
            btnExport.setVisibility(View.VISIBLE);
        } else {
            btnExport.setVisibility(View.GONE);
        }

        if (started) {
            ((RadioButton) rgLanguage.getChildAt(0)).setChecked(true);
            ((RadioButton) rgVideo.getChildAt(0)).setChecked(true);
            started = false;
        }
    }

    @OnClick(R.id.iv_menu)
    void onMenuClick() {
        PopupMenu popup = new PopupMenu(this, ivMenu);

        popup.getMenuInflater()
                .inflate(R.menu.menu_default, popup.getMenu());

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {

                AlertDialog.Builder builder;

                builder = new AlertDialog.Builder(ActivityLanguageSelection.this, R.style.AlertDialogLight);

                builder.setMessage(getString(R.string.confirm_logout));
                builder.setPositiveButton(getString(R.string.btn_logout), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        logout();
                    }
                });
                builder.setNegativeButton(getString(R.string.btn_cancel), null);
                builder.show();

                return true;
            }
        });

        popup.show();
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(getString(R.string.message_confirm_exit));
        builder.setPositiveButton(getString(R.string.btn_exit), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        builder.setNegativeButton(getString(R.string.btn_cancel), null);
        builder.show();
    }

    @OnClick(R.id.btn_export)
    void exportData() {
        LUtils.saveFile(this, dataManager.getSurveyData());
    }

    private void logout() {
        dataManager.logout();
        startActivity(new Intent(ActivityLanguageSelection.this, ActivityBaseLogin.class));
        finish();
    }
}