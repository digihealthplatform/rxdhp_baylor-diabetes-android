package com.dhp.baylordiabetes.ui.user;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;

import com.dhp.baylordiabetes.R;
import com.dhp.baylordiabetes.data.adapter.AdapterSpinner;
import com.dhp.baylordiabetes.data.asset.AssetReader;
import com.dhp.baylordiabetes.data.item.VideoFile;
import com.dhp.baylordiabetes.ui.ActivityBase;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.dhp.baylordiabetes.data.asset.AssetReader.KEY_TITLE_WATCH_VIDEO;


public class ActivityVideo extends ActivityBase {

    @BindView(R.id.rg_video)
    RadioGroup rgVideo;

    @BindView(R.id.sp_select_section)
    Spinner spSelectVideo;

    @BindView(R.id.iv_play)
    ImageView ivPlay;

    @BindView(R.id.rb_view_full)
    RadioButton rbFullVideo;

    @BindView(R.id.rb_view_module)
    RadioButton rbModuleVideo;

    String videoUrl          = "";
    String videoTitleEnglish = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video);

        ButterKnife.bind(this);

        initToolbar(AssetReader.getAppText(KEY_TITLE_WATCH_VIDEO));

        final VideoFile[] videoFiles = AssetReader.getVideoFiles(AssetReader.getJsonData());

        videoTitleEnglish = "" + (videoFiles.length + 1);

        String[] videoList = new String[videoFiles.length + 1];
        videoList[0] = AssetReader.getAppText(AssetReader.KEY_SELECT);

        for (int i = 0; i < videoFiles.length; i++) {
            videoList[i + 1] = " " + videoFiles[i].getTitle() + " ";
        }

        rbFullVideo.setText(AssetReader.getAppText(AssetReader.KEY_VIEW_FULL_VIDEO));
        rbModuleVideo.setText(AssetReader.getAppText(AssetReader.KEY_VIEW_MODULE_VIDEO));

        rgVideo.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int indexId) {
                if (-1 == indexId) {
                    return;
                }

                if (R.id.rb_view_full == indexId) {
                    spSelectVideo.setVisibility(View.INVISIBLE);
                    videoUrl = "";
                    videoTitleEnglish = "" + (videoFiles.length + 1);
                    ivPlay.setVisibility(View.VISIBLE);
                } else {
                    spSelectVideo.setVisibility(View.VISIBLE);
                    ivPlay.setVisibility(View.INVISIBLE);
                }
            }
        });

        spSelectVideo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (position < 1) {
                    return;
                }

                videoUrl = videoFiles[position - 1].getFileName();
                videoTitleEnglish = "" + position;
                ivPlay.setVisibility(View.VISIBLE);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        setSpinnerAdapter(spSelectVideo, videoList);
    }

    @OnClick(R.id.iv_play)
    void onPlay() {
        Intent intent;

        if (AssetReader.isGamesEnabled()) {
            intent = new Intent(this, ActivityVideoPlayWithGame.class);
        } else {
            intent = new Intent(this, ActivityVideoPlay.class);
        }

        intent.putExtra("video_file", videoUrl);

        AssetReader.setVideoWatched(videoTitleEnglish);

        AssetReader.videoOrCounsellingStartDateTime = Calendar.getInstance().getTimeInMillis();

        startActivity(intent);
    }

    private void setSpinnerAdapter(Spinner spinner, String[] list) {
        final AdapterSpinner<String> adapterSpinner = new AdapterSpinner<>(
                this,
                R.layout.spinner_layout,
                new ArrayList<>(Arrays.asList(list))
        );
        adapterSpinner.setDropDownViewResource(R.layout.spinner_layout);
        spinner.setAdapter(adapterSpinner);
    }
}