package com.dhp.baylordiabetes.ui.user;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.dhp.baylordiabetes.R;
import com.dhp.baylordiabetes.data.adapter.AdapterPagination;
import com.dhp.baylordiabetes.data.asset.AssetReader;
import com.dhp.baylordiabetes.ui.ActivityBase;
import com.dhp.baylordiabetes.util.LUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class ActivityCounselling extends ActivityBase {

    @BindView(R.id.iv_pic)
    ImageView ivPic;

    @BindView(R.id.iv_prev)
    ImageView ivPrev;

    @BindView(R.id.iv_next)
    ImageView ivNext;

    @BindView(R.id.rv_pagination)
    RecyclerView rvPagination;

    @BindView(R.id.btn_submit)
    Button btnSubmit;

    private int currentIndex;

    private AdapterPagination adapterPagination;

    private String[] counsellingImages;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_counselling);

        ButterKnife.bind(this);

        btnSubmit.setText("Done");

        counsellingImages = AssetReader.getCounsellingImages();

        adapterPagination = new AdapterPagination(this, counsellingImages.length);
        rvPagination.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        rvPagination.setAdapter(adapterPagination);

        showImage();
    }

    private void showImage() {
        String imgPath = LUtils.getImagePath(counsellingImages[currentIndex]);

        Glide.with(this)
                .load(imgPath)
//                .placeholder(R.drawable.loader)
                .fitCenter()
                .into(ivPic);

        if ((counsellingImages.length - 1) == currentIndex) {
            btnSubmit.setVisibility(View.VISIBLE);
            ivNext.setVisibility(View.GONE);

        } else if (currentIndex < (counsellingImages.length - 1)) {
            ivNext.setVisibility(View.VISIBLE);
            btnSubmit.setVisibility(View.GONE);
        } else {

            ivNext.setVisibility(View.GONE);
            btnSubmit.setVisibility(View.GONE);
        }

        if (0 == currentIndex) {
            ivPrev.setVisibility(View.GONE);
        } else {
            ivPrev.setVisibility(View.VISIBLE);
        }

        adapterPagination.setSelection(currentIndex);
    }

    @OnClick(R.id.iv_prev)
    void onPrevClicked() {
        if (0 == currentIndex) {
            return;
        }

        --currentIndex;
        showImage();
    }

    @OnClick(R.id.iv_next)
    void onNexClicked() {
        if ((counsellingImages.length - 1) == currentIndex) {
            return;
        }

        ++currentIndex;
        showImage();
    }

    @OnClick(R.id.btn_submit)
    void onDoneClicked() {
        if (AssetReader.toShowStartPrePostQuestions()) {
            startActivity(new Intent(this, ActivityStartPostQuestions.class));
        } else {
            startActivity(new Intent(this, ActivityPostQuestions.class));
        }
        finish();
    }
}