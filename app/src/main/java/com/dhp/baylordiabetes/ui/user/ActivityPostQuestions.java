package com.dhp.baylordiabetes.ui.user;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Point;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dhp.baylordiabetes.R;
import com.dhp.baylordiabetes.data.DataManager;
import com.dhp.baylordiabetes.data.adapter.AdapterAnswer;
import com.dhp.baylordiabetes.data.adapter.AdapterPagination;
import com.dhp.baylordiabetes.data.asset.AssetReader;
import com.dhp.baylordiabetes.data.item.ColumnQuestionAnswer;
import com.dhp.baylordiabetes.data.item.ItemAnswer;
import com.dhp.baylordiabetes.data.item.ItemQuestion;
import com.dhp.baylordiabetes.ui.AnswerCallback;
import com.dhp.baylordiabetes.ui.ActivityBase;
import com.dhp.baylordiabetes.util.LView;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.dhp.baylordiabetes.data.asset.AssetReader.KEY_ANSWER;
import static com.dhp.baylordiabetes.data.asset.AssetReader.KEY_CANCEL;
import static com.dhp.baylordiabetes.data.asset.AssetReader.KEY_CONFIRM_PROCEED;
import static com.dhp.baylordiabetes.data.asset.AssetReader.KEY_SELECT_ANSWER;
import static com.dhp.baylordiabetes.data.asset.AssetReader.KEY_TITLE_POST_QUESTIONS;
import static com.dhp.baylordiabetes.data.asset.AssetReader.KEY_UPDATE_ANSWER;
import static com.dhp.baylordiabetes.data.asset.AssetReader.KEY_YES;
import static com.dhp.baylordiabetes.util.LUtils.getImagePath;


public class ActivityPostQuestions extends ActivityBase
        implements AnswerCallback,
        View.OnClickListener {

    private int currentAnswer = -1;
    private int currentQuestion;

    private int[] selectedAnswers;

    private ItemQuestion[] postQuestions;
    private ItemQuestion[] postQuestionsEnglish;

    @BindView(R.id.tv_question_title)
    TextView tvQuestionTitle;

    @BindView(R.id.tv_question)
    TextView tvQuestion;

    @BindView(R.id.btn_answer)
    Button btnAnswer;

    @BindView(R.id.ll_rec_answers)
    LinearLayout llAnswersRec;

    @BindView(R.id.rv_answers)
    RecyclerView rvAnswers;

    @BindView(R.id.ll_answers)
    LinearLayout llAnswers;

    @BindView(R.id.iv_answer)
    ImageView ivAnswer;

    @BindView(R.id.iv_prev)
    ImageView ivPrev;

    @BindView(R.id.iv_next)
    ImageView ivNext;

    @BindView(R.id.iv_play_audio)
    ImageView ivPlayAudio;

    @BindView(R.id.rv_pagination)
    RecyclerView rvPagination;

    @BindView(R.id.btn_submit)
    Button btnSubmit;

    private AdapterPagination adapterPagination;

    @Inject
    DataManager dataManager;

    private AlertDialog answersDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question);
        ButterKnife.bind(this);
        getActivityComponent(this).inject(this);

        initToolbar(AssetReader.getAppText(KEY_TITLE_POST_QUESTIONS));

        postQuestions = AssetReader.getPostQuestions(AssetReader.getJsonData());
        postQuestionsEnglish = AssetReader.getPostQuestions(AssetReader.getJsonDataEnglish());

        adapterPagination = new AdapterPagination(this, postQuestions.length);
        rvPagination.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        rvPagination.setAdapter(adapterPagination);

        selectedAnswers = new int[postQuestions.length];
        Arrays.fill(selectedAnswers, -1);

        btnSubmit.setText(AssetReader.getAppText(AssetReader.KEY_SUBMIT));

        showQuestion();

        AssetReader.postQuestionsStartDateTime = Calendar.getInstance().getTimeInMillis();
    }

    @Override
    protected void onStop() {
        stopAudio();
        super.onStop();
    }

    private void showQuestion() {
        initAudio(ivPlayAudio, postQuestions[currentQuestion].getAudioFile());

        if (currentQuestion > 0) {
            ivPrev.setVisibility(View.VISIBLE);
        } else {
            ivPrev.setVisibility(View.GONE);
        }

        tvQuestionTitle.setText(postQuestions[currentQuestion].getTitle());
        tvQuestion.setText(postQuestions[currentQuestion].getQuestion());

        if (null != postQuestions[currentQuestion].getAnswersInPopup()
                && postQuestions[currentQuestion].getAnswersInPopup().toLowerCase().equals("yes")) {
            llAnswers.setVisibility(View.VISIBLE);
            llAnswersRec.setVisibility(View.GONE);

            btnAnswer.setText(AssetReader.getAppText(KEY_ANSWER));

            if (-1 != currentAnswer) {
                onAnswerSelected(currentAnswer);
                showGifAnswer();

            } else {
                ivAnswer.setVisibility(View.GONE);
            }

        } else {
            AdapterAnswer adapterAnswer = new AdapterAnswer(this, postQuestions[currentQuestion].getAnswers());
            rvAnswers.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
            rvAnswers.setAdapter(adapterAnswer);
            llAnswersRec.setVisibility(View.VISIBLE);
            llAnswers.setVisibility(View.GONE);

            if (-1 != currentAnswer) {
                adapterAnswer.setSelection(currentAnswer);
            }
        }

        adapterPagination.setSelection(currentQuestion);

        btnSubmit.setVisibility(View.GONE);
    }

    @Override
    public void onAnswerSelected(int position) {
        currentAnswer = position;

        if (currentQuestion < postQuestions.length - 1) {
            ivNext.setVisibility(View.VISIBLE);
        }

        if (currentQuestion == postQuestions.length - 1) { // Last question
            ivNext.setVisibility(View.GONE);
            btnSubmit.setVisibility(View.VISIBLE);
        }
    }

    private void showGifAnswer() {
        ivAnswer.setVisibility(View.VISIBLE);

        File imageFile = new File(getImagePath(postQuestions[currentQuestion].getAnswers()[currentAnswer].getIcon()));

        if (imageFile.exists()) {
            Uri imageUri = Uri.fromFile(imageFile);
            LView.loadImage(this, imageUri, ivAnswer);
        }

        btnAnswer.setText(AssetReader.getAppText(KEY_UPDATE_ANSWER));
    }

    @OnClick(R.id.btn_answer)
    void showAnswersDialog() {
        ItemAnswer[] answers = postQuestions[currentQuestion].getAnswers();

        View dialogView;

        if (2 == answers.length) {
            dialogView = LayoutInflater.from(this).inflate(R.layout.dialog_answers_2, null);

        } else if (3 == answers.length) {
            dialogView = LayoutInflater.from(this).inflate(R.layout.dialog_answers_3, null);

        } else {
            dialogView = LayoutInflater.from(this).inflate(R.layout.dialog_answers_4, null);
        }

        if (answers.length > 3) {
            ImageView imageView4 = (ImageView) dialogView.findViewById(R.id.iv_answer_4);
            imageView4.setOnClickListener(this);

            File imageFile = new File(getImagePath(answers[3].getIcon()));

            if (imageFile.exists()) {
                Uri imageUri = Uri.fromFile(imageFile);
                LView.loadImage(this, imageUri, imageView4);
            }
        }

        if (answers.length > 2) {
            ImageView imageView3 = (ImageView) dialogView.findViewById(R.id.iv_answer_3);
            imageView3.setOnClickListener(this);

            File imageFile = new File(getImagePath(answers[2].getIcon()));

            if (imageFile.exists()) {
                Uri imageUri = Uri.fromFile(imageFile);
                LView.loadImage(this, imageUri, imageView3);
            }
        }

        if (answers.length > 1) {
            ImageView imageView2 = (ImageView) dialogView.findViewById(R.id.iv_answer_2);
            imageView2.setOnClickListener(this);

            File imageFile = new File(getImagePath(answers[1].getIcon()));

            if (imageFile.exists()) {
                Uri imageUri = Uri.fromFile(imageFile);
                LView.loadImage(this, imageUri, imageView2);
            }

            ImageView imageView1 = (ImageView) dialogView.findViewById(R.id.iv_answer_1);
            imageView1.setOnClickListener(this);

            imageFile = new File(getImagePath(answers[0].getIcon()));

            if (imageFile.exists()) {
                Uri imageUri = Uri.fromFile(imageFile);
                LView.loadImage(this, imageUri, imageView1);
            }
        }

        AlertDialog.Builder builder;

        builder = new AlertDialog.Builder(this, R.style.AlertDialogLight);

        builder.setView(dialogView);
        builder.setTitle(AssetReader.getAppText(KEY_SELECT_ANSWER));

        answersDialog = builder.create();
        answersDialog.show();
        answersDialog.setCanceledOnTouchOutside(false);

        Point size = new Point();
        getWindowManager().getDefaultDisplay().getSize(size);
        answersDialog.getWindow()
                .setLayout(size.x, ViewGroup.LayoutParams.MATCH_PARENT);
    }

    @OnClick(R.id.iv_prev)
    void onPrev() {
        currentQuestion--;
        currentAnswer = selectedAnswers[currentQuestion];
        ivNext.setVisibility(View.VISIBLE);
        showQuestion();
    }

    @OnClick(R.id.iv_next)
    void onNext() {
        selectedAnswers[currentQuestion] = currentAnswer;
        currentQuestion++;
        currentAnswer = selectedAnswers[currentQuestion];
        showQuestion();

        if (-1 == currentAnswer || currentQuestion == postQuestions.length - 1) {
            ivNext.setVisibility(View.GONE);
        }
    }

    @OnClick(R.id.btn_submit)
    void onSubmit() {
        stopAudio();

        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(this);
        builder.setMessage(AssetReader.getAppText(KEY_CONFIRM_PROCEED));
        builder.setPositiveButton(AssetReader.getAppText(KEY_YES), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                selectedAnswers[currentQuestion] = currentAnswer;

                ArrayList<ColumnQuestionAnswer> questionAnswers = new ArrayList<>(16);

                for (int i = 0; i < selectedAnswers.length; i++) {
                    ColumnQuestionAnswer columnQuestionAnswer = new ColumnQuestionAnswer();
                    columnQuestionAnswer.setQuestion(postQuestionsEnglish[i].getQuestion());
                    columnQuestionAnswer.setAnswer(postQuestionsEnglish[i].getAnswers()[selectedAnswers[i]].getAnswer());

                    questionAnswers.add(columnQuestionAnswer);
                }

                AssetReader.setPostQuestionQuestionAnswers(questionAnswers, selectedAnswers);
                AssetReader.surveyEnded();

                AssetReader.saveSurveyData(dataManager, ActivityPostQuestions.this.getLocation());

                startActivity(new Intent(ActivityPostQuestions.this, ActivityResult.class));
                finish();
            }
        });

        builder.setNegativeButton(AssetReader.getAppText(KEY_CANCEL), null);
        builder.show();
    }

    @Override
    public void onClick(View view) {
        int position = -1;

        switch (view.getId()) {
            case R.id.iv_answer_1: {
                position = 0;
            }
            break;

            case R.id.iv_answer_2: {
                position = 1;
            }
            break;

            case R.id.iv_answer_3: {
                position = 2;
            }
            break;

            case R.id.iv_answer_4: {
                position = 3;
            }
            break;
        }

        if (-1 < position) {
            onAnswerSelected(position);
        }

        if (null != answersDialog && answersDialog.isShowing()) {
            answersDialog.dismiss();

            if (-1 < position) {
                showGifAnswer();
            }
        }
    }
}