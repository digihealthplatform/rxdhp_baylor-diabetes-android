package com.dhp.baylordiabetes.ui.user;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.dhp.baylordiabetes.R;
import com.dhp.baylordiabetes.data.asset.AssetReader;
import com.dhp.baylordiabetes.ui.ActivityBase;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.dhp.baylordiabetes.data.asset.AssetReader.KEY_ANIMATION_BY;
import static com.dhp.baylordiabetes.data.asset.AssetReader.KEY_CONSULTANTS;
import static com.dhp.baylordiabetes.data.asset.AssetReader.KEY_COPYRIGHT;
import static com.dhp.baylordiabetes.data.asset.AssetReader.KEY_CO_INVESTIGATORS;
import static com.dhp.baylordiabetes.data.asset.AssetReader.KEY_CREATIVE_DIRECTORS;
import static com.dhp.baylordiabetes.data.asset.AssetReader.KEY_DEVELOPED_BY;
import static com.dhp.baylordiabetes.data.asset.AssetReader.KEY_FUNDED;
import static com.dhp.baylordiabetes.data.asset.AssetReader.KEY_NURSE_CONSULTANT;
import static com.dhp.baylordiabetes.data.asset.AssetReader.KEY_PRIMARY_INVESTIGATOR;
import static com.dhp.baylordiabetes.data.asset.AssetReader.KEY_START_NEW;
import static com.dhp.baylordiabetes.data.asset.AssetReader.KEY_TITLE_THANK_YOU;
import static com.dhp.baylordiabetes.util.LUtils.getImagePath;


public class ActivityThankYou extends ActivityBase {

    @BindView(R.id.tv_primary_investigator)
    TextView tvPrimaryInvestigator;

    @BindView(R.id.tv_co_investigators)
    TextView tvCoInvestigators;

    @BindView(R.id.tv_creative_director)
    TextView tvCreativeDirector;

    @BindView(R.id.tv_medical_consultants)
    TextView tvMedicalConsultants;

    @BindView(R.id.tv_nurse_consultant)
    TextView tvNurseConsultant;

    @BindView(R.id.tv_developed_by)
    TextView tvDevelopedBy;

    @BindView(R.id.tv_animation_by)
    TextView tvAnimationBy;

    @BindView(R.id.iv_thank_you)
    ImageView ivThankYou;

    @BindView(R.id.btn_start_new)
    Button btnStartNew;

    @BindView(R.id.tv_copyright)
    TextView tvCopyright;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_thank_you);
        ButterKnife.bind(this);

        initToolbar(AssetReader.getAppText(KEY_TITLE_THANK_YOU));

        File imageFile = new File(getImagePath("monkey.gif"));

        if (imageFile.exists()) {
            Uri imageUri = Uri.fromFile(imageFile);
            Glide.with(this)
                    .load(imageUri)
                    .asGif()
                    .into(ivThankYou);
        }

        btnStartNew.setText(AssetReader.getAppText(KEY_START_NEW));

        tvPrimaryInvestigator.setText(AssetReader.getStringValue(KEY_PRIMARY_INVESTIGATOR));
        tvCoInvestigators.setText(AssetReader.getStringValue(KEY_CO_INVESTIGATORS));
        tvCreativeDirector.setText(AssetReader.getStringValue(KEY_CREATIVE_DIRECTORS));
        tvMedicalConsultants.setText(AssetReader.getStringValue(KEY_FUNDED));
        tvNurseConsultant.setText(AssetReader.getStringValue(KEY_NURSE_CONSULTANT));
        tvDevelopedBy.setText(AssetReader.getStringValue(KEY_DEVELOPED_BY));
        tvAnimationBy.setText(AssetReader.getStringValue(KEY_ANIMATION_BY));
        tvCopyright.setText(AssetReader.getStringValue(KEY_COPYRIGHT));
    }

    @OnClick(R.id.btn_start_new)
    void onStartNew() {
        Intent intent = new Intent(ActivityThankYou.this, ActivityLanguageSelection.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        overridePendingTransition(0, 0);
    }

    @Override
    public void onBackPressed() {
//        finish();
    }
}