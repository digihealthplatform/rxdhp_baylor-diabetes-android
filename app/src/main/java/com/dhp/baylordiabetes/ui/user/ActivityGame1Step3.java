package com.dhp.baylordiabetes.ui.user;

import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.Button;
import android.widget.VideoView;

import com.dhp.baylordiabetes.R;
import com.dhp.baylordiabetes.data.asset.AssetReader;
import com.dhp.baylordiabetes.ui.ActivityBase;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.dhp.baylordiabetes.util.LConstants.AUDIO_PATH;
import static com.dhp.baylordiabetes.util.LConstants.VIDEO_PATH;


public class ActivityGame1Step3 extends ActivityBase {

    @BindView(R.id.video_view)
    VideoView videoView;

    @BindView(R.id.btn_start)
    Button btnStart;

    private int currentVideo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_1);
        ButterKnife.bind(this);
        initToolbar(AssetReader.getGameTitle(1));

        btnBlink = btnStart;
        blinkHandler.postDelayed(blinkRunnable, 500);

        currentVideo = getIntent().getIntExtra("current_video", 0);

        playVideo(AssetReader.getGameVideo(1, 4));
        playAudioFile(AssetReader.getGameAudio(1, 4));
    }

    void playVideo(String videoUrl) {
        videoView.setVideoPath(Environment.getExternalStorageDirectory().getAbsolutePath() + VIDEO_PATH + videoUrl);
        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.setVolume(0f, 0f);
                mp.setLooping(true);
            }
        });
        videoView.requestFocus();

        videoView.start();
    }

    @OnClick(R.id.btn_start)
    void onStartClicked() {
        Intent intent = new Intent(this, ActivityGame1Step4.class);
        intent.putExtra("current_video", currentVideo);
        startActivity(intent);
    }

    private void playAudioFile(final String fileName) {

        new Thread(new Runnable() {
            @Override
            public void run() {
                final File audioFile = new File(Environment.getExternalStorageDirectory(), AUDIO_PATH + fileName);

                if (!audioFile.exists()) {
                    return;
                }

                audioPlayerGame = MediaPlayer.create(ActivityGame1Step3.this, Uri.fromFile(audioFile));
                audioPlayerGame.start();
                audioPlayerGame.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mediaPlayer) {
                        btnStart.setVisibility(View.VISIBLE);
                    }
                });
            }
        }).start();
    }
}