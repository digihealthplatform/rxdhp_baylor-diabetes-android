package com.dhp.baylordiabetes.ui.admin;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.dhp.baylordiabetes.R;
import com.dhp.baylordiabetes.data.DataManager;
import com.dhp.baylordiabetes.data.adapter.AdapterUser;
import com.dhp.baylordiabetes.data.asset.AssetReader;
import com.dhp.baylordiabetes.data.table.TableUser;
import com.dhp.baylordiabetes.ui.ActivityBase;
import com.dhp.baylordiabetes.ui.login.ActivityBaseLogin;
import com.dhp.baylordiabetes.util.LUtils;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;

import static com.dhp.baylordiabetes.data.database.SharedPrefsHelper.AUTO_SYNC_ENABLED;
import static com.dhp.baylordiabetes.data.database.SharedPrefsHelper.KEY_AUTO_SYNC;


public class ActivityBaseAdminHome extends ActivityBase {

    @BindView(R.id.tv_empty)
    TextView tvEmpty;

    @BindView(R.id.rv_user_list)
    RecyclerView rvUserList;

    @BindView(R.id.iv_menu)
    ImageView ivMenu;

    @BindView(R.id.cb_auto_sync)
    CheckBox cbAutoSync;

    @Inject
    DataManager dataManager;

    private AdapterUser        adapterUser;
    private InputMethodManager inputMethodManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_home);
        ButterKnife.bind(this);
        getActivityComponent(this).inject(this);

        inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

        adapterUser = new AdapterUser(this, dataManager, rvUserList, tvEmpty);
        rvUserList.setLayoutManager(new LinearLayoutManager(this));
        rvUserList.setAdapter(adapterUser);

        cbAutoSync.setChecked(dataManager.getBooleanPref(KEY_AUTO_SYNC, AUTO_SYNC_ENABLED));
        cbAutoSync.setOnCheckedChangeListener(new CheckBox.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                dataManager.setPref(KEY_AUTO_SYNC, b);
            }
        });

        boolean initSuccess = AssetReader.init();
        if (!initSuccess) {
            logout();
            return;
        }

        initToolbar(getString(R.string.title_admin_home));
        AssetReader.setSelectedLanguage(0);
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(getString(R.string.message_confirm_exit));
        builder.setPositiveButton(getString(R.string.btn_exit), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        builder.setNegativeButton(getString(R.string.btn_cancel), null);
        builder.show();
    }

    @OnClick(R.id.iv_menu)
    void onMenuClick() {
        PopupMenu popup = new PopupMenu(this, ivMenu);

        popup.getMenuInflater()
                .inflate(R.menu.menu_admin_home, popup.getMenu());

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                onMenuItemClicked(item);

                return true;
            }
        });

        popup.show();
    }

    private void onMenuItemClicked(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_logout: {
                AlertDialog.Builder builder;

                builder = new AlertDialog.Builder(ActivityBaseAdminHome.this, R.style.AlertDialogLight);

                builder.setMessage(getString(R.string.confirm_logout_admin));
                builder.setPositiveButton(getString(R.string.btn_logout), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        logout();
                    }
                });
                builder.setNegativeButton(getString(R.string.btn_cancel), null);
                builder.show();
            }
            break;

//            case R.id.action_export: {
//                LUtils.saveFile(this, dataManager.getSurveyData());
//            }
        }
    }

    private void logout() {
        dataManager.logout();
        startActivity(new Intent(ActivityBaseAdminHome.this, ActivityBaseLogin.class));
        finish();
    }

    @OnClick(R.id.iv_add_user)
    void addUser() {
        View dialogView = LayoutInflater.from(this).inflate(R.layout.dialog_add_user, null);

        AlertDialog.Builder builder;

        builder = new AlertDialog.Builder(this, R.style.AlertDialogLight);

        builder.setView(dialogView);

        builder.setPositiveButton(getString(R.string.btn_save), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        builder.setTitle(R.string.add_user);
        builder.setNegativeButton(getString(R.string.btn_cancel), null);

        final AlertDialog dialog = builder.create();
        dialog.show();
        dialog.setCanceledOnTouchOutside(false);

        final EditText etFullName = (EditText) dialogView.findViewById(R.id.et_full_name);
        final EditText etUsername = (EditText) dialogView.findViewById(R.id.et_username);
        final EditText etPassword = (EditText) dialogView.findViewById(R.id.et_password);
        final CheckBox cbTrainer  = (CheckBox) dialogView.findViewById(R.id.cb_trainer);

        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String fullName = etFullName.getText().toString();
                String username = etUsername.getText().toString();
                String password = etPassword.getText().toString();

                if (!fullName.isEmpty() && !password.isEmpty() && !username.isEmpty()) {

                    List<TableUser> users = dataManager.getAllUsers();

                    for (TableUser tableUser : users) {
                        if (username.equals(tableUser.getUsername())) {
                            Toasty.warning(ActivityBaseAdminHome.this, getString(R.string.msg_user_exits), Toast.LENGTH_SHORT).show();
                            return;
                        }
                    }

                    TableUser tableUser = new TableUser();
                    tableUser.setUserKey(0);
                    tableUser.setUsername(username);
                    tableUser.setFullName(fullName);
                    tableUser.setPassword(password);
                    tableUser.setTrainer(cbTrainer.isChecked());

                    dataManager.createUser(tableUser);

                    adapterUser.notifyDataSetChanged();

                    tvEmpty.setVisibility(View.GONE);
                    rvUserList.setVisibility(View.VISIBLE);

                    inputMethodManager.hideSoftInputFromWindow(etPassword.getWindowToken(), 0);

                    dialog.dismiss();
                    Toasty.success(ActivityBaseAdminHome.this, getString(R.string.msg_user_added), Toast.LENGTH_SHORT).show();

                } else {
                    Toasty.warning(ActivityBaseAdminHome.this, getString(R.string.msg_fill_all_fields), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @OnClick(R.id.btn_export)
    void exportData() {
        LUtils.saveFile(this, dataManager.getSurveyData());
    }
}