package com.dhp.baylordiabetes.ui.user;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.widget.Button;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.dhp.baylordiabetes.R;
import com.dhp.baylordiabetes.data.asset.AssetReader;
import com.dhp.baylordiabetes.ui.ActivityBase;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.dhp.baylordiabetes.util.LUtils.getImagePath;


public class ActivityGameStart extends ActivityBase {

    @BindView(R.id.iv_thumb)
    ImageView ivThumb;

    @BindView(R.id.btn_start)
    Button btnStart;

    private int currentVideo;

    private boolean isDarkBlue;

    protected final Handler blinkHandler = new Handler();

    protected Runnable blinkRunnable = new Runnable() {

        @Override
        public void run() {
            resetButtonColor();

            isDarkBlue = !isDarkBlue;

            blinkHandler.postDelayed(this, 500);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_start);
        ButterKnife.bind(this);

        currentVideo = getIntent().getIntExtra("current_video", 0);

        initToolbar(AssetReader.getGameTitle(currentVideo));

        btnStart.setText(AssetReader.getAppText("start") + "   ");

        btnBlink = btnStart;
        blinkHandler.postDelayed(blinkRunnable, 500);

        resetButtonColor();

        Glide.with(this)
                .load(getImagePath(AssetReader.getGameGif(1, 1)))
                .asGif()
                .into(ivThumb);
    }


    private void resetButtonColor() {
        if (isDarkBlue) {
            if (1 == currentVideo) {
                btnBlink.setBackground(ContextCompat.getDrawable(ActivityGameStart.this, R.drawable.btn_background_yellow));

            } else if (2 == currentVideo) {
                btnBlink.setBackground(ContextCompat.getDrawable(ActivityGameStart.this, R.drawable.btn_background_green));

            } else if (3 == currentVideo) {
                btnBlink.setBackground(ContextCompat.getDrawable(ActivityGameStart.this, R.drawable.btn_background_purple));

            } else {
                btnBlink.setBackground(ContextCompat.getDrawable(ActivityGameStart.this, R.drawable.btn_background_purple));
            }
        } else {
            if (1 == currentVideo) {
                btnBlink.setBackground(ContextCompat.getDrawable(ActivityGameStart.this, R.drawable.btn_background_yellow_dark));

            } else if (2 == currentVideo) {
                btnBlink.setBackground(ContextCompat.getDrawable(ActivityGameStart.this, R.drawable.btn_background_green_dark));

            } else if (3 == currentVideo) {
                btnBlink.setBackground(ContextCompat.getDrawable(ActivityGameStart.this, R.drawable.btn_background_purple_dark));

            } else {
                btnBlink.setBackground(ContextCompat.getDrawable(ActivityGameStart.this, R.drawable.btn_background_purple_dark));
            }
        }
    }

    @OnClick(R.id.btn_start)
    void onStartClicked() {
        Intent intent = null;

        if (1 == currentVideo) {
            intent = new Intent(this, ActivityGame1Step1.class);

        } else if (2 == currentVideo) {
            intent = new Intent(this, ActivityGame2.class);

        } else if (3 == currentVideo) {
            intent = new Intent(this, ActivityGame3.class);

        } else if (4 == currentVideo) {
            intent = new Intent(this, ActivityGame4.class);
        }

        intent.putExtra("current_video", currentVideo);
        startActivity(intent);

        finish();
    }
}