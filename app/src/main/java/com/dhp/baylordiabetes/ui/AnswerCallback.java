package com.dhp.baylordiabetes.ui;


public interface AnswerCallback {
    void onAnswerSelected(int position);
}