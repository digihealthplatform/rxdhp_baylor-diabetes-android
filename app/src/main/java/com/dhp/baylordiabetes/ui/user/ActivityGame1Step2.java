package com.dhp.baylordiabetes.ui.user;

import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.widget.VideoView;

import com.dhp.baylordiabetes.R;
import com.dhp.baylordiabetes.data.asset.AssetReader;
import com.dhp.baylordiabetes.ui.ActivityBase;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.dhp.baylordiabetes.util.LConstants.AUDIO_PATH;
import static com.dhp.baylordiabetes.util.LConstants.VIDEO_PATH;


public class ActivityGame1Step2 extends ActivityBase
        implements MediaPlayer.OnCompletionListener {

    @BindView(R.id.video_view)
    VideoView videoView;

    private int currentVideo;

    private boolean isSuccessPlayed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_1);
        ButterKnife.bind(this);
        initToolbar(AssetReader.getGameTitle(1));

        currentVideo = getIntent().getIntExtra("current_video", 0);

        playVideo(AssetReader.getGameVideo(1, 3));

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!isSuccessPlayed) {
                    playAudioFile(AssetReader.getGameAudioFile("success_audio"));
                    isSuccessPlayed = true;
                }
            }
        }, 3000);
    }

    void playVideo(String videoUrl) {
        videoView.setVideoPath(Environment.getExternalStorageDirectory().getAbsolutePath() + VIDEO_PATH + videoUrl);
        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.setVolume(0f, 0f);
            }
        });
        videoView.setOnCompletionListener(this);
        videoView.requestFocus();

        videoView.start();
    }

    @Override
    public void onCompletion(MediaPlayer mediaPlayer) {
        videoView.start();
    }

    private void playAudioFile(final String fileName) {

        new Thread(new Runnable() {
            @Override
            public void run() {
                final File audioFile = new File(Environment.getExternalStorageDirectory(), AUDIO_PATH + fileName);

                if (!audioFile.exists()) {
                    return;
                }

                audioPlayerGame = MediaPlayer.create(ActivityGame1Step2.this, Uri.fromFile(audioFile));
                audioPlayerGame.start();
                audioPlayerGame.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mediaPlayer) {
                        Intent intent = new Intent(ActivityGame1Step2.this, ActivityGame1Step3.class);
                        intent.putExtra("current_video", currentVideo);
                        startActivity(intent);
                        finish();
                    }
                });
            }
        }).start();
    }
}