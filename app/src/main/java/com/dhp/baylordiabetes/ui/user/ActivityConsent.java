package com.dhp.baylordiabetes.ui.user;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;

import com.dhp.baylordiabetes.R;
import com.dhp.baylordiabetes.data.adapter.AdapterConsent;
import com.dhp.baylordiabetes.data.asset.AssetReader;
import com.dhp.baylordiabetes.data.item.ConsentData;
import com.dhp.baylordiabetes.ui.ActivityBase;
import com.dhp.baylordiabetes.util.LView;

import java.io.File;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.dhp.baylordiabetes.util.LUtils.getImagePath;


public class ActivityConsent extends ActivityBase {

    private static final String TAG = ActivityConsent.class.getSimpleName();

    @BindView(R.id.rv_consent)
    RecyclerView rvConsent;

    @BindView(R.id.iv_play_audio)
    ImageView ivPlayAudio;

    @BindView(R.id.iv_yes)
    ImageView ivYes;

    @BindView(R.id.iv_no)
    ImageView ivNo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consent);
        ButterKnife.bind(this);
        ConsentData consentData = AssetReader.getConsent();
        initToolbar(consentData.getTitle());

        AdapterConsent adapterConsent = new AdapterConsent(this, consentData.getPoints());
        rvConsent.setLayoutManager(new LinearLayoutManager(this));
        rvConsent.setAdapter(adapterConsent);

        File imageFile = new File(getImagePath(consentData.getAgreeIcon()));

        if (imageFile.exists()) {
            Uri imageUri = Uri.fromFile(imageFile);
            LView.loadImage(this, imageUri, ivYes);
        }

        imageFile = new File(getImagePath(consentData.getCancelIcon()));

        if (imageFile.exists()) {
            Uri imageUri = Uri.fromFile(imageFile);
            LView.loadImage(this, imageUri, ivNo);
        }

        AssetReader.surveyStarted();

        initAudio(ivPlayAudio, consentData.getAudioFile());

        AssetReader.consentStartDateTime = Calendar.getInstance().getTimeInMillis();

        requestLocationWithPermissionCheck();
    }

    @OnClick(R.id.iv_yes)
    void onYes() {
        startActivity(new Intent(this, ActivityLocation.class));
        finish();
    }

    @Override
    protected void onStop() {
        stopAudio();
        super.onStop();
    }

    @OnClick(R.id.iv_no)
    void onNo() {
        finish();
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}