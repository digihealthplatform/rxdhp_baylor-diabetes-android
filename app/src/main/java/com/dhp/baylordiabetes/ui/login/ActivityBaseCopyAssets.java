package com.dhp.baylordiabetes.ui.login;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.widget.Toast;

import com.dhp.baylordiabetes.R;
import com.dhp.baylordiabetes.data.DataManager;
import com.dhp.baylordiabetes.data.asset.CopyAssetsTask;
import com.dhp.baylordiabetes.ui.ActivityBase;
import com.dhp.baylordiabetes.util.LToast;

import java.io.File;

import javax.inject.Inject;

import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;

import static com.dhp.baylordiabetes.util.LConstants.AUDIO_PATH;
import static com.dhp.baylordiabetes.util.LConstants.CODE_WRITE_PERMISSION;
import static com.dhp.baylordiabetes.util.LConstants.FILES_PATH_HIDDEN;
import static com.dhp.baylordiabetes.util.LConstants.GIF_PATH;
import static com.dhp.baylordiabetes.util.LConstants.ICONS_PATH;
import static com.dhp.baylordiabetes.util.LConstants.INPUT_JSON_PATH;
import static com.dhp.baylordiabetes.util.LConstants.OUTPUT_JSON_PATH;
import static com.dhp.baylordiabetes.util.LConstants.VIDEO_PATH;


public class ActivityBaseCopyAssets extends ActivityBase
        implements CopyAssetsTask.CallBack {

    @Inject
    DataManager dataManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_copy_assets);
        ButterKnife.bind(this);
        getActivityComponent(this).inject(this);

        initToolbar(getString(R.string.app_name));

        createFolderWithPermissionCheck();
    }

    @Override
    public void onBackPressed() {
    }

    private void createFolderWithPermissionCheck() {
        int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);

        if (PackageManager.PERMISSION_GRANTED != permissionCheck) {

            ActivityCompat.requestPermissions((Activity) this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    CODE_WRITE_PERMISSION);
        } else {
            createFolders();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {

        switch (requestCode) {
            case CODE_WRITE_PERMISSION: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    createFolders();
                } else {
                    Toasty.warning(this, getString(R.string.message_permission_needed), Toast.LENGTH_SHORT).show();
                    finish();
                }
            }
            break;
        }
    }

    private void createFolders() {

        createFolder(VIDEO_PATH);
        createFolder(AUDIO_PATH);
        createFolder(OUTPUT_JSON_PATH);
        createFolder(FILES_PATH_HIDDEN);
        createFolder(ICONS_PATH);
        createFolder(GIF_PATH);
        createFolder(INPUT_JSON_PATH);

        new CopyAssetsTask(this, this).execute();
    }

    private void createFolder(String folderPath) {

        File filePath = new File(Environment.getExternalStorageDirectory(), folderPath);
        if (!filePath.exists()) {
            filePath.mkdirs();
        }
    }

    @Override
    public void onAssetCopied(boolean copySuccess) {

        if (copySuccess) {
            dataManager.onAssetsCopied();
            startActivity(new Intent(this, ActivityBaseLoginCheck.class));

        } else {
            LToast.warning(getString(R.string.invalid_apk));
        }

        finish();
    }
}