package com.dhp.baylordiabetes.ui;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.dhp.baylordiabetes.LApplication;
import com.dhp.baylordiabetes.R;
import com.dhp.baylordiabetes.data.asset.AssetReader;
import com.dhp.baylordiabetes.data.di.component.ActivityComponent;
import com.dhp.baylordiabetes.data.di.component.DaggerActivityComponent;
import com.dhp.baylordiabetes.data.di.module.ActivityModule;
import com.dhp.baylordiabetes.ui.user.ActivityLanguageSelection;
import com.dhp.baylordiabetes.util.LLog;
import com.dhp.baylordiabetes.util.LToast;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;

import java.io.File;

import es.dmoral.toasty.Toasty;

import static com.dhp.baylordiabetes.data.asset.AssetReader.KEY_ICON_BAYLOR;
import static com.dhp.baylordiabetes.data.asset.AssetReader.KEY_ICON_BBH;
import static com.dhp.baylordiabetes.util.LConstants.AUDIO_PATH;
import static com.dhp.baylordiabetes.util.LUtils.getImagePath;


public class ActivityBase extends AppCompatActivity
        implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        com.google.android.gms.location.LocationListener,
        ResultCallback<LocationSettingsResult> {

    private static final String TAG = ActivityBase.class.getSimpleName();

    private LinearLayout llFullScreenGif;
    private ImageView    ivGif;

    int currentAudioFile = 0;

    private boolean isAudioPlaying;

    private String[] audioFiles;

    protected ActivityComponent activityComponent;
    private   MediaPlayer       audioPlayer;
    private   ImageView         ivPlayAudio;

    protected final Handler blinkHandler = new Handler();

    protected Button btnBlink;

    private boolean isDarkBlue;

    protected GoogleApiClient googleApiClient;

    protected static Location        lastLocation;
    protected        LocationRequest locationRequest;

    private             int REQUEST_CHECK_SETTINGS   = 100;
    public static final int CODE_LOCATION_PERMISSION = 102;

    private boolean locationRequestDialogShown;

    protected MediaPlayer audioPlayerGame;

    protected Runnable blinkRunnable = new Runnable() {

        @Override
        public void run() {
            if (isDarkBlue) {
                btnBlink.setBackground(ContextCompat.getDrawable(ActivityBase.this, R.drawable.btn_background_yellow));
            } else {
                btnBlink.setBackground(ContextCompat.getDrawable(ActivityBase.this, R.drawable.btn_background_yellow_dark));
            }

            isDarkBlue = !isDarkBlue;

            blinkHandler.postDelayed(this, 500);
        }
    };

    protected void initToolbar(String title) {
        ImageView ivBbhLogo    = findViewById(R.id.iv_bbh_logo);
        ImageView ivBaylorLogo = findViewById(R.id.iv_baylor_logo);
        TextView  tvTitle      = findViewById(R.id.tv_title);

        String imgPath = getImagePath(AssetReader.getStringValue(KEY_ICON_BBH));
        Glide.with(this)
                .load(imgPath)
                //                .placeholder(R.drawable.loader)
                .into(ivBbhLogo);

        imgPath = getImagePath(AssetReader.getStringValue(KEY_ICON_BAYLOR));
        Glide.with(this)
                .load(imgPath)
//                .placeholder(R.drawable.loader)
                .into(ivBaylorLogo);

        tvTitle.setText(title);
    }

    protected void initViews() {
        LayoutInflater inflater = getLayoutInflater();
        llFullScreenGif = (LinearLayout) inflater.inflate(R.layout.layout_gif, null);
        ViewGroup rootViewGroup = (ViewGroup) findViewById(android.R.id.content);
        rootViewGroup.addView(llFullScreenGif);

        llFullScreenGif.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                llFullScreenGif.setVisibility(View.GONE);
            }
        });

        ivGif = llFullScreenGif.findViewById(R.id.iv_gif);
    }

    protected void requestLocationWithPermissionCheck() {

        if (null != lastLocation) {
            return;
        }

        if (!checkPlayServices()) {
            LToast.warning("Play services not available grant location permission");
            return;
        }

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_COARSE_LOCATION},
                    CODE_LOCATION_PERMISSION);

        } else {
            fetchLocation();
        }
    }

    protected void stopLocationRequest() {
        if (googleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(googleApiClient, this);
            googleApiClient.disconnect();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {

            case CODE_LOCATION_PERMISSION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    fetchLocation();
                } else {
                    locationRequestDialogShown = false;
                    LToast.error(AssetReader.getAppText("grant_location_permission"));
                }
        }

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    protected synchronized void fetchLocation() {
        if (null == locationRequest) {
            locationRequest = LocationRequest.create();
            locationRequest.setInterval(1000);
            locationRequest.setFastestInterval(1000);
            locationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        }

        googleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        googleApiClient.connect();
    }

    @Override
    public void onConnected(Bundle connectionHint) {

        lastLocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);

        if (null != lastLocation) {
            return;
        }

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);
        builder.setAlwaysShow(true);

        PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(
                googleApiClient,
                builder.build());

        result.setResultCallback(this);
    }

    @Override
    public void onResult(@NonNull LocationSettingsResult locationSettingsResult) {
        final Status status = locationSettingsResult.getStatus();

        switch (status.getStatusCode()) {

            case LocationSettingsStatusCodes.SUCCESS:
                LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
                break;

            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                try {
                    if (!locationRequestDialogShown) {
                        status.startResolutionForResult(ActivityBase.this, REQUEST_CHECK_SETTINGS);
                        locationRequestDialogShown = true;
                    }

                } catch (IntentSender.SendIntentException e) {
                    LLog.printStackTrace(e);
                }
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CHECK_SETTINGS) {

            if (resultCode == RESULT_OK) {
                LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);

            } else {
                locationRequestDialogShown = false;
                LToast.error(AssetReader.getAppText("grant_location_permission"));
            }
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        Log.d(TAG, "onConnectionFailed: ");
    }

    @Override
    public void onConnectionSuspended(int cause) {
        Log.d(TAG, "onConnectionSuspended: ");
    }

    @Override
    public void onLocationChanged(Location location) {
        this.lastLocation = location;
        stopLocationRequest();
    }

    private boolean checkPlayServices() {
        int resultCode = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(this);
        return resultCode == ConnectionResult.SUCCESS;
    }

    public String getLocation() {
        if (null != lastLocation) {
            return lastLocation.getLatitude() + "," + lastLocation.getLongitude();
        }
        return "";
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        try {
            audioPlayerGame.stop();
        } catch (Exception e) {
        }
    }

    protected void showFullScreenGif(String imageName) {
        llFullScreenGif.setVisibility(View.VISIBLE);

        Glide.with(this)
                .load(getImagePath(imageName))
                .asGif()
                .into(ivGif);
    }

    protected void playAudio(String fileName, final boolean toHide) {
        final File audioFile = new File(Environment.getExternalStorageDirectory(), AUDIO_PATH + fileName);

        stopAudio();

        if (!audioFile.exists()) {
            llFullScreenGif.setVisibility(View.GONE);
            return;
        }

        audioPlayer = MediaPlayer.create(this, Uri.fromFile(audioFile));
        audioPlayer.start();
        audioPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                if (toHide) {
                    llFullScreenGif.setVisibility(View.GONE);
                }
                onAudioComplete();
            }
        });
    }

    protected void onAudioComplete() {
    }

    protected void hideFullScreenGif() {
        llFullScreenGif.setVisibility(View.GONE);
    }

    protected ActivityComponent getActivityComponent(Activity activity) {
        if (activityComponent == null) {
            activityComponent = DaggerActivityComponent.builder()
                    .activityModule(new ActivityModule(activity))
                    .applicationComponent(LApplication.get(activity).getComponent())
                    .build();
        }
        return activityComponent;
    }

    @Override
    public void onBackPressed() {
        confirmBackPressed();
    }

    protected void confirmBackPressed() {
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(this);
        builder.setMessage(AssetReader.getAppText(AssetReader.KEY_CONFIRM_ON_BACK));
        builder.setPositiveButton(AssetReader.getAppText(AssetReader.KEY_YES), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
//                ActivityBase.super.onBackPressed();
                Intent intent = new Intent(ActivityBase.this, ActivityLanguageSelection.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                overridePendingTransition(0, 0);
            }
        });
        builder.setNegativeButton(AssetReader.getAppText(AssetReader.KEY_NO), null);
        builder.show();
    }

    protected void initAudio(ImageView ivPlayAudio, String audioFileName) {
        stopAudio();

        ivPlayAudio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isAudioPlaying) {
                    LApplication.setAutoPlayAudio(false);
                    stopAudio();
                } else {
                    LApplication.setAutoPlayAudio(true);
                    playAudio();
                }
            }
        });

        if (null == audioFileName || audioFileName.isEmpty()) {
            audioFiles = null;
            return;
        }

        this.ivPlayAudio = ivPlayAudio;

        this.audioFiles = audioFileName.split(",");
        currentAudioFile = 0;

        if (LApplication.getAutoPlayAudio()) {
            playAudio();
        }
    }

    protected void playAudio() {
        if (null == audioFiles) {
            return;
        }

        final File audioFile = new File(Environment.getExternalStorageDirectory(), AUDIO_PATH + audioFiles[currentAudioFile]);

        if (!audioFile.exists()) {
            Toasty.warning(this, "Audio file doesn't exit").show();
            LApplication.setAutoPlayAudio(false);
            return;
        }
        ivPlayAudio.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.playing_audio));
        audioPlayer = MediaPlayer.create(this, Uri.fromFile(audioFile));

        try {
            audioPlayer.start();
            audioPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mediaPlayer) {
                    ++currentAudioFile;

                    if (null == audioFiles) {
                        return;
                    }

                    if (currentAudioFile >= audioFiles.length) {
                        ivPlayAudio.setImageDrawable(ContextCompat.getDrawable(ActivityBase.this, R.drawable.play_audio));
                        currentAudioFile = 0;
                        isAudioPlaying = false;
                    } else {
                        playAudio();
                    }
                }
            });
        } catch (Exception e) {
        }

        isAudioPlaying = true;
    }

    protected void stopAudio() {
        try {
            audioPlayer.stop();
            ivPlayAudio.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.play_audio));
            isAudioPlaying = false;
        } catch (Exception e) {
            LLog.printStackTrace(e);
        }
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }
}