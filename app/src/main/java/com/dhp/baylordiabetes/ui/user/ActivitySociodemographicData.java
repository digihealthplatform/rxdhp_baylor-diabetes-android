package com.dhp.baylordiabetes.ui.user;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.dhp.baylordiabetes.R;
import com.dhp.baylordiabetes.data.adapter.AdapterAnswer;
import com.dhp.baylordiabetes.data.asset.AssetReader;
import com.dhp.baylordiabetes.data.item.ColumnQuestionAnswer;
import com.dhp.baylordiabetes.data.item.ItemAnswer;
import com.dhp.baylordiabetes.data.item.ItemQuestion;
import com.dhp.baylordiabetes.data.item.ItemQuestionAnswer;
import com.dhp.baylordiabetes.data.item.SociodemographicData;
import com.dhp.baylordiabetes.ui.AnswerCallback;
import com.dhp.baylordiabetes.ui.ActivityBase;

import java.util.ArrayList;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.dhp.baylordiabetes.data.asset.AssetReader.KEY_CONFIRM_PROCEED;
import static com.dhp.baylordiabetes.data.asset.AssetReader.KEY_NO;
import static com.dhp.baylordiabetes.data.asset.AssetReader.KEY_YES;


public class ActivitySociodemographicData extends ActivityBase
        implements AnswerCallback {

    private String parentQuestion;
    private String parentQuestionEnglish;

    private ItemQuestion currentQuestion;
    private ItemQuestion currentQuestionEnglish;

    private ItemQuestion[] questions;
    private ItemQuestion[] questionsEnglish;

    private ItemQuestion[] nestedQuestions;
    private ItemQuestion[] nestedQuestionsEnglish;

    private ItemQuestionAnswer questionAnswer;
    private ItemQuestionAnswer questionAnswerEnglish;

    private ArrayList<ItemQuestionAnswer> questionAnswers;
    private ArrayList<ItemQuestionAnswer> questionAnswersStack;
    private ArrayList<ItemQuestionAnswer> questionAnswersStackEnglish;

    private InputMethodManager inputMethodManager;

    @BindView(R.id.tv_question)
    TextView tvQuestion;

    @BindView(R.id.rv_answers)
    RecyclerView rvAnswers;

    @BindView(R.id.et_answer)
    EditText etAnswer;

    @BindView(R.id.iv_play_audio)
    ImageView ivPlayAudio;

    @BindView(R.id.iv_prev)
    ImageView ivPrev;

    @BindView(R.id.iv_next)
    ImageView ivNext;

    @BindView(R.id.btn_submit)
    Button btnSubmit;

    private String answerString;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sociodemographic_data);
        ButterKnife.bind(this);

        inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

        etAnswer.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.toString().isEmpty()) {
                    ivNext.setVisibility(View.GONE);

                } else {
                    try {
                        if (Integer.parseInt(charSequence.toString()) > 0) {
                            ivNext.setVisibility(View.VISIBLE);
                            answerString = charSequence.toString();
                        } else {
                            ivNext.setVisibility(View.GONE);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        questionAnswers = new ArrayList<>();
        questionAnswersStack = new ArrayList<>();
        questionAnswersStackEnglish = new ArrayList<>();

        btnSubmit.setText(AssetReader.getAppText(AssetReader.KEY_SUBMIT));

        initData();
        showQuestion();

        AssetReader.sociodemographicStartDateTime = Calendar.getInstance().getTimeInMillis();
    }

    @Override
    protected void onStop() {
        stopAudio();
        super.onStop();
    }

    private void initData() {
        SociodemographicData sociodemographicData        = AssetReader.getSocioDemographicQuestions(AssetReader.getJsonData());
        SociodemographicData sociodemographicDataEnglish = AssetReader.getSocioDemographicQuestions(AssetReader.getJsonDataEnglish());

        initToolbar(sociodemographicData.getTitle());

        questions = sociodemographicData.getQuestions();
        questionsEnglish = sociodemographicDataEnglish.getQuestions();

        currentQuestion = questions[0];
        currentQuestionEnglish = questionsEnglish[0];
    }

    private void showQuestion() {

        initAudio(ivPlayAudio, currentQuestion.getAudioFile());

        tvQuestion.setText(currentQuestion.getQuestion());

        if (null == currentQuestion.getAnswers()) {
            etAnswer.setVisibility(View.VISIBLE);

            if (!etAnswer.getText().toString().isEmpty()) {
                answerString = etAnswer.getText().toString();
                ivNext.setVisibility(View.VISIBLE);

            } else {
                ivNext.setVisibility(View.GONE);
            }

            rvAnswers.setVisibility(View.GONE);

            nestedQuestions = null;
            nestedQuestionsEnglish = null;

            return;
        } else {
            etAnswer.setVisibility(View.GONE);
        }

        AdapterAnswer adapterAnswer = new AdapterAnswer(this, currentQuestion.getAnswers());
        rvAnswers.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        rvAnswers.setAdapter(adapterAnswer);
        rvAnswers.setVisibility(View.VISIBLE);

        int answerIndex = getAnswerIndex(getUserAnswer(currentQuestion.getQuestion()), currentQuestion.getAnswers());

        if (-1 != answerIndex) {
            adapterAnswer.setSelection(answerIndex);
            ivNext.setVisibility(View.VISIBLE);

        } else {
            ivNext.setVisibility(View.GONE);
        }

        if (isFirstQuestion()) {
            ivPrev.setVisibility(View.GONE);
        } else {
            ivPrev.setVisibility(View.VISIBLE);
        }

        btnSubmit.setVisibility(View.GONE);

        handleNestedQuestions(answerIndex);
    }

    private void handleNestedQuestions(int answerIndex) {
        if (-1 != answerIndex || (null == currentQuestion.getAnswers() && !tvQuestion.getText().toString().isEmpty())) {
            if (null != currentQuestion.getAnswers()[answerIndex].getQuestions()) {
                nestedQuestions = currentQuestion.getAnswers()[answerIndex].getQuestions();
                nestedQuestionsEnglish = currentQuestionEnglish.getAnswers()[answerIndex].getQuestions();

                parentQuestion = currentQuestion.getQuestion();
                parentQuestionEnglish = currentQuestionEnglish.getQuestion();

            } else {
                nestedQuestions = null;
                nestedQuestionsEnglish = null;
            }
        }
    }


    @Override
    public void onAnswerSelected(int position) {
        questionAnswer = new ItemQuestionAnswer();
        questionAnswer.setQuestion(currentQuestion);
        questionAnswer.setAnswer(currentQuestion.getAnswers()[position].getAnswer());

        questionAnswerEnglish = new ItemQuestionAnswer();
        questionAnswerEnglish.setQuestion(currentQuestionEnglish);
        questionAnswerEnglish.setAnswer(currentQuestionEnglish.getAnswers()[position].getAnswer());

        if (null != parentQuestion) {
            questionAnswer.setParentQuestion(parentQuestion);
            questionAnswerEnglish.setParentQuestion(parentQuestionEnglish);
        }

        if (null != currentQuestion.getAnswers()[position].getQuestions()) {
            nestedQuestions = currentQuestion.getAnswers()[position].getQuestions();
            nestedQuestionsEnglish = currentQuestionEnglish.getAnswers()[position].getQuestions();

            parentQuestion = currentQuestion.getQuestion();
            parentQuestionEnglish = currentQuestionEnglish.getQuestion();

        } else {
            nestedQuestions = null;
            nestedQuestionsEnglish = null;
        }

        if (isLastQuestion()) {
            ivNext.setVisibility(View.GONE);
            btnSubmit.setVisibility(View.VISIBLE);
        } else {
            ivNext.setVisibility(View.VISIBLE);
            btnSubmit.setVisibility(View.GONE);
        }
    }

    private boolean isFirstQuestion() {
        return currentQuestion.getQuestion().equals(questions[0].getQuestion());
    }

    private boolean isLastQuestion() {
        return currentQuestion.getQuestion().equals(questions[questions.length - 1].getQuestion());
    }

    @OnClick(R.id.iv_prev)
    void onPrev() {
        inputMethodManager.hideSoftInputFromWindow(etAnswer.getWindowToken(), 0);

        if (null == currentQuestion.getAnswers()) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    gotoPreviousQuestion();
                }
            }, 200);
        } else {
            gotoPreviousQuestion();
        }
    }

    private void gotoPreviousQuestion() {
        currentQuestion = getPreviousQuestion();
        currentQuestionEnglish = getPreviousQuestionEnglish();

        showQuestion();

        ivNext.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.iv_next)
    void onNext() {

        inputMethodManager.hideSoftInputFromWindow(etAnswer.getWindowToken(), 0);

        if (null == currentQuestion.getAnswers()) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    gotoNextQuestion();
                }
            }, 200);
        } else {
            gotoNextQuestion();
        }
    }

    private void gotoNextQuestion() {

        if (null != answerString) {
            questionAnswer = new ItemQuestionAnswer();
            questionAnswer.setQuestion(currentQuestion);
            questionAnswer.setAnswer(answerString);

            questionAnswerEnglish = new ItemQuestionAnswer();
            questionAnswerEnglish.setQuestion(currentQuestionEnglish);
            questionAnswerEnglish.setAnswer(answerString);

            if (null != parentQuestion) {
                questionAnswer.setParentQuestion(parentQuestion);
                questionAnswerEnglish.setParentQuestion(parentQuestionEnglish);
            }

            answerString = null;
        }

        questionAnswersStack.add(questionAnswer);
        questionAnswersStackEnglish.add(questionAnswerEnglish);
        addOrUpdateQuestionAnswers(questionAnswer);

        currentQuestion = getNextQuestion();
        currentQuestionEnglish = getNextQuestionEnglish();

        showQuestion();
    }

    private String getUserAnswer(String question) {

        for (ItemQuestionAnswer itemQuestionAnswer : questionAnswers) {
            if (question.equals(itemQuestionAnswer.getQuestion().getQuestion())) {
                return itemQuestionAnswer.getAnswer();
            }
        }

        return "";
    }

    private int getAnswerIndex(String answer, ItemAnswer[] answers) {

        for (int i = 0; i < answers.length; i++) {
            if (answer.equals(answers[i].getAnswer())) {
                return i;
            }
        }

        return -1;
    }

    private ItemQuestion getNextQuestion() {
        ItemQuestion nextQuestion = null;

        if (null != nestedQuestions) {
            nextQuestion = nestedQuestions[0];
            nestedQuestions = null;

        } else {
            String currentQuestion;

            if (null != questionAnswersStack.get(questionAnswersStack.size() - 1).getParentQuestion()) {
                currentQuestion = questionAnswersStack.get(questionAnswersStack.size() - 1).getParentQuestion();
                parentQuestion = null;
            } else {
                currentQuestion = this.currentQuestion.getQuestion();
            }

            int i;
            for (i = 0; i < questions.length; i++) {
                if (questions[i].getQuestion().equals(currentQuestion)) {
                    i++;
                    break;
                }
            }

            if (i < questions.length) {
                nextQuestion = questions[i];
            }
        }

        return nextQuestion;
    }

    private ItemQuestion getNextQuestionEnglish() {
        ItemQuestion nextQuestion = null;

        if (null != nestedQuestionsEnglish) {
            nextQuestion = nestedQuestionsEnglish[0];
            nestedQuestionsEnglish = null;

        } else {
            String currentQuestion;

            if (null != questionAnswersStackEnglish.get(questionAnswersStackEnglish.size() - 1).getParentQuestion()) {
                currentQuestion = questionAnswersStackEnglish.get(questionAnswersStackEnglish.size() - 1).getParentQuestion();
                parentQuestionEnglish = null;
            } else {
                currentQuestion = this.currentQuestionEnglish.getQuestion();
            }

            int i;
            for (i = 0; i < questionsEnglish.length; i++) {
                if (questionsEnglish[i].getQuestion().equals(currentQuestion)) {
                    i++;
                    break;
                }
            }

            if (i < questionsEnglish.length) {
                nextQuestion = questionsEnglish[i];
            }
        }

        return nextQuestion;
    }

    private ItemQuestion getPreviousQuestion() {
        ItemQuestion previousQuestion = questionAnswersStack.get(questionAnswersStack.size() - 1).getQuestion();

        parentQuestion = questionAnswersStack.get(questionAnswersStack.size() - 1).getParentQuestion();

        questionAnswersStack.remove(questionAnswersStack.size() - 1);
        return previousQuestion;
    }

    private ItemQuestion getPreviousQuestionEnglish() {
        ItemQuestion previousQuestion = questionAnswersStackEnglish.get(questionAnswersStackEnglish.size() - 1).getQuestion();

        parentQuestionEnglish = questionAnswersStackEnglish.get(questionAnswersStackEnglish.size() - 1).getParentQuestion();

        questionAnswersStackEnglish.remove(questionAnswersStackEnglish.size() - 1);
        return previousQuestion;
    }

    private void addOrUpdateQuestionAnswers(ItemQuestionAnswer questionAnswer) {
        for (int i = 0; i < questionAnswers.size(); i++) {
            if (questionAnswers.get(i).getQuestion().equals(questionAnswer.getQuestion())) {
                questionAnswers.remove(i);
                questionAnswers.add(i, questionAnswer);
                return;
            }
        }

        questionAnswers.add(questionAnswer);
    }

    @OnClick(R.id.btn_submit)
    void onSubmit() {
        stopAudio();

        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(this);
        builder.setMessage(AssetReader.getAppText(KEY_CONFIRM_PROCEED));
        builder.setPositiveButton(AssetReader.getAppText(KEY_YES), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                questionAnswersStack.add(questionAnswer);
                questionAnswersStackEnglish.add(questionAnswerEnglish);

                ArrayList<ColumnQuestionAnswer> questionAnswers = new ArrayList<>(16);

                for (ItemQuestionAnswer itemQuestionAnswer : questionAnswersStackEnglish) {
                    ColumnQuestionAnswer columnQuestionAnswer = new ColumnQuestionAnswer();
                    columnQuestionAnswer.setQuestion(itemQuestionAnswer.getQuestion().getQuestion());
                    columnQuestionAnswer.setAnswer(itemQuestionAnswer.getAnswer());

                    questionAnswers.add(columnQuestionAnswer);
                }

                AssetReader.setSocioDemographicAnswers(questionAnswers);

                if (AssetReader.toShowStartPrePostQuestions()) {
                    startActivity(new Intent(ActivitySociodemographicData.this, ActivityStartPreQuestions.class));
                } else {
                    startActivity(new Intent(ActivitySociodemographicData.this, ActivityPreQuestions.class));
                }

                finish();
            }
        });

        builder.setNegativeButton(AssetReader.getAppText(KEY_NO), null);
        builder.show();
    }
}