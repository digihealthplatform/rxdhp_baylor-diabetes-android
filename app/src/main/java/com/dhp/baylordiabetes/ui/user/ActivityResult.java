package com.dhp.baylordiabetes.ui.user;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.dhp.baylordiabetes.R;
import com.dhp.baylordiabetes.data.DataManager;
import com.dhp.baylordiabetes.data.adapter.AdapterResult;
import com.dhp.baylordiabetes.data.asset.AssetReader;
import com.dhp.baylordiabetes.data.item.ItemQuestion;
import com.dhp.baylordiabetes.data.item.ItemResultDialog;
import com.dhp.baylordiabetes.data.item.VideoRating;
import com.dhp.baylordiabetes.ui.ActivityBase;
import com.dhp.baylordiabetes.ui.login.ActivityBaseLogin;
import com.dhp.baylordiabetes.util.LToast;
import com.dhp.baylordiabetes.util.LView;

import java.io.File;
import java.util.Calendar;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.dhp.baylordiabetes.data.asset.AssetReader.KEY_DONE;
import static com.dhp.baylordiabetes.data.asset.AssetReader.KEY_SELECT_VIDEO_RATING;
import static com.dhp.baylordiabetes.data.asset.AssetReader.KEY_TITLE_RESULTS;
import static com.dhp.baylordiabetes.data.asset.AssetReader.KEY_TITLE_VIDEO_RATING;
import static com.dhp.baylordiabetes.data.asset.AssetReader.KEY_WATCH_FULL_VIDEO;
import static com.dhp.baylordiabetes.data.asset.AssetReader.KEY_YOUR_SCORE;
import static com.dhp.baylordiabetes.util.LUtils.getImagePath;
import static com.dhp.baylordiabetes.util.LView.setSpinnerAdapter;


public class ActivityResult extends ActivityBase {

    private static final String TAG = ActivityResult.class.getSimpleName();

    @BindView(R.id.rv_result)
    RecyclerView rvResult;

    @BindView(R.id.iv_menu)
    ImageView ivMenu;

    @BindView(R.id.btn_done)
    Button btnDone;

    @BindView(R.id.btn_watch_full_video)
    Button btnWatchFullVideo;

    @Inject
    DataManager dataManager;

    private int watchVideoClickedCount;

    private AlertDialog ratingDialog;

    private String videoRating = "";

    private VideoRating[] videoRatings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        ButterKnife.bind(this);
        getActivityComponent(this).inject(this);

        initToolbar(AssetReader.getAppText(KEY_TITLE_RESULTS));

        AdapterResult adapter = new AdapterResult(this, AssetReader.getPostQuestions(AssetReader.getJsonData()), AssetReader.getPostQuestionUserAnswers());
        rvResult.setLayoutManager(new LinearLayoutManager(this));
        rvResult.setAdapter(adapter);
        rvResult.setNestedScrollingEnabled(false);

        btnDone.setText(AssetReader.getAppText(KEY_DONE));

        if (AssetReader.isToShowVideoLinkInResult() && AssetReader.surveyType.equals("Counselling")) {
            btnWatchFullVideo.setVisibility(View.VISIBLE);
            btnWatchFullVideo.setText(AssetReader.getAppText(KEY_WATCH_FULL_VIDEO));
        } else {
            btnWatchFullVideo.setVisibility(View.GONE);
        }

        AssetReader.resultStartDateTime = Calendar.getInstance().getTimeInMillis();

        showResultDialog();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (1 == watchVideoClickedCount && AssetReader.surveyType.equals("Counselling")) {
            showVideoRatingDialog();
        }
    }

    private void showVideoRatingDialog() {
        if (null != ratingDialog && ratingDialog.isShowing()) {
            return;
        }

        final View dialogView = getLayoutInflater().inflate(R.layout.dialog_video_rating, null);

        AlertDialog.Builder builder;

        builder = new AlertDialog.Builder(this, R.style.AlertDialogLight);
        builder.setView(dialogView);

        TextView tvTitle = (TextView) dialogView.findViewById(R.id.tv_title);
        tvTitle.setText(AssetReader.getAppText(KEY_TITLE_VIDEO_RATING));

        Spinner spRating = (Spinner) dialogView.findViewById(R.id.sp_video_rating);

        videoRatings = AssetReader.getVideoRatings();

        final String[] ratings = getRatingsArray();

        setSpinnerAdapter(this, spRating, ratings);

        spRating.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                if (position > 0) {
                    videoRating = getRatingId(position - 1);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        builder.setPositiveButton(AssetReader.getAppText(KEY_DONE), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });

        ratingDialog = builder.create();

        ratingDialog.show();

        ratingDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (videoRating.isEmpty()) {
                    LToast.warning(AssetReader.getAppText(KEY_SELECT_VIDEO_RATING));
                } else {
                    AssetReader.videoRating = videoRating;
                    ratingDialog.dismiss();
                }
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        AssetReader.lastSurveyNumber = -1;
    }

    private void showResultDialog() {
        int correctAnswersCount = getTotalCorrectAnswers();
        int totalQuestions      = AssetReader.getPostQuestions(AssetReader.getJsonData()).length;

        final View dialogView = getLayoutInflater().inflate(R.layout.dialog_result, null);

        AlertDialog.Builder builder;

        builder = new AlertDialog.Builder(this, R.style.AlertDialogLight);
        builder.setView(dialogView);
        final Dialog dialog = builder.create();

        TextView tvScore   = (TextView) dialogView.findViewById(R.id.tv_score);
        TextView tvMessage = (TextView) dialogView.findViewById(R.id.tv_message);

        ImageView ivResult = (ImageView) dialogView.findViewById(R.id.iv_result);

        String result = AssetReader.getAppText(KEY_YOUR_SCORE) + " " + correctAnswersCount + "/" + totalQuestions;
        tvScore.setText(result);

        ItemResultDialog itemResultDialog = AssetReader.getResultDialog(AssetReader.getJsonData(), correctAnswersCount, totalQuestions);

        if (!itemResultDialog.getMessage().equals("")) {
            tvMessage.setText(itemResultDialog.getMessage());
        }

        File userAnsFile = new File(getImagePath(itemResultDialog.getImage()));
        Uri  imageUri    = Uri.fromFile(userAnsFile);
        LView.loadImage(this, imageUri, ivResult);

        dialogView.findViewById(R.id.iv_close_dialog).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private int getTotalCorrectAnswers() {
        int count = 0;

        ItemQuestion[] questions = AssetReader.getPostQuestions(AssetReader.getJsonData());

        int[] userAnswers = AssetReader.getPostQuestionUserAnswers();

        for (int i = 0; i < questions.length; i++) {

            if (questions[i].getCorrectAnswer().equals(questions[i].getAnswers()[userAnswers[i]].getAnswer())) {
                ++count;
            }
        }

        return count;
    }

    @OnClick(R.id.btn_watch_full_video)
    void onWatchFullVideoClicked() {
        Intent intent = new Intent(this, ActivityVideoPlay.class);
        intent.putExtra("video_file", "");
        intent.putExtra("first_page_play", true);

        ++watchVideoClickedCount;

        startActivity(intent);
    }

    @OnClick(R.id.btn_done)
    void startNew() {
        AssetReader.resultEndDateTime = Calendar.getInstance().getTimeInMillis();
        AssetReader.updateResultDateTime();

        Intent intent = new Intent(ActivityResult.this, ActivityThankYou.class);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
    }

    @OnClick(R.id.iv_menu)
    void onMenuClick() {
        PopupMenu popup = new PopupMenu(this, ivMenu);

        popup.getMenuInflater()
                .inflate(R.menu.menu_default, popup.getMenu());

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {

                AlertDialog.Builder builder = new AlertDialog.Builder(ActivityResult.this, R.style.AlertDialogLight);

                builder.setMessage(getString(R.string.confirm_logout));
                builder.setPositiveButton(getString(R.string.btn_logout), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dataManager.logout();
                        startActivity(new Intent(ActivityResult.this, ActivityBaseLogin.class));
                        finish();
                    }
                });
                builder.setNegativeButton(getString(R.string.btn_cancel), null);
                builder.show();

                return true;
            }
        });

        popup.show();
    }

    private String[] getRatingsArray() {
        String[] ratingsArray = new String[videoRatings.length + 1];
        ratingsArray[0] = AssetReader.getAppText(AssetReader.KEY_SELECT);

        for (int i = 0; i < videoRatings.length; i++) {
            ratingsArray[i + 1] = videoRatings[i].getTitle();
        }

        return ratingsArray;
    }

    private String getRatingId(int position) {
        return videoRatings[position].getId();
    }
}