package com.dhp.baylordiabetes.ui.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dhp.baylordiabetes.LApplication;
import com.dhp.baylordiabetes.R;
import com.dhp.baylordiabetes.data.DataManager;
import com.dhp.baylordiabetes.ui.ActivityBase;
import com.dhp.baylordiabetes.ui.admin.ActivityBaseAdminHome;
import com.dhp.baylordiabetes.ui.user.ActivityLanguageSelection;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;

import static com.dhp.baylordiabetes.util.LConstants.ADMIN_PWD;


public class ActivityBaseLogin extends ActivityBase {

    private boolean userLogin = true;

    @BindView(R.id.et_username)
    EditText etUsername;

    @BindView(R.id.et_password)
    EditText etPassword;

    @BindView(R.id.tv_login_user)
    TextView tvLoginUser;

    @BindView(R.id.tv_login_admin)
    TextView tvLoginAdmin;

    @BindView(R.id.ll_user)
    LinearLayout llUser;

    @BindView(R.id.ll_admin)
    LinearLayout llAdmin;

    @BindView(R.id.cb_remember_me)
    CheckBox cbRememberMe;

    @BindView(R.id.btn_login)
    Button btnLogin;

    @Inject
    DataManager dataManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        getActivityComponent(this).inject(this);

        initToolbar(getString(R.string.title_login));
    }

    @OnClick(R.id.ll_admin)
    void adminLogin() {
        if (!userLogin) {
            return;
        }

        userLogin = false;

        tvLoginUser.setTextColor(ContextCompat.getColor(this, R.color.color_grey_dark));
        tvLoginAdmin.setTextColor(ContextCompat.getColor(this, R.color.white));

        llUser.setBackgroundColor(ContextCompat.getColor(this, R.color.white));
        llAdmin.setBackgroundColor(ContextCompat.getColor(this, R.color.green));

        btnLogin.setText(getString(R.string.login_as_admin));

        etUsername.setVisibility(View.GONE);

        clearFields();
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @OnClick(R.id.ll_user)
    void userLogin() {
        if (userLogin) {
            return;
        }
        userLogin = true;

        tvLoginUser.setTextColor(ContextCompat.getColor(this, R.color.white));
        tvLoginAdmin.setTextColor(ContextCompat.getColor(this, R.color.color_grey_dark));

        llUser.setBackgroundColor(ContextCompat.getColor(this, R.color.green));
        llAdmin.setBackgroundColor(ContextCompat.getColor(this, R.color.white));
        btnLogin.setText(getString(R.string.login_as_user));

        etUsername.setVisibility(View.VISIBLE);

        clearFields();

        cbRememberMe.setChecked(true);
    }

    private void clearFields() {
        etUsername.setText("");
        etPassword.setText("");
        cbRememberMe.setChecked(false);
        etUsername.requestFocus();
    }

    @OnClick(R.id.btn_login)
    void login() {

        if (!userLogin) {
            if (etPassword.getText().toString().equals(ADMIN_PWD)) {
                dataManager.adminLoggedIn(cbRememberMe.isChecked());
                startActivity(new Intent(this, ActivityBaseAdminHome.class));
                finish();
            } else {
                Toasty.warning(LApplication.getContext(), LApplication.getContext().getString(R.string.invalid_credentials),
                        Toast.LENGTH_SHORT).show();
            }
            return;
        }

        if (dataManager.validateUserLogin(etUsername.getText().toString(), etPassword.getText().toString())) {
            dataManager.userLoggedIn(cbRememberMe.isChecked(), etUsername.getText().toString());
            startActivity(new Intent(this, ActivityLanguageSelection.class));
            finish();

        } else {
            Toasty.warning(LApplication.getContext(), LApplication.getContext().getString(R.string.invalid_credentials),
                    Toast.LENGTH_SHORT).show();
        }
    }
}