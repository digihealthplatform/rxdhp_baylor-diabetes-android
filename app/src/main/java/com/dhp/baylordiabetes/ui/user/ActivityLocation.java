package com.dhp.baylordiabetes.ui.user;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.dhp.baylordiabetes.R;
import com.dhp.baylordiabetes.data.asset.AssetReader;
import com.dhp.baylordiabetes.data.item.ItemLocationName;
import com.dhp.baylordiabetes.data.item.ItemLocationType;
import com.dhp.baylordiabetes.ui.ActivityBase;
import com.dhp.baylordiabetes.util.LToast;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.view.View.VISIBLE;
import static com.dhp.baylordiabetes.data.asset.AssetReader.KEY_CONTINUE;
import static com.dhp.baylordiabetes.data.asset.AssetReader.KEY_ENTER_LOCATION_NAME;
import static com.dhp.baylordiabetes.data.asset.AssetReader.KEY_LOCATION_NAME;
import static com.dhp.baylordiabetes.data.asset.AssetReader.KEY_LOCATION_TYPE;
import static com.dhp.baylordiabetes.data.asset.AssetReader.KEY_SELECT_LOCATION_TYPE;
import static com.dhp.baylordiabetes.data.asset.AssetReader.KEY_TITLE_LOCATION;
import static com.dhp.baylordiabetes.util.LView.setSpinnerAdapter;


public class ActivityLocation extends ActivityBase {

    private String locationType = "";
    private String location     = "";

    private int locationTypePosition;

    private String[] locationTypes;
    private String[] locations;

    @BindView(R.id.tv_location_type)
    TextView tvLocationType;

    @BindView(R.id.sp_location_type)
    Spinner spLocationType;

    @BindView(R.id.tv_location_name)
    TextView tvLocationName;

    @BindView(R.id.sp_location_name)
    Spinner spLocation;

    @BindView(R.id.et_location_other)
    EditText etLocationOther;

    @BindView(R.id.btn_continue)
    Button btnContinue;

    private ItemLocationType[] locationTypeItems;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location);
        ButterKnife.bind(this);

        initToolbar(AssetReader.getAppText(KEY_TITLE_LOCATION));

        tvLocationName.setText(AssetReader.getAppText(KEY_LOCATION_NAME) + ": ");
        tvLocationType.setText(AssetReader.getAppText(KEY_LOCATION_TYPE) + ": ");
        btnContinue.setText(AssetReader.getAppText(KEY_CONTINUE));

        locationTypeItems = AssetReader.getLocationTypes();

        locationTypes = getLocationTypesArray();
        locations = getLocationNamesArray(0);

        setSpinnerAdapter(this, spLocationType, locationTypes);
        setSpinnerAdapter(this, spLocation, locations);

        spLocationType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                if (position > 0) {
                    locationTypePosition = position - 1;
                    locationType = getLocationTypeId(position - 1);
                    location = "";
                    locations = getLocationNamesArray(position - 1);
                    setSpinnerAdapter(ActivityLocation.this, spLocation, locations);
                    etLocationOther.setVisibility(View.GONE);
                    etLocationOther.setText("");
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        spLocation.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                if (position > 0) {
                    if (locations.length - 1 == position) {
                        etLocationOther.setVisibility(VISIBLE);
                    } else {
                        location = getLocationNameId(position - 1);
                        etLocationOther.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
    }

    @OnClick(R.id.btn_continue)
    void onContinue() {
        if (VISIBLE == etLocationOther.getVisibility()) {
            location = etLocationOther.getText().toString().trim();
        }

        if (locationType.isEmpty()) {
            LToast.warning(AssetReader.getAppText(KEY_SELECT_LOCATION_TYPE));

        } else if (location.isEmpty()) {
            LToast.warning(AssetReader.getAppText(KEY_ENTER_LOCATION_NAME));

        } else {
            if (VISIBLE == etLocationOther.getVisibility()) {
                location = getOtherLocationNameId() + " " + etLocationOther.getText().toString().trim();
            }

            AssetReader.saveLocationDetails(locationType, location);
            startActivity(new Intent(this, ActivitySociodemographicData.class));
        }
    }

    private String[] getLocationTypesArray() {
        String[] locationTypesArray = new String[locationTypeItems.length + 1];
        locationTypesArray[0] = AssetReader.getAppText(AssetReader.KEY_SELECT);

        for (int i = 0; i < locationTypeItems.length; i++) {
            locationTypesArray[i + 1] = locationTypeItems[i].getTitle();
        }

        return locationTypesArray;
    }

    private String[] getLocationNamesArray(int index) {
        ArrayList<ItemLocationName> locationsArray = locationTypeItems[index].getLocations();

        String[] locationNamesArray = new String[locationsArray.size() + 1];
        locationNamesArray[0] = AssetReader.getAppText(AssetReader.KEY_SELECT);

        for (int i = 0; i < locationsArray.size(); i++) {
            locationNamesArray[i + 1] = locationsArray.get(i).getTitle();
        }

        return locationNamesArray;
    }

    private String getLocationTypeId(int position) {
        return locationTypeItems[position].getId();
    }

    private String getLocationNameId(int position) {
        return locationTypeItems[locationTypePosition].getLocations().get(position).getId();
    }

    private String getOtherLocationNameId() {
        int locationsLength = locationTypeItems[locationTypePosition].getLocations().size();
        return locationTypeItems[locationTypePosition].getLocations().get(locationsLength - 1).getId();
    }
}