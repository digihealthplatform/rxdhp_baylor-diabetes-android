package com.dhp.baylordiabetes.ui.user;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.widget.MediaController;
import android.widget.VideoView;

import com.dhp.baylordiabetes.R;
import com.dhp.baylordiabetes.data.asset.AssetReader;
import com.dhp.baylordiabetes.data.item.VideoFile;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.dhp.baylordiabetes.util.LConstants.VIDEO_PATH;


public class ActivityVideoPlay extends AppCompatActivity
        implements MediaPlayer.OnCompletionListener,
        MediaPlayer.OnInfoListener {

    @BindView(R.id.video_view)
    VideoView videoView;

    boolean watchChunk;
    boolean firstPagePlay;
    private String      videoUrl;
    private VideoFile[] videoFiles;
    private int         currentVideo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_play);
        ButterKnife.bind(this);

        videoUrl = "";

        if (null != getIntent()) {
            watchChunk = getIntent().getBooleanExtra("watch_chunk", false);
            firstPagePlay = getIntent().getBooleanExtra("first_page_play", false);
            videoUrl = getIntent().getStringExtra("video_file");
        }

        videoFiles = AssetReader.getVideoFiles(AssetReader.getJsonData());

        if (!videoUrl.isEmpty()) {
            playVideo(videoUrl);
        } else {
            playVideo(videoFiles[currentVideo].getFileName());
            ++currentVideo;
        }
    }

    @Override
    public void onBackPressed() {
    }

    void playVideo(String videoUrl) {
        videoView.setVideoPath(Environment.getExternalStorageDirectory().getAbsolutePath() + VIDEO_PATH + videoUrl);
        videoView.setMediaController(new MediaController(this));
        videoView.setOnCompletionListener(this);
        videoView.setOnInfoListener(this);
        videoView.requestFocus();

        videoView.start();
    }

    @Override
    public void onCompletion(MediaPlayer mediaPlayer) {

        if (watchChunk) {
            finish();
            return;
        }

        if (videoUrl.isEmpty() && currentVideo < videoFiles.length) {
            playVideo(videoFiles[currentVideo].getFileName());
            ++currentVideo;

        } else {
            if (!firstPagePlay) {
                if (AssetReader.toShowStartPrePostQuestions()) {
                    startActivity(new Intent(this, ActivityStartPostQuestions.class));
                } else {
                    startActivity(new Intent(this, ActivityPostQuestions.class));
                }
            }
            finish();
        }
    }

    @Override
    public boolean onInfo(MediaPlayer mediaPlayer, int what, int extra) {

//        MediaPlayer.TrackInfo[] trackInfoArray = mediaPlayer.getTrackInfo();
//
//        for (int i = 0; i < trackInfoArray.length; i++) {
//            // you can switch out the language comparison logic to whatever works for you
//            if (trackInfoArray[i].getTrackType() == MediaPlayer.TrackInfo.MEDIA_TRACK_TYPE_AUDIO
//                    && trackInfoArray[i].getLanguage().equals(Locale.getDefault().getISO3Language())
//            {
//                mediaPlayer.selectTrack(i);
//                break;
//            }
//        }
        return true;
    }
}