package com.dhp.baylordiabetes.ui.user;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.dhp.baylordiabetes.R;
import com.dhp.baylordiabetes.data.asset.AssetReader;
import com.dhp.baylordiabetes.data.item.ItemGameIteration;
import com.dhp.baylordiabetes.data.item.ItemSize;
import com.dhp.baylordiabetes.data.item.ItemUserSelectionGame;
import com.dhp.baylordiabetes.ui.ActivityBase;
import com.dhp.baylordiabetes.util.LUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.dhp.baylordiabetes.data.asset.AssetReader.getStringValueGame;


public class ActivityGame3 extends ActivityBase
        implements View.OnTouchListener {

    private static final String TAG = ActivityGame3.class.getSimpleName();

    @BindView(R.id.iv_left_1)
    ImageView ivLeft1;

    @BindView(R.id.iv_left_2)
    ImageView ivLeft2;

    @BindView(R.id.iv_right_1)
    ImageView ivRight1;

    @BindView(R.id.iv_right_2)
    ImageView ivRight2;

    @BindView(R.id.iv_right_3)
    ImageView ivRight3;

    @BindView(R.id.iv_centre)
    ImageView ivCentre;

    @BindView(R.id.rl_root)
    RelativeLayout rlRoot;

    Point lastPoint = new Point();

    private int windowWidth;
    private int windowHeight;

    ArrayList<ItemSize> sizes = new ArrayList<>();

    private int currentVideo;

    private boolean isPressed;

    private int pressedViewId;

    private int currentIteration;

    private ItemGameIteration[] gameIterations;

    ArrayList<String>  itemsInPlate;
    ArrayList<Integer> itemsInPartitions;

    private boolean isGoodChoice;

    private boolean isFirstAudioComplete;
    private boolean isSecondSoundComplete;

    private boolean toNotAutoProceed;

    private ItemUserSelectionGame itemUserSelectionGame;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_3);
        ButterKnife.bind(this);
        initToolbar(AssetReader.getGameTitle(3));

        initViews();

        loadImage(ivCentre, getStringValueGame(3, "plate_image"));

        currentVideo = getIntent().getIntExtra("current_video", 0);

        AssetReader.game3StartDateTime = System.currentTimeMillis();

        gameIterations = AssetReader.getGameIterations();

        itemsInPlate = new ArrayList<>();
        itemsInPartitions = new ArrayList<>();

        rlRoot.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                windowHeight = rlRoot.getMeasuredHeight();
                windowWidth = rlRoot.getMeasuredWidth();

                if (windowHeight > 0) {
                    rlRoot.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                }
            }
        });

        ivLeft1.setOnTouchListener(this);
        ivLeft2.setOnTouchListener(this);
        ivRight1.setOnTouchListener(this);
        ivRight2.setOnTouchListener(this);
        ivRight3.setOnTouchListener(this);

        playAudio(AssetReader.getStringValueGame(3, "audio"), false);

        itemUserSelectionGame = new ItemUserSelectionGame();
        itemUserSelectionGame.setGame3(new String[3]);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopAudio();
    }

    @Override
    protected void onStop() {
        super.onStop();
        stopAudio();
    }

    private void initItems() {
        ItemGameIteration gameIteration = gameIterations[currentIteration];

        sizes.add(new ItemSize((int) windowWidth / 4, (int) windowHeight / 3, 0, (int) windowHeight / 6));
        sizes.add(new ItemSize((int) windowWidth / 4, (int) windowHeight / 3, 0, (int) windowHeight / 6 + sizes.get(0).getHeight()));

        sizes.add(new ItemSize((int) windowWidth / 4, (int) windowHeight / 3, sizes.get(0).getWidth() * 3, 0));
        sizes.add(new ItemSize((int) windowWidth / 4, (int) windowHeight / 3, sizes.get(0).getWidth() * 3, sizes.get(0).getHeight()));
        sizes.add(new ItemSize((int) windowWidth / 4, (int) windowHeight / 3, sizes.get(0).getWidth() * 3, sizes.get(0).getHeight() * 2));
        sizes.add(new ItemSize((int) windowWidth / 2, windowHeight, sizes.get(0).getWidth(), 0));

        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) ivLeft1.getLayoutParams();

        layoutParams.width = sizes.get(0).getWidth();
        layoutParams.height = sizes.get(0).getHeight();
        layoutParams.topMargin = sizes.get(0).getY();
        layoutParams.leftMargin = sizes.get(0).getX();
        ivLeft1.setLayoutParams(layoutParams);
        loadImage(ivLeft1, gameIteration.getFoodItems()[0].getImage());

        layoutParams = (RelativeLayout.LayoutParams) ivLeft2.getLayoutParams();
        layoutParams.width = sizes.get(1).getWidth();
        layoutParams.height = sizes.get(1).getHeight();
        layoutParams.topMargin = sizes.get(1).getY();
        layoutParams.leftMargin = sizes.get(1).getX();
        ivLeft2.setLayoutParams(layoutParams);
        loadImage(ivLeft2, gameIteration.getFoodItems()[1].getImage());

        layoutParams = (RelativeLayout.LayoutParams) ivRight1.getLayoutParams();
        layoutParams.width = sizes.get(2).getWidth();
        layoutParams.height = sizes.get(2).getHeight();
        layoutParams.topMargin = sizes.get(2).getY();
        layoutParams.leftMargin = sizes.get(2).getX();
        ivRight1.setLayoutParams(layoutParams);
        loadImage(ivRight1, gameIteration.getFoodItems()[2].getImage());

        layoutParams = (RelativeLayout.LayoutParams) ivRight2.getLayoutParams();
        layoutParams.width = sizes.get(3).getWidth();
        layoutParams.height = sizes.get(3).getHeight();
        layoutParams.topMargin = sizes.get(3).getY();
        layoutParams.leftMargin = sizes.get(3).getX();
        ivRight2.setLayoutParams(layoutParams);
        loadImage(ivRight2, gameIteration.getFoodItems()[3].getImage());

        layoutParams = (RelativeLayout.LayoutParams) ivRight3.getLayoutParams();
        layoutParams.width = sizes.get(4).getWidth();
        layoutParams.height = sizes.get(4).getHeight();
        layoutParams.topMargin = sizes.get(4).getY();
        layoutParams.leftMargin = sizes.get(4).getX();
        ivRight3.setLayoutParams(layoutParams);
        loadImage(ivRight3, gameIteration.getFoodItems()[4].getImage());

        layoutParams = (RelativeLayout.LayoutParams) ivCentre.getLayoutParams();
        layoutParams.width = sizes.get(5).getWidth();
        layoutParams.height = sizes.get(5).getHeight();
        layoutParams.leftMargin = sizes.get(5).getX();
        ivCentre.setLayoutParams(layoutParams);
        loadImage(ivCentre, getStringValueGame(3, "plate_image"));
    }

    private void loadImage(ImageView imageView, String imageName) {
        String imgPath = LUtils.getImagePath(imageName);

        Glide.with(this)
                .load(imgPath)
                .fitCenter()
                .into(imageView);
    }

    @Override
    public boolean onTouch(View view, MotionEvent event) {
        if (isPressed && pressedViewId != view.getId()) {
            return true;
        }

        final Point point = new Point((int) event.getRawX(), (int) event.getRawY());

        ImageView imageView = (ImageView) view;

        pressedViewId = view.getId();

        int position = 0;

        switch (view.getId()) {

            case R.id.iv_left_1:
                position = 0;
                break;

            case R.id.iv_left_2:
                position = 1;
                break;

            case R.id.iv_right_1:
                position = 2;
                break;

            case R.id.iv_right_2:
                position = 3;
                break;

            case R.id.iv_right_3:
                position = 4;
                break;
        }

        switch (event.getAction() & MotionEvent.ACTION_MASK) {

            case MotionEvent.ACTION_DOWN:
                view.bringToFront();
                lastPoint = point;
                isPressed = true;
                break;

            case MotionEvent.ACTION_UP:

                Log.d(TAG, "onTouch: " + lastPoint.x + " " + lastPoint.y);
                isPressed = false;

                // Find closest plate partition
                int platePartition = 0;

                if (lastPoint.x >= sizes.get(5).getX()
                        && lastPoint.x <= (sizes.get(5).getX() + (int) (sizes.get(5).getWidth() / 2))) {

                    if (lastPoint.y < (windowHeight / 2 + view.getWidth() / 2)) {
                        platePartition = 1;

                    } else {
                        platePartition = 2;
                    }

                } else if (lastPoint.x >= sizes.get(5).getX()
                        && lastPoint.x <= (sizes.get(5).getX() + sizes.get(5).getWidth())) {

                    if (lastPoint.y < windowHeight / 2 + view.getWidth() / 2) {
                        platePartition = 3;

                    } else {
                        platePartition = 4;
                    }
                }

                String id = gameIterations[currentIteration].getFoodItems()[position].getId();

                if (0 == platePartition) {
                    moveToOriginalPosition(imageView, position);

                    if (itemsInPlate.contains(id)) {
                        int index = itemsInPlate.indexOf(id);
                        itemsInPlate.remove(id);
                        itemsInPartitions.remove(index);
                    }
                } else {
                    moveInsidePlate(imageView, position, platePartition);

                    if (itemsInPlate.contains(id)) {
                        int index = itemsInPlate.indexOf(id);
                        itemsInPlate.remove(id);
                        itemsInPartitions.remove(index);
                    }

                    itemsInPlate.add(id);
                    itemsInPartitions.add(platePartition);

                    checkForGoodChoice();
                }

                break;

            case MotionEvent.ACTION_MOVE:
                final Point offset = new Point(point.x - lastPoint.x, point.y - lastPoint.y);
                RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) imageView.getLayoutParams();

                if (layoutParams.leftMargin + offset.x + imageView.getWidth() < windowWidth
                        && 0 < layoutParams.leftMargin + offset.x
                        && layoutParams.topMargin + offset.y + imageView.getHeight() < windowHeight
                        && 0 < layoutParams.topMargin + offset.y) {

                    layoutParams.leftMargin += offset.x;
                    layoutParams.topMargin += offset.y;
                    imageView.setLayoutParams(layoutParams);
                }

                lastPoint = point;
                break;
        }
        return true;
    }

    private void checkForGoodChoice() {
        if (3 <= itemsInPlate.size()) {
            List<String> goodChoice12 = Arrays.asList(gameIterations[currentIteration].getGoodChoicePartition12());
            List<String> goodChoice34 = Arrays.asList(gameIterations[currentIteration].getGoodChoicePartition3());

            isGoodChoice = true;

            for (int i = 0; i < itemsInPlate.size(); i++) {

                if (itemsInPartitions.get(i) <= 2) {
                    if (!goodChoice12.contains(itemsInPlate.get(i))) {
                        isGoodChoice = false;
                        break;
                    }
                } else {
                    if (!goodChoice34.contains(itemsInPlate.get(i))) {
                        isGoodChoice = false;
                        break;
                    }
                }
            }

            isSecondSoundComplete = false;

            if (isGoodChoice) {
                showFullScreenGif(AssetReader.getStringValueGame(3, "good_choice_gif"));
                playAudio(AssetReader.getStringValueGame(3, "good_choice_sound"), false);
                toNotAutoProceed = false;
            } else {
                showFullScreenGif(AssetReader.getStringValueGame(3, "wrong_choice_gif"));
                playAudio(AssetReader.getStringValueGame(3, "wrong_choice_sound"), false);
                toNotAutoProceed = false;
            }
        }
    }

    private void moveToOriginalPosition(ImageView imageView, int position) {
        TranslateAnimation animation = new TranslateAnimation(0, 0, 0, 0);
        animation.setDuration(100);
        animation.setFillAfter(false);
        animation.setAnimationListener(new MoveToOriginalPositionAnimate(imageView, position));
        ivLeft2.startAnimation(animation);
    }

    private void moveInsidePlate(ImageView imageView, int position, int platePartition) {
        TranslateAnimation animation = new TranslateAnimation(0, 0, 0, 0);
        animation.setDuration(100);
        animation.setFillAfter(false);
        animation.setAnimationListener(new MoveInsidePlateAnimate(imageView, position, platePartition));
        ivLeft2.startAnimation(animation);
    }

    class MoveToOriginalPositionAnimate implements Animation.AnimationListener {

        private final int position;

        private final ImageView imageView;

        public MoveToOriginalPositionAnimate(ImageView imageView, int position) {
            this.imageView = imageView;
            this.position = position;
        }

        @Override
        public void onAnimationEnd(Animation animation) {
            imageView.clearAnimation();
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) imageView.getLayoutParams();
            layoutParams.width = sizes.get(position).getWidth();
            layoutParams.height = sizes.get(position).getHeight();
            layoutParams.topMargin = sizes.get(position).getY();
            layoutParams.leftMargin = sizes.get(position).getX();
            imageView.setLayoutParams(layoutParams);
        }

        @Override
        public void onAnimationRepeat(Animation animation) {
        }

        @Override
        public void onAnimationStart(Animation animation) {
        }
    }

    class MoveInsidePlateAnimate implements Animation.AnimationListener {

        private final int position;
        private final int platePartition;

        private final ImageView imageView;

        public MoveInsidePlateAnimate(ImageView imageView, int position, int platePartition) {
            this.imageView = imageView;
            this.position = position;
            this.platePartition = platePartition;
        }

        @Override
        public void onAnimationEnd(Animation animation) {
            imageView.clearAnimation();
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) imageView.getLayoutParams();
            layoutParams.width = sizes.get(position).getWidth();
            layoutParams.height = sizes.get(position).getHeight();

            switch (platePartition) {

                case 1: {
                    layoutParams.topMargin = (windowHeight / 2) - sizes.get(position).getHeight();
                    layoutParams.leftMargin = (int) ((windowWidth / 2) - sizes.get(position).getWidth() + sizes.get(position).getWidth() * 0.2f);
                    break;
                }

                case 2: {
                    layoutParams.topMargin = (windowHeight / 2);
                    layoutParams.leftMargin = (int) ((windowWidth / 2) - sizes.get(position).getWidth() + sizes.get(position).getWidth() * 0.2f);
                    break;
                }

                case 3: {
                    layoutParams.topMargin = (windowHeight / 2) - sizes.get(position).getHeight();
                    layoutParams.leftMargin = (int) ((windowWidth / 2) - sizes.get(position).getWidth() * 0.2f);
                    break;
                }

                case 4: {
                    layoutParams.topMargin = (windowHeight / 2);
                    layoutParams.leftMargin = (int) ((windowWidth / 2) - sizes.get(position).getWidth() * 0.2f);
                    break;
                }
            }

            imageView.setLayoutParams(layoutParams);
        }

        @Override
        public void onAnimationRepeat(Animation animation) {
        }

        @Override
        public void onAnimationStart(Animation animation) {
        }
    }

    @Override
    public void onAudioComplete() {
        if (toNotAutoProceed) {
            return;
        }

        if (!isFirstAudioComplete) {
            isFirstAudioComplete = true;
            initItems();

        } else if (!isSecondSoundComplete) {
            isSecondSoundComplete = true;

            if (isGoodChoice) {
                playAudio(AssetReader.getStringValueGame(3, "good_choice_speech"), false);
            } else {
                playAudio(AssetReader.getStringValueGame(3, "wrong_choice_speech"), false);
            }

        } else {
            if (isGoodChoice) {
                onDoneClicked();
            } else {
                hideFullScreenGif();
                showFoodDialog();
            }
        }
    }

    void onDoneClicked() {
        ++currentIteration;

        StringBuilder items = new StringBuilder();

        for (int i = 0; i < itemsInPlate.size(); i++) {
            String item      = itemsInPlate.get(i);
            int    partition = itemsInPartitions.get(i);

            if (4 == partition) {
                partition = 3;
            }

            if (!items.toString().isEmpty()) {
                items.append(";");
            }

            items.append(item).append(",").append(partition);
        }

        itemUserSelectionGame.getGame3()[currentIteration - 1] = items.toString();

        if (3 == currentIteration) {
            AssetReader.game3EndDateTime = System.currentTimeMillis();
            AssetReader.setItemUserSelectionGame(itemUserSelectionGame);

            Intent intent = new Intent(this, ActivityVideoPlayWithGame.class);
            intent.putExtra("current_video", currentVideo);
            startActivity(intent);
            finish();

        } else {
            playAudio(AssetReader.getStringValueGame(3, "audio"), false);
            toNotAutoProceed = true;
            hideFullScreenGif();
            isSecondSoundComplete = false;
            itemsInPlate.clear();
            itemsInPartitions.clear();
            initItems();
        }
    }

    private void showFoodDialog() {
        final View dialogView = getLayoutInflater().inflate(R.layout.dialog_good_food, null);

        AlertDialog.Builder builder;

        builder = new AlertDialog.Builder(this, R.style.AlertDialogLight);
        builder.setView(dialogView);
        final Dialog dialog = builder.create();

        ImageView ivPlate = (ImageView) dialogView.findViewById(R.id.iv_plate);
        loadImage(ivPlate, gameIterations[currentIteration].getGoodChoicePlateImage());

        dialogView.findViewById(R.id.iv_close_dialog).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                onDoneClicked();
            }
        });

        dialog.setCancelable(false);
        dialog.show();
    }
}