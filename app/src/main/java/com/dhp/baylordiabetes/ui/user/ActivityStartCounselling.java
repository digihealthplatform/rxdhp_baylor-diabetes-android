package com.dhp.baylordiabetes.ui.user;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;

import com.dhp.baylordiabetes.R;
import com.dhp.baylordiabetes.data.asset.AssetReader;
import com.dhp.baylordiabetes.ui.ActivityBase;

import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.dhp.baylordiabetes.data.asset.AssetReader.KEY_COUNSELLING_COMPLETE;
import static com.dhp.baylordiabetes.data.asset.AssetReader.KEY_START_COUNSELLING;


public class ActivityStartCounselling extends ActivityBase {

    @BindView(R.id.btn_start_counselling)
    Button btnStartCounselling;

    private boolean isStarted;

    private String[] counsellingImages;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_counselling);
        ButterKnife.bind(this);
        initToolbar("Counselling");

        counsellingImages = AssetReader.getCounsellingImages();

        btnStartCounselling.setText(AssetReader.getAppText(KEY_START_COUNSELLING));
    }

    @OnClick(R.id.btn_start_counselling)
    void onStartClicked() {

        if (null == counsellingImages || 0 == counsellingImages.length) {
            if (!isStarted) {
                AssetReader.videoOrCounsellingStartDateTime = Calendar.getInstance().getTimeInMillis();
                btnStartCounselling.setText(AssetReader.getAppText(KEY_COUNSELLING_COMPLETE));

            } else {
                if (AssetReader.toShowStartPrePostQuestions()) {
                    startActivity(new Intent(this, ActivityStartPostQuestions.class));
                } else {
                    startActivity(new Intent(this, ActivityPostQuestions.class));
                }
                finish();
            }

        } else {
            AssetReader.videoOrCounsellingStartDateTime = Calendar.getInstance().getTimeInMillis();
            startActivity(new Intent(this, ActivityCounselling.class));
            finish();
        }

        isStarted = true;
    }
}