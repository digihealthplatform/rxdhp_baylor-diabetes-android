package com.dhp.baylordiabetes;

import android.app.Application;
import android.content.Context;
import android.os.StrictMode;

import com.dhp.baylordiabetes.data.DataManager;
import com.dhp.baylordiabetes.data.asset.AssetReader;
import com.dhp.baylordiabetes.data.database.BaylorDatabase;
import com.dhp.baylordiabetes.data.database.SQLCipherHelperImpl;
import com.dhp.baylordiabetes.data.di.component.ApplicationComponent;
import com.dhp.baylordiabetes.data.di.component.DaggerApplicationComponent;
import com.dhp.baylordiabetes.data.di.module.ApplicationModule;
import com.raizlabs.android.dbflow.config.DatabaseConfig;
import com.raizlabs.android.dbflow.config.DatabaseDefinition;
import com.raizlabs.android.dbflow.config.FlowConfig;
import com.raizlabs.android.dbflow.config.FlowManager;
import com.raizlabs.android.dbflow.structure.database.DatabaseHelperListener;
import com.raizlabs.android.dbflow.structure.database.OpenHelper;

import java.util.Calendar;

import javax.inject.Inject;

import static com.dhp.baylordiabetes.data.database.SharedPrefsHelper.KEY_COUNSELLING_COUNT_BATCH;
import static com.dhp.baylordiabetes.data.database.SharedPrefsHelper.KEY_COUNSELLING_COUNT_BATCH_TRAINER;
import static com.dhp.baylordiabetes.data.database.SharedPrefsHelper.KEY_VIDEO_COUNT_BATCH;
import static com.dhp.baylordiabetes.data.database.SharedPrefsHelper.KEY_VIDEO_COUNT_BATCH_TRAINER;


public class LApplication extends Application {

    private static LApplication instance;

    private static boolean autoPlayAudio;

    protected ApplicationComponent applicationComponent;

    @Inject
    DataManager dataManager;

    @Override
    public void onCreate() {
        super.onCreate();

        instance = this;

        applicationComponent = DaggerApplicationComponent
                .builder()
                .applicationModule(new ApplicationModule(this))
                .build();
        applicationComponent.inject(this);

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        FlowManager.init(new FlowConfig.Builder(this).build());

        FlowManager.init(new FlowConfig.Builder(this)
                .addDatabaseConfig(
                        new DatabaseConfig.Builder(BaylorDatabase.class)
                                .openHelper(new DatabaseConfig.OpenHelperCreator() {
                                    @Override
                                    public OpenHelper createHelper(DatabaseDefinition databaseDefinition, DatabaseHelperListener helperListener) {
                                        return new SQLCipherHelperImpl(databaseDefinition, helperListener);
                                    }
                                }).build())
                .build());
    }

    public static LApplication get(Context context) {
        return (LApplication) context.getApplicationContext();
    }

    public static Context getContext() {
        return instance;
    }

    public ApplicationComponent getComponent() {
        return applicationComponent;
    }

    public static void setAutoPlayAudio(boolean autoPlayAudio) {
        LApplication.autoPlayAudio = autoPlayAudio;
    }

    public static boolean getAutoPlayAudio() {
        return autoPlayAudio;
    }

    public static boolean toShowVideo(DataManager dataManager) {
        int surveyBatchVideoCount;
        int surveyBatchCounsellingCount;
        int halfBatch;

        if (dataManager.isTrainer()) {
            surveyBatchVideoCount = dataManager.getInt(KEY_VIDEO_COUNT_BATCH_TRAINER);
            surveyBatchCounsellingCount = dataManager.getInt(KEY_COUNSELLING_COUNT_BATCH_TRAINER);
            halfBatch = AssetReader.getRandomBatchSizeTrainer() / 2;

        } else {
            surveyBatchVideoCount = dataManager.getInt(KEY_VIDEO_COUNT_BATCH);
            surveyBatchCounsellingCount = dataManager.getInt(KEY_COUNSELLING_COUNT_BATCH);
            halfBatch = AssetReader.getRandomBatchSize() / 2;
        }

        if (AssetReader.isToRandomise()) {
            if (surveyBatchVideoCount >= halfBatch) {
                return false;

            } else if (surveyBatchCounsellingCount >= halfBatch) {
                return true;

            } else {
                long currentMillis = Calendar.getInstance().getTimeInMillis();

                if (AssetReader.isToRandomise() && (currentMillis % 2) == 0) {
                    return false;
                }
                return true;
            }
        } else {
            return true;
        }
    }
}